graph [
  defaultnodesize 0
  name "SpinningMarchantiaSkel2Graph(27)"
  node [
    id 0
    label "0"
    graphics [
      y 120.000000
      x 13.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      y 66.000000
      x 24.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      y 175.000000
      x 29.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      y 41.000000
      x 34.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      y 9.000000
      x 48.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      y 111.000000
      x 49.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      y 132.000000
      x 55.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      y 57.000000
      x 56.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 8
    label "8"
    graphics [
      y 90.000000
      x 60.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 9
    label "9"
    graphics [
      y 126.000000
      x 61.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 10
    label "10"
    graphics [
      y 79.000000
      x 65.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 11
    label "11"
    graphics [
      y 86.000000
      x 71.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 12
    label "12"
    graphics [
      y 102.000000
      x 74.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 13
    label "13"
    graphics [
      y 68.000000
      x 77.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 14
    label "14"
    graphics [
      y 74.000000
      x 82.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 15
    label "15"
    graphics [
      y 115.000000
      x 82.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 16
    label "16"
    graphics [
      y 104.000000
      x 83.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 17
    label "17"
    graphics [
      y 88.000000
      x 84.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 18
    label "18"
    graphics [
      y 75.000000
      x 86.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 19
    label "19"
    graphics [
      y 113.000000
      x 89.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 20
    label "20"
    graphics [
      y 116.000000
      x 91.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 21
    label "21"
    graphics [
      y 80.000000
      x 93.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 22
    label "22"
    graphics [
      y 29.000000
      x 106.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 23
    label "23"
    graphics [
      y 111.000000
      x 110.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 24
    label "24"
    graphics [
      y 73.000000
      x 115.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 25
    label "25"
    graphics [
      y 231.000000
      x 135.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 26
    label "26"
    graphics [
      y 39.000000
      x 174.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  edge [
    source 0
    target 8
    eid 0
    weight 57.012193
    pts "[(14, 119), (15, 119), (16, 118), (17, 118), (18, 117), (19, 116), (20, 115), (21, 114), (22, 114), (23, 113), (24, 112), (25, 111), (26, 110), (27, 110), (28, 109), (29, 109), (30, 108), (31, 107), (32, 106), (33, 106), (34, 105), (35, 105), (36, 104), (37, 103), (38, 102), (39, 101), (40, 101), (41, 100), (42, 100), (43, 99), (44, 98), (45, 98), (46, 97), (47, 96), (48, 96), (49, 95), (50, 95), (51, 95), (52, 95), (53, 94), (54, 94), (55, 93), (56, 93), (57, 92), (58, 91), (59, 90)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 46
    capa 57.012193
    conn 0
  ]
  edge [
    source 1
    target 10
    eid 1
    weight 44.798990
    pts "[(25, 67), (26, 67), (27, 67), (28, 68), (29, 68), (30, 68), (31, 68), (32, 69), (33, 69), (34, 70), (35, 71), (36, 71), (37, 71), (38, 71), (39, 72), (40, 72), (41, 72), (42, 72), (43, 72), (44, 73), (45, 73), (46, 74), (47, 74), (48, 75), (49, 75), (50, 76), (51, 77), (52, 77), (53, 78), (54, 78), (55, 79), (56, 79), (57, 79), (58, 79), (59, 79), (60, 79), (61, 80), (62, 80), (63, 80), (64, 79)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 40
    capa 44.798990
    conn 0
  ]
  edge [
    source 2
    target 6
    eid 2
    weight 53.112698
    pts "[(29, 174), (29, 173), (29, 172), (29, 171), (30, 170), (30, 169), (30, 168), (31, 167), (31, 166), (31, 165), (32, 164), (32, 163), (33, 162), (33, 161), (33, 160), (33, 159), (33, 158), (33, 157), (33, 156), (34, 155), (34, 154), (35, 153), (35, 152), (36, 151), (37, 150), (38, 149), (39, 148), (39, 147), (40, 146), (41, 145), (41, 144), (42, 143), (43, 142), (44, 141), (45, 140), (46, 139), (47, 138), (48, 137), (48, 136), (49, 135), (50, 135), (51, 134), (52, 134), (53, 133), (54, 133)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 45
    capa 53.112698
    conn 0
  ]
  edge [
    source 3
    target 7
    eid 3
    weight 26.798990
    pts "[(35, 41), (36, 42), (37, 43), (38, 43), (39, 44), (40, 44), (41, 45), (42, 46), (43, 46), (44, 47), (45, 48), (46, 48), (47, 49), (48, 50), (49, 50), (50, 51), (51, 52), (51, 53), (52, 54), (53, 55), (54, 55), (55, 56)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 22
    capa 26.798990
    conn 0
  ]
  edge [
    source 4
    target 13
    eid 4
    weight 68.426407
    pts "[(48, 10), (48, 11), (49, 12), (49, 13), (50, 14), (50, 15), (51, 16), (52, 17), (53, 18), (53, 19), (54, 20), (55, 21), (56, 22), (57, 23), (58, 24), (59, 25), (60, 26), (61, 27), (61, 28), (62, 29), (62, 30), (63, 31), (63, 32), (64, 33), (65, 34), (65, 35), (66, 36), (66, 37), (66, 38), (67, 39), (67, 40), (68, 41), (69, 42), (70, 43), (70, 44), (71, 45), (72, 46), (72, 47), (73, 48), (73, 49), (73, 50), (73, 51), (74, 52), (74, 53), (75, 54), (75, 55), (75, 56), (76, 57), (76, 58), (76, 59), (77, 60), (77, 61), (78, 62), (78, 63), (78, 64), (78, 65), (78, 66)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 57
    capa 68.426407
    conn 0
  ]
  edge [
    source 5
    target 12
    eid 5
    weight 26.727922
    pts "[(50, 111), (51, 111), (52, 111), (53, 110), (54, 110), (55, 109), (56, 109), (57, 109), (58, 108), (59, 108), (60, 107), (61, 107), (62, 107), (63, 106), (64, 106), (65, 105), (66, 105), (67, 105), (68, 104), (69, 104), (70, 103), (71, 103), (72, 103), (73, 102)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 24
    capa 26.727922
    conn 0
  ]
  edge [
    source 7
    target 10
    eid 6
    weight 18.727922
    pts "[(57, 61), (58, 62), (59, 63), (60, 64), (61, 65), (62, 66), (63, 67), (64, 68), (65, 69), (65, 70), (65, 71), (66, 72), (66, 73), (66, 74), (66, 75), (66, 76)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 16
    capa 18.727922
    conn 0
  ]
  edge [
    source 7
    target 13
    eid 7
    weight 20.142136
    pts "[(60, 57), (61, 57), (62, 58), (63, 58), (64, 59), (65, 60), (66, 60), (67, 61), (68, 62), (69, 63), (70, 64), (71, 65), (72, 66), (73, 66), (74, 67), (75, 67), (76, 67)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 17
    capa 20.142136
    conn 0
  ]
  edge [
    source 9
    target 15
    eid 8
    weight 23.142136
    pts "[(62, 125), (63, 125), (64, 124), (65, 124), (66, 123), (67, 122), (68, 122), (69, 121), (70, 120), (71, 120), (72, 119), (73, 119), (74, 118), (75, 118), (76, 118), (77, 117), (78, 116), (79, 116), (80, 115), (81, 115)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 20
    capa 23.142136
    conn 0
  ]
  edge [
    source 10
    target 11
    eid 9
    weight 6.242641
    pts "[(67, 80), (68, 81), (68, 82), (69, 83), (69, 84), (70, 85)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 6
    capa 6.242641
    conn 0
  ]
  edge [
    source 10
    target 14
    eid 10
    weight 13.828427
    pts "[(68, 78), (69, 77), (70, 77), (71, 77), (72, 77), (73, 77), (74, 76), (75, 76), (76, 76), (77, 76), (78, 76), (79, 76), (80, 76), (81, 76)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 14
    capa 13.828427
    conn 0
  ]
  edge [
    source 11
    target 12
    eid 11
    weight 12.828427
    pts "[(72, 88), (72, 89), (72, 90), (72, 91), (73, 92), (73, 93), (73, 94), (73, 95), (74, 96), (74, 97), (74, 98), (74, 99), (74, 100)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 13
    capa 12.828427
    conn 0
  ]
  edge [
    source 11
    target 17
    eid 12
    weight 9.828427
    pts "[(74, 86), (75, 86), (76, 87), (77, 87), (78, 87), (79, 88), (80, 88), (81, 88), (82, 88), (83, 88)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 10
    capa 9.828427
    conn 0
  ]
  edge [
    source 12
    target 16
    eid 13
    weight 5.828427
    pts "[(77, 102), (78, 102), (79, 102), (80, 103), (81, 103), (82, 104)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 6
    capa 5.828427
    conn 0
  ]
  edge [
    source 13
    target 14
    eid 14
    weight 3.828427
    pts "[(79, 70), (80, 71), (81, 72), (81, 73)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 4
    capa 3.828427
    conn 0
  ]
  edge [
    source 14
    target 18
    eid 15
    weight 1.000000
    pts "[(84, 75), (85, 75)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 2
    capa 1.000000
    conn 0
  ]
  edge [
    source 15
    target 25
    eid 16
    weight 139.681241
    pts "[(83, 117), (83, 118), (83, 119), (82, 120), (82, 121), (81, 122), (81, 123), (80, 124), (80, 125), (80, 126), (79, 127), (79, 128), (79, 129), (79, 130), (78, 131), (78, 132), (77, 133), (77, 134), (77, 135), (77, 136), (77, 137), (77, 138), (77, 139), (77, 140), (78, 141), (78, 142), (78, 143), (78, 144), (79, 145), (79, 146), (79, 147), (79, 148), (80, 149), (80, 150), (80, 151), (80, 152), (80, 153), (81, 154), (81, 155), (82, 156), (82, 157), (82, 158), (83, 159), (83, 160), (84, 161), (84, 162), (84, 163), (84, 164), (85, 165), (85, 166), (86, 167), (86, 168), (87, 169), (87, 170), (87, 171), (87, 172), (88, 173), (88, 174), (89, 175), (89, 176), (90, 177), (90, 178), (90, 179), (91, 180), (92, 181), (92, 182), (93, 183), (94, 184), (95, 185), (95, 186), (96, 187), (97, 188), (97, 189), (98, 190), (99, 191), (99, 192), (100, 193), (101, 194), (101, 195), (102, 196), (103, 197), (103, 198), (103, 199), (104, 200), (105, 201), (106, 202), (107, 203), (108, 204), (109, 205), (110, 206), (111, 207), (112, 208), (113, 209), (114, 209), (115, 210), (116, 211), (117, 212), (118, 213), (119, 214), (120, 215), (121, 216), (122, 217), (123, 218), (124, 219), (125, 220), (126, 221), (126, 222), (127, 223), (128, 224), (129, 225), (130, 226), (131, 227), (132, 228), (133, 229), (134, 230)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 115
    capa 139.681241
    conn 0
  ]
  edge [
    source 15
    target 16
    eid 17
    weight 5.000000
    pts "[(84, 106), (84, 107), (84, 108), (84, 109), (84, 110), (84, 111)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 6
    capa 5.000000
    conn 0
  ]
  edge [
    source 15
    target 19
    eid 18
    weight 2.000000
    pts "[(86, 113), (87, 113), (88, 113)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 3
    capa 2.000000
    conn 0
  ]
  edge [
    source 16
    target 17
    eid 19
    weight 13.000000
    pts "[(85, 103), (85, 102), (85, 101), (85, 100), (85, 99), (85, 98), (85, 97), (85, 96), (85, 95), (85, 94), (85, 93), (85, 92), (85, 91), (85, 90)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 14
    capa 13.000000
    conn 0
  ]
  edge [
    source 17
    target 18
    eid 20
    weight 5.414214
    pts "[(85, 84), (85, 83), (85, 82), (86, 81), (86, 80), (86, 79)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 6
    capa 5.414214
    conn 0
  ]
  edge [
    source 17
    target 21
    eid 21
    weight 6.828427
    pts "[(87, 86), (88, 86), (89, 86), (90, 86), (91, 85), (92, 85), (93, 84)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 7
    capa 6.828427
    conn 0
  ]
  edge [
    source 18
    target 22
    eid 22
    weight 51.041631
    pts "[(88, 74), (88, 73), (88, 72), (89, 71), (89, 70), (90, 69), (90, 68), (90, 67), (90, 66), (90, 65), (90, 64), (91, 63), (91, 62), (92, 61), (93, 60), (94, 59), (94, 58), (94, 57), (95, 56), (95, 55), (96, 54), (96, 53), (97, 52), (97, 51), (97, 50), (98, 49), (98, 48), (98, 47), (99, 46), (99, 45), (100, 44), (101, 43), (101, 42), (101, 41), (102, 40), (102, 39), (103, 38), (103, 37), (103, 36), (103, 35), (103, 34), (104, 33), (105, 32), (105, 31), (105, 30)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 45
    capa 51.041631
    conn 0
  ]
  edge [
    source 18
    target 21
    eid 23
    weight 3.414214
    pts "[(89, 78), (90, 78), (91, 79), (92, 79)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 4
    capa 3.414214
    conn 0
  ]
  edge [
    source 19
    target 23
    eid 24
    weight 18.414214
    pts "[(91, 112), (92, 112), (93, 112), (94, 112), (95, 112), (96, 112), (97, 111), (98, 111), (99, 111), (100, 111), (101, 111), (102, 111), (103, 111), (104, 111), (105, 111), (106, 111), (107, 111), (108, 111), (109, 111)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 19
    capa 18.414214
    conn 0
  ]
  edge [
    source 19
    target 20
    eid 25
    weight 0.000000
    pts "[(91, 115)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 21
    target 24
    eid 26
    weight 19.313708
    pts "[(98, 81), (99, 80), (100, 79), (101, 78), (102, 77), (103, 77), (104, 76), (105, 76), (106, 76), (107, 75), (108, 75), (109, 75), (110, 74), (111, 74), (112, 74), (113, 73), (114, 73)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 17
    capa 19.313708
    conn 0
  ]
  edge [
    source 23
    target 24
    eid 27
    weight 36.071068
    pts "[(111, 109), (111, 108), (111, 107), (111, 106), (111, 105), (111, 104), (111, 103), (111, 102), (111, 101), (111, 100), (111, 99), (111, 98), (111, 97), (111, 96), (111, 95), (111, 94), (112, 93), (112, 92), (112, 91), (112, 90), (112, 89), (112, 88), (113, 87), (113, 86), (114, 85), (115, 84), (115, 83), (115, 82), (116, 81), (116, 80), (116, 79), (116, 78), (116, 77), (116, 76), (116, 75)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 35
    capa 36.071068
    conn 0
  ]
  edge [
    source 24
    target 26
    eid 28
    weight 68.669048
    pts "[(118, 73), (119, 72), (120, 71), (121, 70), (122, 70), (123, 70), (124, 69), (125, 69), (126, 68), (127, 67), (128, 66), (129, 66), (130, 65), (131, 64), (132, 63), (133, 63), (134, 63), (135, 62), (136, 62), (137, 61), (138, 61), (139, 61), (140, 60), (141, 60), (142, 59), (143, 59), (144, 59), (145, 58), (146, 58), (147, 57), (148, 57), (149, 56), (150, 56), (151, 55), (152, 55), (153, 55), (154, 54), (155, 54), (156, 53), (157, 52), (158, 52), (159, 51), (160, 51), (161, 50), (162, 50), (163, 49), (164, 49), (165, 48), (166, 47), (167, 46), (168, 45), (169, 44), (170, 43), (171, 42), (172, 41), (173, 40)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 56
    capa 68.669048
    conn 0
  ]
]