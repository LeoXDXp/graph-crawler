import math, json, copy
import cv2 as cv
from matplotlib import pyplot as plt
import networkx as nx
import numpy as np
from scipy.special import comb

def rotateM(point: tuple, origin: tuple=(0,0), angle: float=-90*math.pi/180) -> tuple[int, int]:
    """
    Rotate a point counterclockwise (270°) by a given angle around a given origin.
    The angle should be given in radians.
    """
    #print(point)
    ox, oy = origin
    px, py = point
    cosAngle = math.cos(angle)
    sinAngle = math.sin(angle)
    qx = ox + cosAngle * (px - ox) - sinAngle * (py - oy)
    qy = oy + sinAngle * (px - ox) + cosAngle * (py - oy)
    return int(qx), int(qy)



'''
Writes networkX graph to GML file
'''
def createGML(name: str, folder: str, graph: nx.Graph, save: bool=True) -> None:
    if not save:
        return
    graphName = name + "_Skel2Graph"
    gmlFile = open(folder + "/" + name + "_skelGraph.gml", "w+")
    gmlFile.write("graph [\n")
    gmlFile.write("  defaultnodesize 0\n")
    gmlFile.write('  name \"%s(%d)\"\n' %(graphName, len(list(graph.nodes))))

    for n in graph.nodes():
        x,y = graph.nodes[n]['pts'][0]
        gmlFile.write("  node [\n")
        gmlFile.write("    id %d\n" %n)
        gmlFile.write("    label \"%d\"\n" %n)
        gmlFile.write("    graphics [\n")
        gmlFile.write("      y %f\n" %y)
        gmlFile.write("      x %f\n" %x)
        gmlFile.write("      hasOutline 0\n")
        gmlFile.write("      z 0.0\n")
        gmlFile.write("      hasFill 0\n")
        gmlFile.write("    ]\n")
        gmlFile.write("  ]\n")


    for (s,e) in graph.edges():
        ptsList = []
        for x,y in graph[s][e]['pts']:
            ptsList.append((x,y))
        gmlFile.write("  edge [\n")
        gmlFile.write("    source %d\n" %s)
        gmlFile.write("    target %d\n" %e)
        gmlFile.write("    eid %d\n" %graph[s][e]['eid'])
        gmlFile.write("    weight %f\n" %graph[s][e]['weight'])
        gmlFile.write("    pts \"%s\"\n" %str(ptsList))
        gmlFile.write("    graphics [\n")
        gmlFile.write("      width 1.41\n")
        gmlFile.write('      fill  \"#00b8FF\" \n')
        gmlFile.write('      targetArrow  \"none\" \n')
        gmlFile.write("    ]\n")
        gmlFile.write("    lgth %d\n" %len(ptsList))
        gmlFile.write("    capa %f\n" %graph[s][e]['weight'])
        gmlFile.write("    conn 0\n")
        gmlFile.write("  ]\n")

    gmlFile.write("]")
    gmlFile.close()
    # Testing
    h = nx.read_gml(folder + "/" + name + "_skelGraph.gml")
    print(h.edges(), h.nodes())

'''
There is a comb function for Python which does 'n choose k'
only you can't apply it to an array right away
So here we vectorize it...
'''
def myComb(a,b):
    return comb(a,b,exact=True)

vComb = np.vectorize(myComb)

'''

'''
def get_tp_fp_tn_fn(cooccurrence_matrix):
    tp_plus_fp = vComb(cooccurrence_matrix.sum(0, dtype=int),2).sum()
    tp_plus_fn = vComb(cooccurrence_matrix.sum(1, dtype=int),2).sum()
    tp = vComb(cooccurrence_matrix.astype(int), 2).sum()
    fp = tp_plus_fp - tp
    fn = tp_plus_fn - tp
    tn = comb(cooccurrence_matrix.sum(), 2) - tp - fp - fn
    return [tp, fp, tn, fn]

'''
https://gist.githubusercontent.com/jwcarr/626cbc80e0006b526688/raw/48846b5975b9d48ce28cbc9ae19a8f70006fdbfa/vi.py
Variation of information (VI): Meila, M. (2007). Comparing clusterings-an information
based distance. Journal of Multivariate Analysis, 98, 873-895. doi:10.1016/j.jmva.2006.11.013
https://en.wikipedia.org/wiki/Variation_of_information
'''
def variation_of_information(X: list, Y: list, n: int) -> float:
    #n = float(sum([len(x) for x in X]))
    print("-----Variation of Information\nMin: 0.0. Max (log(n)): %f" %(math.log(n)))
    sigma = 0.0
    for x in X:
        p = len(x) / n
        for y in Y:
            q = len(y) / n
            r = len(set(x) & set(y)) / n
            if r > 0.0:
                sigma += r * (math.log(r / p, 2) + math.log(r / q, 2))
    return abs(sigma)

'''
Calculate VI, Rand and Jaccard metrics for the individualized filaments by an
algorithm regarding the ground truth
Returns a list of lists with not-exactly matched filaments in overMatchedProposedFils
'''
def metrics(g_truth: list, proposedFilaments: dict, edgeCount: list) -> list:
    exactAnts = []
    overMatchAnts = []
    overMatchAnts2Edges = []
    overMatchAnts3Edges = []
    overMatchAnts4_6Edges = []
    overMatchedProposedFils = []
    for counter, gtsol in enumerate(g_truth):
        for key, propSol in proposedFilaments.items():
            if (sorted(gtsol) == sorted(propSol)):
                #print(f"counter {counter}, key {key}: {sorted(propSol)}")
                exactAnts.append(counter)
                continue
            # overmatch means gtsol is smaller than propSol
            overmatchSet = set(sorted(gtsol)).difference(sorted(propSol))
            remainingSet = set(sorted(propSol)).difference(sorted(gtsol))
            if ((len(overmatchSet) == 0) and (len(remainingSet) < 2)):
                overMatchAnts.append(counter)
                overMatchedProposedFils.append(key)
            # Consider case of 2 edges of overmatch
            elif ((len(overmatchSet) == 0) and (len(remainingSet) == 2)):
                overMatchAnts2Edges.append(counter)
                overMatchedProposedFils.append(key)
            elif ((len(overmatchSet) == 0) and (len(remainingSet) == 3)):
                overMatchAnts3Edges.append(counter)
                overMatchedProposedFils.append(key)
            elif ((len(overmatchSet) == 0) and (len(remainingSet) < 6)):
                overMatchAnts4_6Edges.append(counter)
    print("------Exact matches: %d -> " %(len(exactAnts)), end = ' ')
    for ant in exactAnts:
        print(ant, end=' ')
    print()
    print("----over matches: %d -> " %(len(overMatchAnts)), end = ' ')
    for ant in overMatchAnts:
        print(ant, end=' ')
    print()
    print("---- 2 edges over matches: %d -> " %(len(overMatchAnts2Edges)), end = ' ')
    for ant in overMatchAnts2Edges:
        print(ant, end=' ')
    print()
    print("---- 3 edges over matches: %d -> " %(len(overMatchAnts3Edges)), end = ' ')
    for ant in overMatchAnts3Edges:
        print(ant, end=' ')
    print()
    print("---- 4-6 edges over matches: %d -> " %(len(overMatchAnts4_6Edges)), end = ' ')
    for ant in overMatchAnts4_6Edges:
        print(ant, end=' ')
    print()

    VI_Y_proposedFilaments = [x for x in proposedFilaments.values()] # Y, angle 60
    contingency_table = np.zeros((len(g_truth), len(proposedFilaments)))
    for i in range(len(g_truth)):
        #print(proposedFilaments[i])
        gtruth_set = set(g_truth[i])
        for j in range(len(proposedFilaments)):
            try:
                contingency_table[i][j] = len(gtruth_set.intersection(set(proposedFilaments[j])) )
            except KeyError:
                # Some keys can come as string
                contingency_table[i][j] = len(gtruth_set.intersection(set(proposedFilaments[str(j)])) )
            except:
                print(f"ERROR, debug Data -> i,j: {i},{j}; proposedFils: {proposedFilaments} ")
                return []

    #print (contingency_table)
    # Get the stats
    tp, fp, tn, fn = get_tp_fp_tn_fn(contingency_table)
    randIdx = float(tp + tn) / (tp + fp + fn + tn)
    JacIdx = float (tp) / (tp + fp + fn)
    precision = float(tp) / (tp + fp)
    recall = float(tp) / (tp + fn)
    f1 = (2.0 * precision * recall) / (precision + recall)
    print("----- %f %d %d %d %d %f %f %f %f %f"
          %(variation_of_information(g_truth,VI_Y_proposedFilaments, edgeCount),
            tp, fp, tn, fn, randIdx, JacIdx, precision, recall, f1))
    return overMatchedProposedFils

'''
Filter input image, making all inputs of black background and white foreground.
* path: image file location
'''
def image_preparation(path: str, bg_to_black: bool = False, savefig: bool = False,
               manual_thresh: bool = False) -> tuple[np.array, np.array]:
    plt.figure(figsize=(18, 16))

    folder, name = path.rsplit("/",1)
    outSkel = cv.imread(path, cv.IMREAD_GRAYSCALE)
    initialImage = outSkel
    if bg_to_black:
        outSkel =~outSkel
    print(outSkel.max(), outSkel.min())
    if manual_thresh:
        ret, outSkelThresh = cv.threshold(outSkel, 70, 255, cv.THRESH_BINARY)
    else:
        ret, outSkelThresh = cv.threshold(outSkel, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)
    plt.imshow(outSkelThresh, plt.cm.gray)
    outSkel = outSkelThresh
    outSkel = outSkel/255
    #outSkel = outSkel.astype(int)
    #plt.imshow(outSkel, plt.cm.gray)
    print(outSkel.min(), outSkel.max(),outSkel.shape)
    plt.axis("off")
    if savefig:
        # Remove extension from name
        name = name.rsplit(".",1)
        plt.savefig(folder + "/" + name + "_outSkel_bw.png")
    return outSkel, initialImage

'''
Saves networkX graph as JSON, while adding additional information
'''

def graph2json(graph: nx.Graph, save_json: bool=False, folder: str=",", name: str=None) -> None:
    # Identify center nodes or "soma" nodes used by neuron logic in Phil
    largest_cc = max(nx.connected_components(graph), key=len)
    #largest_cc
    subgraph = graph.subgraph(list(largest_cc))
    #centerNodes = nx.center(subgraph)
    closenessCentral = sorted(nx.closeness_centrality(subgraph).items(), key=lambda item: item[1])
    centerNodes = [k[0] for k in closenessCentral[-3:]]
    print(f"Center nodes: {centerNodes}")

    # SKNW sets some stuff as np.array which is not accepted by nx.node_link_data
    graph_json = copy.deepcopy(graph)
    for n in graph.nodes():
        graph_json.nodes[n]['pts'] = graph.nodes[n]['pts'].tolist()

        t1,t2 = rotateM(graph.nodes[n]['o'])
        graph_json.nodes[n]['o'] = np.array([t1,abs(t2)])
        graph_json.nodes[n]['o'] = graph_json.nodes[n]['o'].tolist()

        # Signal center nodes
        if n in centerNodes:
            graph_json.nodes[n]['centernode'] = 1

    for (s,e) in graph.edges():
        for counter, pnt in enumerate(graph[s][e]['pts']):
            t1,t2 = rotateM(pnt)
            graph_json[s][e]['pts'][counter] = [t1,abs(t2)]
        graph_json[s][e]['pts'] = graph_json[s][e]['pts'].tolist()

    if save_json:
        # Cant proceed without at least a name
        if name:
            data1 = nx.node_link_data(graph_json)
            #print(data1)
            with open(folder + "/" + name + '_Skel.json', 'w') as outfile:
                json.dump(data1, outfile)
        else:
            print("Cant save Skel JSON file without a name. A folder path is also suggested")
    print(graph_json.nodes())

'''
Pass the ground truth list to 2 dictionaries:
 * gtruth_dict: List to dictionary
 * gtruth_rev: Uses the edges as key
'''
def gtruth_map(g_truth: dict) -> tuple[dict, dict]:
    gtruth_dict = {}
    gtruth_rev = {}
    i = 0
    for filament in g_truth:
        gtruth_rev[tuple(sorted(filament))] = i
        gtruth_dict[i] = sorted(filament)
        i += 1

    return gtruth_dict, gtruth_rev

'''
Compares the ground truth with the proposed filaments returned by a filament individualization algorithm.
The comparison is done by comparing the selected edges by the expert and by the algorithm

Returns a list of dictionaries, where each dictionary indicates the ants/filaments that where exactly matched by 
the algorithm, 
'''
def exact_match(gtruth_rev: dict, results: list[dict], names: list=None) -> dict:
    
    exactList = []
    #extendendList = [] # Up to over/submatch 3
    
    # Each res in result is a set of proposed filaments by the algorithm
    for counter, res in enumerate(results):
        exactMatch = {}
        for key, value in res.items():
            # Sort the edge IDs of each ant, the same way it was done for the ground truth
            if tuple(sorted(value)) in gtruth_rev:
                exactMatch[key] = value
        exactList.append(exactMatch)
        try:
            print(f"Exact match {names[counter]}: {len(exactMatch)}")
        except:
            print(f"Exact match {counter}: {len(exactMatch)}")
    #print (exactList)
    
    return exactList
