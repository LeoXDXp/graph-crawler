graph [
  defaultnodesize 0
  name "Synth-QuantitativeIFS-Fig6_BasicTest2_Skel2Graph(5)"
  node [
    id 0
    label "0"
    graphics [
      y 59.000000
      x 10.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      y 59.000000
      x 72.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      y 109.000000
      x 73.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      y 15.000000
      x 74.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      y 58.000000
      x 132.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  edge [
    source 0
    target 1
    eid 0
    weight 60.000000
    pts "[(11, 59), (12, 59), (13, 59), (14, 59), (15, 59), (16, 59), (17, 59), (18, 59), (19, 59), (20, 59), (21, 59), (22, 59), (23, 59), (24, 59), (25, 59), (26, 59), (27, 59), (28, 59), (29, 59), (30, 59), (31, 59), (32, 59), (33, 59), (34, 59), (35, 59), (36, 59), (37, 59), (38, 59), (39, 59), (40, 59), (41, 59), (42, 59), (43, 59), (44, 59), (45, 59), (46, 59), (47, 59), (48, 59), (49, 59), (50, 59), (51, 59), (52, 59), (53, 59), (54, 59), (55, 59), (56, 59), (57, 59), (58, 59), (59, 59), (60, 59), (61, 59), (62, 59), (63, 59), (64, 59), (65, 59), (66, 59), (67, 59), (68, 59), (69, 59), (70, 59), (71, 59)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 61
    capa 60.000000
    conn 0
  ]
  edge [
    source 1
    target 3
    eid 1
    weight 41.000000
    pts "[(73, 57), (73, 56), (73, 55), (73, 54), (73, 53), (73, 52), (73, 51), (73, 50), (73, 49), (73, 48), (73, 47), (73, 46), (73, 45), (73, 44), (73, 43), (73, 42), (73, 41), (73, 40), (73, 39), (73, 38), (73, 37), (73, 36), (73, 35), (73, 34), (73, 33), (73, 32), (73, 31), (73, 30), (73, 29), (73, 28), (73, 27), (73, 26), (73, 25), (73, 24), (73, 23), (73, 22), (73, 21), (73, 20), (73, 19), (73, 18), (73, 17), (73, 16)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 42
    capa 41.000000
    conn 0
  ]
  edge [
    source 1
    target 2
    eid 2
    weight 46.828427
    pts "[(73, 62), (73, 63), (73, 64), (73, 65), (73, 66), (73, 67), (73, 68), (73, 69), (73, 70), (73, 71), (73, 72), (73, 73), (73, 74), (73, 75), (73, 76), (73, 77), (73, 78), (73, 79), (73, 80), (73, 81), (73, 82), (73, 83), (73, 84), (73, 85), (73, 86), (73, 87), (73, 88), (73, 89), (73, 90), (73, 91), (73, 92), (72, 93), (72, 94), (72, 95), (72, 96), (72, 97), (72, 98), (73, 99), (73, 100), (73, 101), (73, 102), (73, 103), (73, 104), (73, 105), (73, 106), (73, 107), (73, 108)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 47
    capa 46.828427
    conn 0
  ]
  edge [
    source 1
    target 4
    eid 3
    weight 56.414214
    pts "[(75, 60), (76, 59), (77, 59), (78, 59), (79, 59), (80, 59), (81, 59), (82, 59), (83, 59), (84, 59), (85, 59), (86, 59), (87, 59), (88, 59), (89, 59), (90, 59), (91, 59), (92, 59), (93, 59), (94, 59), (95, 59), (96, 59), (97, 59), (98, 59), (99, 59), (100, 59), (101, 59), (102, 59), (103, 59), (104, 59), (105, 59), (106, 59), (107, 59), (108, 59), (109, 59), (110, 59), (111, 59), (112, 59), (113, 59), (114, 59), (115, 59), (116, 59), (117, 59), (118, 59), (119, 59), (120, 59), (121, 59), (122, 59), (123, 59), (124, 59), (125, 59), (126, 59), (127, 59), (128, 59), (129, 59), (130, 59), (131, 59)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 57
    capa 56.414214
    conn 0
  ]
]