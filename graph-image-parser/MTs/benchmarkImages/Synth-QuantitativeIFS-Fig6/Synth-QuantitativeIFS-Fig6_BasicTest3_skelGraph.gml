graph [
  defaultnodesize 0
  name "Synth-QuantitativeIFS-Fig6_BasicTest3_Skel2Graph(9)"
  node [
    id 0
    label "0"
    graphics [
      y 111.000000
      x 2.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      y 24.000000
      x 8.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      y 27.000000
      x 8.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      y 26.000000
      x 14.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      y 28.000000
      x 20.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      y 32.000000
      x 27.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      y 38.000000
      x 39.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      y 72.000000
      x 72.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 8
    label "8"
    graphics [
      y 10.000000
      x 130.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  edge [
    source 0
    target 7
    eid 0
    weight 84.911688
    pts "[(3, 111), (4, 111), (5, 111), (6, 110), (7, 110), (8, 110), (9, 109), (10, 109), (11, 108), (12, 108), (13, 107), (14, 107), (15, 106), (16, 106), (17, 105), (18, 105), (19, 104), (20, 104), (21, 104), (22, 103), (23, 102), (24, 102), (25, 101), (26, 101), (27, 101), (28, 101), (29, 100), (30, 100), (31, 100), (32, 100), (33, 100), (34, 100), (35, 99), (36, 99), (37, 98), (38, 98), (39, 98), (40, 97), (41, 97), (42, 97), (43, 96), (44, 96), (45, 95), (46, 94), (47, 94), (48, 94), (49, 93), (50, 92), (51, 92), (52, 92), (53, 91), (54, 91), (55, 90), (56, 89), (57, 88), (58, 88), (59, 87), (60, 86), (61, 85), (62, 84), (63, 83), (64, 82), (65, 81), (66, 80), (67, 79), (68, 79), (69, 78), (70, 77), (70, 76), (71, 75), (72, 74)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 71
    capa 84.911688
    conn 0
  ]
  edge [
    source 1
    target 2
    eid 1
    weight 0.000000
    pts "[(9, 24)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 2
    target 3
    eid 2
    weight 1.000000
    pts "[(12, 26), (13, 26)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 2
    capa 1.000000
    conn 0
  ]
  edge [
    source 3
    target 4
    eid 3
    weight 0.000000
    pts "[(19, 28)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 4
    target 5
    eid 4
    weight 1.000000
    pts "[(25, 31), (26, 31)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 2
    capa 1.000000
    conn 0
  ]
  edge [
    source 5
    target 6
    eid 5
    weight 7.656854
    pts "[(32, 34), (33, 34), (34, 35), (35, 35), (36, 36), (37, 37), (38, 38)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 7
    capa 7.656854
    conn 0
  ]
  edge [
    source 6
    target 7
    eid 6
    weight 43.941125
    pts "[(43, 41), (44, 41), (45, 41), (46, 42), (47, 43), (48, 44), (49, 45), (50, 45), (51, 46), (52, 47), (52, 48), (53, 49), (54, 50), (55, 51), (56, 52), (56, 53), (57, 54), (58, 55), (59, 56), (60, 57), (61, 58), (61, 59), (62, 60), (63, 61), (64, 62), (65, 62), (66, 63), (67, 64), (68, 65), (68, 66), (69, 67), (69, 68), (70, 69), (71, 70), (71, 71)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 35
    capa 43.941125
    conn 0
  ]
  edge [
    source 7
    target 8
    eid 7
    weight 86.296465
    pts "[(74, 71), (75, 71), (76, 71), (77, 70), (78, 70), (79, 69), (80, 68), (80, 67), (81, 66), (82, 65), (83, 64), (84, 64), (85, 63), (86, 62), (87, 61), (88, 60), (89, 59), (90, 58), (91, 57), (92, 56), (93, 55), (94, 55), (95, 54), (96, 53), (96, 52), (97, 51), (98, 50), (99, 49), (100, 48), (101, 47), (102, 46), (103, 45), (104, 44), (105, 43), (106, 42), (107, 41), (108, 40), (109, 39), (110, 38), (110, 37), (111, 36), (112, 36), (113, 35), (114, 34), (115, 33), (116, 32), (117, 31), (117, 30), (117, 29), (118, 28), (119, 27), (120, 26), (121, 25), (122, 24), (122, 23), (123, 22), (123, 21), (124, 20), (125, 19), (125, 18), (126, 17), (127, 16), (127, 15), (127, 14), (128, 13), (129, 12), (129, 11)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 67
    capa 86.296465
    conn 0
  ]
]