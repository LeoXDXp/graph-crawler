graph [
  defaultnodesize 0
  name "field4_t0_filtrada_slice1_crop_z_filledSkel2Graph(26)"
  node [
    id 0
    label "0"
    graphics [
      y 253.000000
      x 165.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      y 267.000000
      x 170.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      y 164.000000
      x 175.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      y 244.000000
      x 180.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      y 200.000000
      x 183.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      y 232.000000
      x 192.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      y 259.000000
      x 195.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      y 261.000000
      x 197.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 8
    label "8"
    graphics [
      y 212.000000
      x 199.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 9
    label "9"
    graphics [
      y 209.000000
      x 200.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 10
    label "10"
    graphics [
      y 299.000000
      x 205.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 11
    label "11"
    graphics [
      y 219.000000
      x 206.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 12
    label "12"
    graphics [
      y 254.000000
      x 211.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 13
    label "13"
    graphics [
      y 228.000000
      x 216.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 14
    label "14"
    graphics [
      y 241.000000
      x 227.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 15
    label "15"
    graphics [
      y 216.000000
      x 229.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 16
    label "16"
    graphics [
      y 247.000000
      x 231.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 17
    label "17"
    graphics [
      y 169.000000
      x 234.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 18
    label "18"
    graphics [
      y 244.000000
      x 234.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 19
    label "19"
    graphics [
      y 225.000000
      x 240.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 20
    label "20"
    graphics [
      y 294.000000
      x 247.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 21
    label "21"
    graphics [
      y 282.000000
      x 263.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 22
    label "22"
    graphics [
      y 267.000000
      x 266.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 23
    label "23"
    graphics [
      y 253.000000
      x 272.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 24
    label "24"
    graphics [
      y 222.000000
      x 275.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 25
    label "25"
    graphics [
      y 257.000000
      x 277.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  edge [
    source 0
    target 3
    eid 0
    weight 16.313708
    pts "[(252, 165), (251, 166), (251, 167), (251, 168), (250, 169), (249, 170), (249, 171), (248, 172), (247, 173), (246, 174), (245, 175), (245, 176), (245, 177), (244, 178)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 14
    capa 16.313708
    conn 0
  ]
  edge [
    source 1
    target 3
    eid 1
    weight 26.727922
    pts "[(267, 170), (267, 171), (267, 172), (266, 173), (265, 174), (264, 175), (263, 175), (262, 176), (261, 176), (260, 177), (259, 177), (258, 178), (257, 178), (256, 178), (255, 179), (254, 179), (253, 179), (252, 179), (251, 180), (250, 180), (249, 180), (248, 181), (247, 181), (246, 181)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 24
    capa 26.727922
    conn 0
  ]
  edge [
    source 2
    target 4
    eid 2
    weight 37.899495
    pts "[(164, 176), (165, 177), (166, 177), (167, 177), (168, 177), (169, 177), (170, 178), (171, 178), (172, 178), (173, 178), (174, 178), (175, 179), (176, 179), (177, 180), (178, 180), (179, 180), (180, 180), (181, 180), (182, 180), (183, 181), (184, 181), (185, 181), (186, 181), (187, 182), (188, 182), (189, 182), (190, 182), (191, 182), (192, 182), (193, 183), (194, 183), (195, 183), (196, 183), (197, 183), (198, 183), (199, 183)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 36
    capa 37.899495
    conn 0
  ]
  edge [
    source 3
    target 4
    eid 3
    weight 37.656854
    pts "[(203, 183), (204, 184), (205, 184), (206, 184), (207, 184), (208, 184), (209, 184), (210, 184), (211, 184), (212, 184), (213, 184), (214, 184), (215, 184), (216, 184), (217, 183), (218, 183), (219, 183), (220, 183), (221, 183), (222, 183), (223, 183), (224, 183), (225, 183), (226, 183), (227, 183), (228, 184), (229, 184), (230, 184), (231, 184), (232, 184), (233, 183), (234, 183), (235, 182), (236, 182), (237, 182), (238, 182), (239, 182)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 37
    capa 37.656854
    conn 0
  ]
  edge [
    source 3
    target 5
    eid 4
    weight 8.656854
    pts "[(239, 184), (239, 185), (238, 186), (238, 187), (237, 188), (236, 189), (235, 189), (234, 190)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 8
    capa 8.656854
    conn 0
  ]
  edge [
    source 4
    target 8
    eid 5
    weight 17.142136
    pts "[(201, 185), (201, 186), (202, 187), (203, 188), (204, 189), (204, 190), (205, 191), (206, 192), (207, 193), (208, 194), (209, 195), (209, 196), (210, 197), (211, 198)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 14
    capa 17.142136
    conn 0
  ]
  edge [
    source 5
    target 8
    eid 6
    weight 18.485281
    pts "[(231, 192), (230, 192), (229, 193), (228, 193), (227, 194), (226, 194), (225, 195), (224, 195), (223, 195), (222, 195), (221, 196), (220, 196), (219, 197), (218, 197), (217, 198), (216, 198), (215, 198)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 17
    capa 18.485281
    conn 0
  ]
  edge [
    source 5
    target 11
    eid 7
    weight 16.899495
    pts "[(232, 194), (232, 195), (231, 196), (230, 197), (229, 197), (228, 198), (228, 199), (227, 200), (226, 201), (226, 202), (225, 203), (225, 204), (224, 205), (223, 205), (222, 205)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 15
    capa 16.899495
    conn 0
  ]
  edge [
    source 6
    target 13
    eid 8
    weight 37.455844
    pts "[(258, 194), (257, 194), (256, 195), (255, 195), (254, 195), (253, 195), (252, 195), (251, 195), (250, 196), (249, 197), (248, 198), (247, 199), (246, 200), (245, 200), (244, 201), (243, 202), (242, 202), (241, 203), (240, 204), (239, 204), (238, 204), (237, 205), (236, 206), (235, 207), (235, 208), (234, 209), (234, 210), (233, 211), (232, 213), (231, 214), (230, 215)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 31
    capa 37.455844
    conn 0
  ]
  edge [
    source 7
    target 12
    eid 9
    weight 14.071068
    pts "[(260, 197), (259, 198), (259, 199), (258, 200), (258, 201), (257, 202), (257, 203), (257, 204), (257, 205), (256, 206), (255, 207), (255, 208), (255, 209)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 13
    capa 14.071068
    conn 0
  ]
  edge [
    source 8
    target 9
    eid 10
    weight 1.000000
    pts "[(211, 200), (210, 200)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 2
    capa 1.000000
    conn 0
  ]
  edge [
    source 8
    target 11
    eid 11
    weight 5.656854
    pts "[(214, 201), (215, 202), (216, 203), (217, 204), (218, 205)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 5
    capa 5.656854
    conn 0
  ]
  edge [
    source 10
    target 12
    eid 12
    weight 45.142136
    pts "[(298, 204), (297, 204), (296, 204), (295, 205), (294, 205), (293, 205), (292, 205), (291, 205), (290, 205), (289, 206), (288, 206), (287, 206), (286, 207), (285, 207), (284, 207), (283, 208), (282, 208), (281, 208), (280, 208), (279, 208), (278, 209), (277, 210), (276, 210), (275, 211), (274, 211), (273, 211), (272, 211), (271, 211), (270, 212), (269, 212), (268, 212), (267, 213), (266, 213), (265, 213), (264, 213), (263, 213), (262, 213), (261, 214), (260, 214), (259, 214), (258, 214), (257, 214)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 42
    capa 45.142136
    conn 0
  ]
  edge [
    source 11
    target 17
    eid 13
    weight 59.355339
    pts "[(218, 208), (217, 209), (216, 210), (215, 210), (214, 210), (213, 211), (212, 211), (211, 211), (210, 212), (209, 212), (208, 213), (207, 213), (206, 213), (205, 213), (204, 214), (203, 214), (202, 215), (201, 215), (200, 216), (199, 216), (198, 217), (197, 217), (196, 218), (195, 219), (194, 219), (193, 220), (192, 220), (191, 221), (190, 221), (189, 221), (188, 222), (187, 222), (186, 223), (185, 224), (184, 224), (183, 225), (182, 226), (181, 226), (180, 226), (179, 227), (178, 227), (177, 228), (176, 229), (175, 229), (174, 230), (173, 230), (172, 230), (171, 231), (170, 232), (169, 233)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 50
    capa 59.355339
    conn 0
  ]
  edge [
    source 11
    target 13
    eid 14
    weight 8.485281
    pts "[(221, 209), (222, 210), (223, 211), (224, 212), (225, 213), (226, 214), (227, 215)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 7
    capa 8.485281
    conn 0
  ]
  edge [
    source 12
    target 12
    eid 15
    weight 0.000000
    pts "[(255, 212)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 12
    target 14
    eid 16
    weight 13.313708
    pts "[(250, 215), (250, 216), (249, 217), (248, 218), (247, 219), (246, 220), (245, 221), (244, 222), (243, 223), (243, 224), (242, 225)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 11
    capa 13.313708
    conn 0
  ]
  edge [
    source 12
    target 16
    eid 17
    weight 15.899495
    pts "[(254, 216), (253, 217), (253, 218), (252, 219), (251, 220), (250, 221), (250, 222), (249, 223), (249, 224), (249, 225), (249, 226), (249, 227), (248, 228), (247, 229)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 14
    capa 15.899495
    conn 0
  ]
  edge [
    source 13
    target 15
    eid 18
    weight 13.727922
    pts "[(226, 218), (225, 219), (224, 220), (223, 221), (222, 222), (221, 223), (220, 224), (219, 225), (219, 226), (218, 227), (217, 228)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 11
    capa 13.727922
    conn 0
  ]
  edge [
    source 13
    target 14
    eid 19
    weight 11.899495
    pts "[(230, 219), (231, 220), (232, 221), (233, 221), (234, 221), (235, 222), (236, 223), (236, 224), (237, 225), (238, 226)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 10
    capa 11.899495
    conn 0
  ]
  edge [
    source 14
    target 19
    eid 20
    weight 16.556349
    pts "[(238, 228), (237, 229), (236, 230), (235, 231), (234, 232), (233, 232), (232, 234), (231, 235), (230, 236), (229, 237), (228, 238), (227, 239), (226, 240)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 13
    capa 16.556349
    conn 0
  ]
  edge [
    source 14
    target 18
    eid 21
    weight 3.828427
    pts "[(241, 229), (242, 230), (242, 231), (243, 232)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 4
    capa 3.828427
    conn 0
  ]
  edge [
    source 16
    target 18
    eid 22
    weight 0.000000
    pts "[(246, 232)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 16
    target 20
    eid 23
    weight 50.213203
    pts "[(249, 231), (250, 231), (251, 232), (252, 232), (253, 232), (254, 232), (255, 232), (256, 232), (257, 233), (258, 233), (259, 233), (260, 233), (261, 234), (262, 234), (263, 234), (264, 234), (265, 234), (266, 234), (267, 235), (268, 235), (269, 235), (270, 236), (271, 236), (272, 236), (273, 237), (274, 237), (275, 237), (276, 237), (277, 238), (278, 238), (279, 238), (280, 239), (281, 239), (282, 239), (283, 240), (284, 240), (285, 241), (286, 242), (287, 242), (288, 243), (289, 244), (290, 245), (291, 245), (292, 245), (293, 246)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 45
    capa 50.213203
    conn 0
  ]
  edge [
    source 18
    target 23
    eid 24
    weight 37.970563
    pts "[(244, 237), (244, 238), (244, 239), (244, 240), (244, 241), (243, 242), (243, 243), (243, 244), (243, 245), (243, 246), (242, 247), (242, 248), (242, 249), (243, 250), (243, 251), (243, 252), (243, 253), (243, 254), (244, 255), (245, 257), (245, 258), (245, 259), (245, 260), (245, 261), (245, 262), (246, 263), (247, 264), (248, 265), (248, 266), (249, 267), (250, 268), (251, 269), (252, 270), (252, 271)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 34
    capa 37.970563
    conn 0
  ]
  edge [
    source 18
    target 22
    eid 25
    weight 36.284271
    pts "[(247, 236), (248, 237), (249, 238), (249, 239), (250, 240), (251, 241), (252, 242), (253, 243), (254, 244), (255, 245), (256, 246), (257, 247), (257, 248), (258, 249), (259, 250), (260, 251), (260, 252), (261, 253), (261, 254), (262, 255), (263, 257), (263, 258), (264, 259), (265, 260), (265, 261), (266, 262), (267, 263), (267, 264), (267, 265)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 29
    capa 36.284271
    conn 0
  ]
  edge [
    source 21
    target 22
    eid 26
    weight 12.828427
    pts "[(281, 264), (280, 265), (279, 265), (278, 265), (277, 265), (276, 265), (275, 265), (274, 265), (273, 265), (272, 266), (271, 266), (270, 266), (269, 266)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 13
    capa 12.828427
    conn 0
  ]
  edge [
    source 22
    target 23
    eid 27
    weight 11.071068
    pts "[(266, 268), (265, 269), (264, 269), (263, 269), (262, 270), (261, 270), (260, 271), (259, 271), (258, 272), (257, 273)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 10
    capa 11.071068
    conn 0
  ]
  edge [
    source 23
    target 24
    eid 28
    weight 30.071068
    pts "[(251, 274), (250, 274), (249, 274), (248, 274), (247, 275), (246, 275), (245, 276), (244, 277), (243, 277), (242, 277), (241, 277), (240, 277), (239, 277), (238, 277), (237, 277), (236, 276), (235, 276), (234, 276), (233, 276), (232, 276), (231, 276), (230, 276), (229, 276), (228, 276), (227, 276), (226, 276), (225, 276), (224, 275), (223, 275)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 29
    capa 30.071068
    conn 0
  ]
  edge [
    source 23
    target 25
    eid 29
    weight 0.000000
    pts "[(256, 276)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
]