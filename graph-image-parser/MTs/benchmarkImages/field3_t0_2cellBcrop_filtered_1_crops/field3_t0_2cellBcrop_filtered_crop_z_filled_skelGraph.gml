graph [
  defaultnodesize 0
  name "field3_t0_2cellBcrop_filtered_crop_z_filledSkel2Graph(37)"
  node [
    id 0
    label "0"
    graphics [
      y 115.000000
      x 29.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 1
    label "1"
    graphics [
      y 58.000000
      x 30.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 2
    label "2"
    graphics [
      y 56.000000
      x 34.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 3
    label "3"
    graphics [
      y 95.000000
      x 46.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 4
    label "4"
    graphics [
      y 41.000000
      x 50.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 5
    label "5"
    graphics [
      y 59.000000
      x 53.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 6
    label "6"
    graphics [
      y 103.000000
      x 58.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 7
    label "7"
    graphics [
      y 85.000000
      x 60.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 8
    label "8"
    graphics [
      y 80.000000
      x 67.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 9
    label "9"
    graphics [
      y 75.000000
      x 70.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 10
    label "10"
    graphics [
      y 74.000000
      x 72.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 11
    label "11"
    graphics [
      y 67.000000
      x 76.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 12
    label "12"
    graphics [
      y 66.000000
      x 78.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 13
    label "13"
    graphics [
      y 75.000000
      x 79.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 14
    label "14"
    graphics [
      y 97.000000
      x 79.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 15
    label "15"
    graphics [
      y 124.000000
      x 79.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 16
    label "16"
    graphics [
      y 53.000000
      x 84.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 17
    label "17"
    graphics [
      y 57.000000
      x 84.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 18
    label "18"
    graphics [
      y 94.000000
      x 85.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 19
    label "19"
    graphics [
      y 95.000000
      x 88.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 20
    label "20"
    graphics [
      y 108.000000
      x 91.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 21
    label "21"
    graphics [
      y 73.000000
      x 96.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 22
    label "22"
    graphics [
      y 71.000000
      x 102.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 23
    label "23"
    graphics [
      y 8.000000
      x 107.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 24
    label "24"
    graphics [
      y 111.000000
      x 111.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 25
    label "25"
    graphics [
      y 83.000000
      x 112.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 26
    label "26"
    graphics [
      y 23.000000
      x 113.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 27
    label "27"
    graphics [
      y 86.000000
      x 117.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 28
    label "28"
    graphics [
      y 86.000000
      x 119.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 29
    label "29"
    graphics [
      y 89.000000
      x 122.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 30
    label "30"
    graphics [
      y 83.000000
      x 124.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 31
    label "31"
    graphics [
      y 85.000000
      x 127.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 32
    label "32"
    graphics [
      y 76.000000
      x 136.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 33
    label "33"
    graphics [
      y 108.000000
      x 158.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 34
    label "34"
    graphics [
      y 127.000000
      x 163.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 35
    label "35"
    graphics [
      y 97.000000
      x 171.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  node [
    id 36
    label "36"
    graphics [
      y 83.000000
      x 178.000000
      hasOutline 0
      z 0.0
      hasFill 0
    ]
  ]
  edge [
    source 0
    target 3
    eid 0
    weight 22.798990
    pts "[(30, 114), (30, 113), (31, 112), (31, 111), (32, 110), (33, 109), (34, 108), (35, 107), (36, 106), (37, 105), (38, 104), (39, 103), (40, 102), (41, 101), (42, 100), (43, 99), (44, 99), (45, 98)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 18
    capa 22.798990
    conn 0
  ]
  edge [
    source 1
    target 3
    eid 1
    weight 41.213203
    pts "[(30, 59), (30, 60), (30, 61), (30, 62), (31, 63), (31, 64), (31, 65), (31, 66), (31, 67), (31, 68), (31, 69), (32, 70), (33, 71), (34, 72), (34, 73), (34, 74), (34, 75), (34, 76), (34, 77), (35, 78), (35, 79), (36, 80), (37, 81), (38, 82), (39, 83), (40, 84), (40, 85), (41, 86), (41, 87), (42, 88), (42, 89), (43, 90), (44, 91), (45, 92), (45, 93), (45, 94)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 36
    capa 41.213203
    conn 0
  ]
  edge [
    source 2
    target 5
    eid 2
    weight 17.828427
    pts "[(35, 56), (36, 56), (37, 57), (38, 57), (39, 57), (40, 57), (41, 57), (42, 57), (43, 57), (44, 57), (45, 57), (46, 57), (47, 58), (48, 58), (49, 58), (50, 58), (51, 58), (52, 58)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 18
    capa 17.828427
    conn 0
  ]
  edge [
    source 3
    target 7
    eid 3
    weight 14.313708
    pts "[(48, 94), (49, 94), (50, 93), (51, 92), (52, 91), (53, 90), (54, 90), (55, 89), (56, 88), (57, 87), (58, 86), (59, 86)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 12
    capa 14.313708
    conn 0
  ]
  edge [
    source 3
    target 15
    eid 4
    weight 42.526912
    pts "[(48, 98), (48, 99), (48, 100), (49, 101), (49, 102), (50, 103), (51, 104), (52, 104), (53, 105), (54, 105), (55, 106), (56, 106), (57, 107), (58, 107), (59, 108), (60, 109), (61, 110), (62, 110), (63, 111), (64, 111), (65, 112), (66, 113), (67, 114), (68, 115), (69, 116), (70, 117), (71, 117), (72, 118), (73, 119), (74, 120), (75, 121), (76, 122), (77, 123), (78, 124)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 34
    capa 42.526912
    conn 0
  ]
  edge [
    source 4
    target 16
    eid 5
    weight 36.970563
    pts "[(51, 41), (52, 41), (53, 41), (54, 42), (55, 43), (56, 43), (57, 44), (58, 44), (59, 45), (60, 45), (61, 45), (62, 45), (63, 46), (64, 46), (65, 46), (66, 46), (67, 47), (68, 47), (69, 47), (70, 47), (71, 48), (72, 48), (73, 48), (74, 49), (75, 49), (76, 50), (77, 50), (78, 51), (79, 51), (80, 52), (81, 52), (82, 52), (83, 53)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 33
    capa 36.970563
    conn 0
  ]
  edge [
    source 5
    target 9
    eid 6
    weight 23.727922
    pts "[(53, 61), (54, 62), (54, 63), (54, 64), (54, 65), (55, 66), (55, 67), (56, 68), (57, 69), (58, 70), (59, 71), (60, 72), (61, 72), (62, 73), (63, 74), (64, 74), (65, 74), (66, 74), (67, 74), (68, 74), (69, 74)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 21
    capa 23.727922
    conn 0
  ]
  edge [
    source 5
    target 11
    eid 7
    weight 22.899495
    pts "[(55, 59), (56, 59), (57, 59), (58, 60), (59, 60), (60, 60), (61, 60), (62, 61), (63, 61), (64, 61), (65, 61), (66, 61), (67, 62), (68, 62), (69, 63), (70, 63), (71, 64), (72, 64), (73, 65), (74, 65), (75, 66)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 21
    capa 22.899495
    conn 0
  ]
  edge [
    source 6
    target 14
    eid 8
    weight 20.656854
    pts "[(59, 103), (60, 103), (61, 103), (62, 103), (63, 102), (64, 101), (65, 101), (66, 100), (67, 100), (68, 100), (69, 100), (70, 100), (71, 100), (72, 100), (73, 100), (74, 100), (75, 100), (76, 100), (77, 100), (78, 99)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 20
    capa 20.656854
    conn 0
  ]
  edge [
    source 7
    target 8
    eid 9
    weight 6.242641
    pts "[(61, 84), (62, 83), (63, 83), (64, 82), (65, 82), (66, 81)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 6
    capa 6.242641
    conn 0
  ]
  edge [
    source 7
    target 14
    eid 10
    weight 22.313708
    pts "[(61, 86), (61, 87), (62, 88), (63, 89), (64, 89), (65, 89), (66, 90), (67, 90), (68, 90), (69, 91), (70, 91), (71, 91), (72, 91), (73, 91), (74, 92), (75, 93), (76, 93), (77, 94), (78, 95), (78, 96)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 20
    capa 22.313708
    conn 0
  ]
  edge [
    source 8
    target 9
    eid 11
    weight 2.000000
    pts "[(69, 78), (69, 77), (69, 76)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 3
    capa 2.000000
    conn 0
  ]
  edge [
    source 8
    target 18
    eid 12
    weight 20.970563
    pts "[(69, 81), (70, 81), (71, 82), (72, 83), (73, 84), (74, 85), (75, 85), (76, 86), (77, 87), (78, 88), (79, 88), (80, 89), (81, 90), (82, 90), (83, 91), (84, 92), (85, 93)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 17
    capa 20.970563
    conn 0
  ]
  edge [
    source 9
    target 10
    eid 13
    weight 0.000000
    pts "[(71, 75)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 10
    target 11
    eid 14
    weight 6.242641
    pts "[(72, 73), (73, 72), (73, 71), (74, 70), (75, 69), (75, 68)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 6
    capa 6.242641
    conn 0
  ]
  edge [
    source 10
    target 13
    eid 15
    weight 5.000000
    pts "[(73, 75), (74, 75), (75, 75), (76, 75), (77, 75), (78, 75)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 6
    capa 5.000000
    conn 0
  ]
  edge [
    source 11
    target 12
    eid 16
    weight 0.000000
    pts "[(77, 66)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 12
    target 17
    eid 17
    weight 8.656854
    pts "[(79, 65), (79, 64), (79, 63), (80, 62), (80, 61), (81, 60), (82, 59), (83, 58)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 8
    capa 8.656854
    conn 0
  ]
  edge [
    source 12
    target 21
    eid 18
    weight 18.071068
    pts "[(79, 67), (80, 67), (81, 67), (82, 67), (83, 67), (84, 68), (85, 68), (86, 68), (87, 68), (88, 69), (89, 69), (90, 69), (91, 70), (92, 70), (93, 71), (94, 72), (95, 72)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 17
    capa 18.071068
    conn 0
  ]
  edge [
    source 13
    target 25
    eid 19
    weight 34.727922
    pts "[(80, 77), (81, 78), (82, 79), (83, 80), (84, 81), (85, 82), (86, 82), (87, 83), (88, 83), (89, 83), (90, 83), (91, 83), (92, 83), (93, 83), (94, 83), (95, 83), (96, 83), (97, 83), (98, 84), (99, 84), (100, 85), (101, 85), (102, 85), (103, 85), (104, 85), (105, 85), (106, 85), (107, 85), (108, 85), (109, 85), (110, 85), (111, 84)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 32
    capa 34.727922
    conn 0
  ]
  edge [
    source 13
    target 21
    eid 20
    weight 13.000000
    pts "[(82, 75), (83, 75), (84, 75), (85, 75), (86, 75), (87, 75), (88, 75), (89, 75), (90, 75), (91, 75), (92, 75), (93, 75), (94, 75), (95, 75)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 14
    capa 13.000000
    conn 0
  ]
  edge [
    source 14
    target 20
    eid 21
    weight 13.313708
    pts "[(81, 99), (82, 100), (82, 101), (83, 102), (84, 103), (85, 104), (86, 105), (87, 106), (88, 107), (89, 107), (90, 108)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 11
    capa 13.313708
    conn 0
  ]
  edge [
    source 14
    target 18
    eid 22
    weight 1.000000
    pts "[(83, 96), (84, 96)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 2
    capa 1.000000
    conn 0
  ]
  edge [
    source 16
    target 23
    eid 23
    weight 51.698485
    pts "[(86, 52), (87, 51), (88, 50), (89, 49), (90, 48), (91, 47), (91, 46), (92, 45), (92, 44), (93, 43), (93, 42), (94, 41), (94, 40), (95, 39), (95, 38), (96, 37), (96, 36), (97, 35), (98, 34), (98, 33), (98, 32), (99, 31), (99, 30), (99, 29), (100, 28), (101, 27), (102, 26), (103, 25), (103, 24), (104, 23), (104, 22), (104, 21), (105, 20), (105, 19), (105, 18), (106, 17), (106, 16), (106, 15), (107, 14), (107, 13), (107, 12), (107, 11), (107, 10), (107, 9)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 44
    capa 51.698485
    conn 0
  ]
  edge [
    source 16
    target 17
    eid 24
    weight 0.000000
    pts "[(85, 55)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 17
    target 22
    eid 25
    weight 20.970563
    pts "[(87, 57), (88, 58), (89, 58), (90, 59), (91, 60), (92, 61), (93, 61), (94, 62), (95, 63), (96, 64), (97, 65), (97, 66), (97, 67), (98, 68), (99, 69), (100, 70), (101, 71)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 17
    capa 20.970563
    conn 0
  ]
  edge [
    source 18
    target 19
    eid 26
    weight 0.000000
    pts "[(87, 95)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 19
    target 27
    eid 27
    weight 28.899495
    pts "[(90, 94), (91, 94), (92, 94), (93, 94), (94, 94), (95, 94), (96, 94), (97, 94), (98, 94), (99, 94), (100, 94), (101, 94), (102, 94), (103, 94), (104, 94), (105, 93), (106, 92), (107, 92), (108, 91), (109, 91), (110, 91), (111, 90), (112, 89), (113, 88), (114, 88), (115, 87), (116, 87)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 27
    capa 28.899495
    conn 0
  ]
  edge [
    source 19
    target 24
    eid 28
    weight 27.384776
    pts "[(89, 97), (89, 98), (90, 99), (91, 99), (92, 100), (93, 101), (94, 102), (95, 103), (96, 104), (97, 104), (98, 105), (99, 106), (100, 106), (101, 106), (102, 107), (103, 108), (104, 109), (105, 109), (106, 110), (107, 110), (108, 111), (109, 111), (110, 111)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 23
    capa 27.384776
    conn 0
  ]
  edge [
    source 21
    target 25
    eid 29
    weight 16.899495
    pts "[(98, 74), (99, 74), (100, 74), (101, 75), (102, 75), (103, 76), (104, 76), (105, 77), (106, 78), (107, 78), (108, 79), (109, 79), (110, 80), (111, 81), (111, 82)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 15
    capa 16.899495
    conn 0
  ]
  edge [
    source 25
    target 27
    eid 30
    weight 3.828427
    pts "[(113, 83), (114, 83), (115, 84), (116, 85)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 4
    capa 3.828427
    conn 0
  ]
  edge [
    source 26
    target 30
    eid 31
    weight 61.384776
    pts "[(113, 24), (113, 25), (113, 26), (113, 27), (113, 28), (113, 29), (113, 30), (113, 31), (113, 32), (113, 33), (112, 34), (112, 35), (112, 36), (112, 37), (112, 38), (112, 39), (113, 40), (113, 41), (113, 42), (113, 43), (113, 44), (113, 45), (113, 46), (114, 47), (114, 48), (114, 49), (114, 50), (115, 51), (115, 52), (115, 53), (116, 54), (116, 55), (116, 56), (116, 57), (116, 58), (116, 59), (116, 60), (117, 61), (117, 62), (117, 63), (117, 64), (117, 65), (117, 66), (117, 67), (118, 68), (119, 69), (120, 70), (120, 71), (120, 72), (120, 73), (120, 74), (121, 75), (121, 76), (122, 77), (123, 78), (123, 79), (124, 80)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 57
    capa 61.384776
    conn 0
  ]
  edge [
    source 27
    target 28
    eid 32
    weight 0.000000
    pts "[(118, 86)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 28
    target 29
    eid 33
    weight 0.000000
    pts "[(121, 88)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 28
    target 30
    eid 34
    weight 1.000000
    pts "[(122, 84), (123, 84)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 2
    capa 1.000000
    conn 0
  ]
  edge [
    source 30
    target 31
    eid 35
    weight 0.000000
    pts "[(126, 84)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 1
    capa 0.000000
    conn 0
  ]
  edge [
    source 30
    target 32
    eid 36
    weight 9.242641
    pts "[(127, 80), (128, 80), (129, 79), (130, 79), (131, 78), (132, 78), (133, 77), (134, 77), (135, 77)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 9
    capa 9.242641
    conn 0
  ]
  edge [
    source 33
    target 35
    eid 37
    weight 16.727922
    pts "[(158, 107), (159, 106), (160, 105), (161, 105), (162, 105), (163, 104), (164, 103), (165, 102), (165, 101), (166, 100), (167, 100), (168, 99), (169, 98), (170, 97)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 14
    capa 16.727922
    conn 0
  ]
  edge [
    source 34
    target 35
    eid 38
    weight 30.727922
    pts "[(163, 126), (163, 125), (164, 124), (165, 123), (165, 122), (166, 121), (166, 120), (166, 119), (166, 118), (167, 117), (167, 116), (167, 115), (167, 114), (167, 113), (167, 112), (168, 111), (168, 110), (168, 109), (168, 108), (168, 107), (169, 106), (170, 105), (170, 104), (171, 103), (171, 102), (171, 101), (172, 100), (172, 99)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 28
    capa 30.727922
    conn 0
  ]
  edge [
    source 35
    target 36
    eid 39
    weight 13.485281
    pts "[(172, 95), (173, 94), (173, 93), (174, 92), (174, 91), (174, 90), (175, 89), (175, 88), (176, 87), (177, 86), (178, 85), (178, 84)]"
    graphics [
      width 1.41
      fill  "#00b8FF" 
      targetArrow  "none" 
    ]
    lgth 12
    capa 13.485281
    conn 0
  ]
]