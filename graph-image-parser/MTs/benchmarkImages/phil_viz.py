import cv2 as cv
from matplotlib import pyplot as plt
import matplotlib
from matplotlib.cm import get_cmap
import networkx as nx
import numpy as np
from phil_transformations import rotateM

'''
Plot identified filaments in the graph, with the original image as backgroundPlot identified filaments in the graph, with the original image as background
Also plots the ids of edges, in cyan for normal edges, and in magenta for edges that where identified as center nodes in draw_graph
* saveimg True requires at least that name is not None
* folder by default points to the cwd (current working directory)
'''
def graphPlusBg(graph: nx.Graph, segmentedImage: np.array, centerNodes: list, saveimg: bool=True, 
        fontsze: int=10, mrkrsze: int=9, linesize: int=2, normal_edge_clr: str="cyan",
        center_edge_color: str="magenta", folder: str=".", name: str=None) -> None:
    plt.figure(figsize=(64, 48))
    # draw edges by pts
    for (s,e) in graph.edges():
        ps = graph[s][e]['pts']
        txt1, txt2 = graph[s][e]['center']
        for pnt in graph[s][e]['pts']:
            t1,t2 = rotateM((pnt))
            plt.plot(t1, abs(t2), marker='.', markersize=linesize, color="orange")
        if s in centerNodes or e in centerNodes:
            plt.text(txt1, abs(txt2), str(graph[s][e]['eid']), fontsize=fontsze, color = center_edge_color)
        else:
            plt.text(txt1, abs(txt2), str(graph[s][e]['eid']), fontsize=fontsze, color = normal_edge_clr)

    # draw node by o
    ps = np.array([graph.nodes[i]['o'] for i in graph.nodes])

    for pnt in ps:
        t1,t2 = rotateM((pnt))
        plt.plot(t1, abs(t2), color='red', marker='o', markersize=mrkrsze)

    #ax.plot(0,0,color='yellow',marker='o')
    plt.axis("off")
    inv_image = cv.bitwise_not(segmentedImage)
    plt.imshow(inv_image,plt.cm.gray,zorder=-1)
    if (saveimg and name):
        plt.savefig(folder + "/" + name + "-somaEdges.png")
    elif (saveimg and not name):
        print("Cant save image without at least a name. A folder is also suggested")
        
'''
Receives the proposed filaments by a filament individualization algorithm
Returns a dictionary indicating all the ants where each edge belongs to, and a dictionary
with colors that were assigned to each ant.
'''
def edge2draw(proposedFils: dict, colormap_name :str="hsv", second_cmap_name: str="tab20b") -> tuple[dict, dict]:
    #random.seed(13)
    edge2Ants = {}
    for key,values in proposedFils.items():
        for val in values:
            if val in edge2Ants.keys():
                edge2Ants[val].append(key)
            else:
                edge2Ants[val] = [key]

    colors = {}
    cmap = get_cmap(colormap_name, len(proposedFils.keys())+5)
    cmap_rev = get_cmap(second_cmap_name, len(proposedFils.keys())+5)
    for counter, i in enumerate(proposedFils.keys()):
        if counter % 2 == 0:
            colors[i] = cmap(counter)
        else:
            colors[i] = cmap_rev(counter)
        
    return edge2Ants, colors

'''
Paints the graph with the results from a individualization algorithm, stored in proposedFils_dict
Returns colorDict so the same colors can be used in further iterations or other drawings
'''
def drawResults(graph: nx.Graph, version: str, segmentedImage: np.array,
                nodes2edge: dict, folder: str, name: str, edge2Ants: dict, 
                colors: dict, save_fig: bool=False, extended: bool=False,
                mrkrsz: int=9, fntsz: int=9, linesize: int=32,
                bg: bool=True) -> dict:

    plt.figure(figsize=(28, 26))
    # draw edges by pts
    for (s,e) in graph.edges():
        ps = graph[s][e]['pts']
        txt1, txt2 = graph[s][e]['center']
        #midpt = len(graph[s][e]['pts']) - 1
        #tx1, tx2  = rotateM((graph[s][e]['pts'][int(midpt/2)]))

        edge = nodes2edge[(s,e)]       
        if edge not in edge2Ants:
            continue
        try:
            # if ant in edge2Ants[edge] belongs in colors, it means it must be drawn
            if len(edge2Ants[edge]) > 1:
                if len(set(colors.keys()).intersection(set(edge2Ants[edge]))) > 0:
                    if bg:
                        edgeColor = 'silver'
                    else:
                        edgeColor = 'grey'
                else:
                    # If non of the ants in edge2Ants[edge] was selected, skip
                    edgeColor = 'black'
                    continue
            else:
                edgeColor = colors[edge2Ants[edge][0]]
        except:
            # If the ant has no color, skip
            #print(f"Ant {edge2Ants[edge]} has no color assigned. Skipping")
            continue
        mrkr = '.'

        for pnt in graph[s][e]['pts']:
            t1,t2 = rotateM((pnt))
            plt.plot(t1, abs(t2), marker=mrkr, markersize=linesize, color=edgeColor)
        #font 9 for len(filaments) > 7. Else: font 14
        if extended:
            plt.text(txt1, abs(txt2), str(edge2Ants[edge]) + ": e" + str(edge), fontsize=fntsz,
                     color = 'purple', backgroundcolor='white')
    # draw node by o
    ps = np.array([graph.nodes[i]['o'] for i in graph.nodes])

    for pnt in ps:
        t1,t2 = rotateM((pnt))
        plt.plot(t1, abs(t2), color='red', marker='o', markersize=mrkrsz)

    #ax.plot(0,0,color='yellow',marker='o')
    plt.axis("off")
    if bg:
        inv_image = cv.bitwise_not(segmentedImage)
        plt.imshow(inv_image,plt.cm.gray,zorder=-1)
    else:
        plt.gca().invert_yaxis()
    if save_fig:
        # Update version automatically
        if extended:
            version += "-labeled-ants"
        else:
            version += "-no-labels-ants"
        plt.savefig(f"{folder}/{name}-{version}.png")
    return colors
    
'''
Draw define style graph, no curves
'''
def drawDefineGraph(graph: nx.Graph) -> tuple[list, list, dict]:
    #plt.figure(figsize=(28, 26))
    xCoords = []
    yCoords = []
    nodeListX = []
    nodeListY = []
    edge2node = {}
    for i in graph.nodes:
        for n in graph.neighbors(i):
            #print(i,n)
            tm1,tm2 = rotateM(graph.nodes[i]['o'])
            nodeListX.append(tm1)
            nodeListY.append(tm2)
            edge2node[graph[i][n]['eid']] = [i,n]
            tm1,tm2 = rotateM(graph.nodes[n]['o'])
            nodeListX.append(tm1)
            nodeListY.append(tm2)

            xCoords.append(nodeListX.copy())
            yCoords.append(nodeListY.copy())
            #plt.plot(nodeListX, nodeListY, color='black')
            nodeListX.clear()
            nodeListY.clear()
    return xCoords, yCoords, edge2node

'''
Plot the exact matched filaments proposed by DeFiNe
'''
def drawDefineRes(exact_fils_define: dict, defineAngle: int, graph: nx.Graph, cmap: matplotlib.cm, 
        save: bool=False, folder: str=".") -> None:
    # Get bg graph from drawDefineGraph
    xpts, ypts, edge2node = drawDefineGraph(graph)
    nodeListX = []
    nodeListY = []
    plt.figure(figsize=(28, 26))
    plt.axis("off")
    
    # xpts and ypts contain the pairs for the bg graph
    for counter, pts in enumerate(xpts):
        plt.plot(pts, ypts[counter], color='black',lw=16, ls=":")
    # colors
    colors_define = {}
    for counter, i in enumerate(exact_fils_define.keys()):
        colors_define[i] = cmap(counter)
    #print(colors_define)
    # iterate over the content of the dictionary of exact matches that is res
    for key,value in exact_fils_define.items():
        # generate random color
        clr = colors_define[key]#(round(random.random(),2), round(random.random(),2), round(random.random(),2))
        # Paint each edge of an exact match filament
        for arista in value:
            node1, node2 = edge2node[arista]
            tm1,tm2 = rotateM(graph.nodes[node1]['o'])
            nodeListX.append(tm1)
            nodeListY.append(tm2)
            tm1,tm2 = rotateM(graph.nodes[node2]['o'])
            nodeListX.append(tm1)
            nodeListY.append(tm2)
            plt.plot(nodeListX, nodeListY, color=clr, lw=16)
            nodeListX.clear()
            nodeListY.clear()
    if save:
        print(f"{folder}/DeFiNeExactMatch-{defineAngle}.png")
        plt.savefig(f"{folder}/DeFiNeExactMatch-{defineAngle}.png")

'''
Draws graph with colored and labeled nodes and edges. Removes simple self-loops
Edges and node points go thru rotateM as the Y axis is inverted when coming from an image

* bg_color is passed to savefig. None will render a transparent background

Returns nodes2edge (Maps pair of nodes to each edge) and nodesByPos (maps a node position x,y to a node id)

Note: nx.draw_networkx(graph) can do something similar, but it can take longer
'''
def draw_graph(graph: nx.Graph, plt_title: str=None, edge_color: str="green", edge_text_color: str="purple",
            node_color: str="red", end_node_color: str="cyan", node_text_color: str="black", edge_marker: str=",",
            node_marker: str="o", node_size: int = 2, edge_size: int = 2, bg_color: str="white",
            save_fig: bool=False, folder: str=".", name: str=None) -> tuple[dict, dict]:

    nodes2edge = {}
    fig, ax = plt.subplots(1, 1, figsize=(28, 26))
    #fig.gca().invert_yaxis()

    # draw edges by pts
    i = 0
    for (s,e) in graph.edges():
        # Delete self loop edges
        if (s == e):
            try:
                graph.remove_edge(s,e)
            except:
                print("Couldnt remove self loop %d %d" %(s,e))
            continue
        graph[s][e]['eid'] = i
        # Mid point is used to get a location for the edge id
        midpt = len(graph[s][e]['pts']) - 1
        tx1, tx2  = rotateM((graph[s][e]['pts'][int(midpt/2)]))
        graph[s][e]['center'] = [tx1,tx2]

        for pnt in graph[s][e]['pts']:
            # All points need rotation, specially on Y axis
            t1,t2 = rotateM((pnt))
            ax.plot(t1, t2, color=edge_color, marker=edge_marker, markersize=edge_size, zorder=1)
        ax.text(tx1, tx2, "{:d}".format(i), fontsize=8, color=edge_text_color)
        nodes2edge[(s,e)] = i 
        i += 1

    # draw node by o
    nodesByPos = {}
    ps = []
    # Create a dict that asociates a number id with a node position
    for i in graph.nodes():
        a,b = graph.nodes[i]['o']
        ps.append([a,b])
        nodesByPos[(a,b)] = i
    #print(ps)
    ps = np.array(ps)
    for pnt in ps:
        a,b = pnt
        t1,t2 = rotateM((pnt))
        
        ax.plot(t1, t2, color=node_color, marker=node_marker, markersize=node_size, zorder=1)
        nodeTxt = nodesByPos[(a,b)]
        # Paint end nodes with different color
        if graph.degree[nodeTxt] == 1:
            ax.text(t1, t2, "{:d}".format(nodeTxt), fontsize=8, color=end_node_color)
        else:
            ax.text(t1, t2, "{:d}".format(nodeTxt), fontsize=8, color=node_text_color)

    #ax.plot(0,0,'bo')
    ax.axis("off")
    #ax.imshow(outSkel,zorder=0)#,extent=[0,502,292,0])
    # title and show
    if plt_title:
        ax.set_title(plt_title)
    if save_fig:
        if name:
            if bg_color:
                fig.savefig(folder + "/" + name + "_graph_labeled_nodes.png", facecolor=bg_color)
            else:
                fig.savefig(folder + "/" + name + "_graph_labeled_nodes.png")
            print(folder + "/" + name + "_graph_labeled_nodes.png")
        else:
            print("Cant save graph_labeled_nodes without a name. A folder is also suggested.")
    
    return nodes2edge, nodesByPos
