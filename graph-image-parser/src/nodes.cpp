#include "nodes.hpp"
#include "cluster.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
//#include <opencv2/highgui/highgui.hpp> // cv::imwrite
#include <cmath>

void freeEdges(std::pair<const int, Arista*> &element){
    delete element.second;
}

/* create a node per cluster, in its mass center*/
void initNodeinCluster(std::pair<const int, Cluster*> &element){
    /* skip if node is marked as merged */
    element.second->node.id = element.first;
    if (element.second->clusterType == -2 || element.second->clusterType == -3){return;}
    element.second->node.color = element.second->color;
    element.second->node.coord = element.second->massCenter;
    element.second->node.degree = 0;
    element.second->node.nType = element.second->clusterType;
    element.second->node.parent = element.second;
    /* paint nodes after merger	*/
    cv::circle(frame, element.second->massCenter, 1, cv::Scalar(0,0,255));
}

Arista* createEdge(int edgeid){
    Arista* newEdge = new Arista();
    newEdge->id = edgeid;
    newEdge->width = 0;
    newEdge->angle = 0;
    newEdge->color = cv::Vec3b(0,0,255); // red (normal edge) by default
    newEdge->endEdge = false;
    return newEdge;
}

/* for each cluster, iterate over its neighbors. Create a new Edge for each neighbor. 
 * Add to own edgeMap and neighbors edgeMap */
void populateEdges(std::pair<const int, Cluster*> &element){
    int i;
    double edgeWidth = 0;
    cv::Point tmp_pt;
    //Nodo philEnd1, philEnd2;
    std::unordered_map<int, Arista*>::iterator initialEdge;
    /* if cluster was merged, not assigned or other negative case (-2 clusterType), return */
    if (element.second->clusterType < 0){return;}

    for (std::unordered_map<int, Cluster*>::iterator it = element.second->neighboursMap.begin(); it != element.second->neighboursMap.end(); it++ ){
	// it->second has a clusterType < 0, skip
	if (it->second->clusterType < 0){continue;}
	/* Add the neighbors clusterid as int/key. Add only once to each map */
	if (element.second->edgesMap.count(it->first) == 0){ // count in uMap can be 1 or 0 only
	    /* line iterator. Dont need to compare first or last pixel as they are massCenters and are black (0) after inversion. 
             * At this point, image will be inverted, no matter how it was originnaly */
            cv::LineIterator lit(frameGray, element.second->node.coord, it->second->node.coord, 8);
	    /*
	    if (lit.count - 2 <= cellRestrictions->connectivityThresh && it->second->neighboursMap.size() > 2){
		continue;
	    }*/
            lit++; // Start from 2nd line member. 1st one is always starting node, which is always black
            for (i = 1; i < lit.count - 1; i++, lit++){
                /* if edge goes thru white pixels, fix with nearest black pixel that is not the next or previous pixel */
                if (frameGray.at<uchar>(lit.pos()) == 255){
                    // Select one of the 2 neighbors of lit.pos() - 1
                    tmp_pt = lit.pos();
		    try { // src.at(i,j) is using (i,j) as (row,column) but Point(x,y) is using (x,y) as (column,row) REMEMBER
			if (frameGray.at<uchar>(tmp_pt.y, tmp_pt.x - 1) < neighborThreshold){
                            tmp_pt = cv::Point(tmp_pt.x - 1, tmp_pt.y);
			} else if (frameGray.at<uchar>(tmp_pt.y - 1, tmp_pt.x) < neighborThreshold){
                            tmp_pt = cv::Point(tmp_pt.x, tmp_pt.y - 1);
			}
			edgeWidth += (255 - frameGray.at<uchar>(tmp_pt));
		    } catch (std::exception &e) {
			fprintf(stderr, "x %d y %d cols %d rows %d  err: %s\n", tmp_pt.x, tmp_pt.y, frameGray.cols, frameGray.rows, e.what());
			continue;
		    }
                    // TODO: Store in vector
                } else {
                    edgeWidth += (255 - frameGray.at<uchar>(lit.pos()));
                }
            }

	    /* create new edge as no edge exist between this node and the neighbor node */
	    edgePointers[edgeID] = createEdge(edgeID);
            /* Add nodes to new edge */
            edgePointers[edgeID]->end1 = &element.second->node;
            edgePointers[edgeID]->end2 = &it->second->node;
            edgePointers[edgeID]->length = euclideanDist(element.second->massCenter, it->second->massCenter);
	    edgePointers[edgeID]->angle = angle2Points(element.second->node.coord, it->second->node.coord);
	    /* Divide total sum of pixels by number of pixels */
	    edgePointers[edgeID]->width += edgeWidth + (255*2 - frameGray.at<uchar>(element.second->node.coord) - frameGray.at<uchar>(it->second->node.coord));
	    edgePointers[edgeID]->width /= (lit.count - 2.0);
	    /* for each edge, increase this node and neighbors node degree */
            element.second->node.degree++;
            it->second->node.degree++;
	    /* add to map */
	    element.second->edgesMap[it->first] = edgePointers[edgeID];
	    it->second->edgesMap[element.first] = edgePointers[edgeID];
	    //if (debug){
	    fprintf(metadataFile,"%d\t %d (%.4f)  %d (%.4f) %.4f %.4f %.4f %.4f\t %.4f %.4f \t%.4f\n", edgeID, element.first, element.second->angle, it->first, it->second->angle, element.second->node.coord.x, element.second->node.coord.y, it->second->node.coord.x, it->second->node.coord.y, edgePointers[edgeID]->length, edgePointers[edgeID]->width, edgePointers[edgeID]->angle);
            //}
	    /* increase numInitialSegments if one of the nodes has just 1 neighbor */
	    if (element.second->clusterType == 1 || element.second->clusterType == 4 || it->second->clusterType == 1 || it->second->clusterType == 4){
		edgePointers[edgeID]->endEdge = true;
		cellRestrictions->endEdges[edgeID] = 0;
	    }
	    /* notify advance */
	    if (debug == 0 && edgeID % 100 == 0){printf(" %d ", edgeID);}
	    /* Add to edgePointersByNode*/
	    edgePointersByNode[std::make_pair(element.second->node.id, it->second->node.id)] = edgePointers[edgeID];
	    edgePointersByNode[std::make_pair(it->second->node.id, element.second->node.id)] = edgePointers[edgeID];
	    edgeID++;
	}
    }
}

/* cos(theta) = (vec1 dot vec2)/ (||vec1|| * ||vec2||)
 * Recibes initial, middle (cluster massCenter) and end nodes
 * Returns in degree (180 - angle)*/
double angleBetweenVecs(Nodo& initial, Nodo& middle, Nodo& end){
    double magVec1, magVec2, theta;
    cv::Point2f transformedInit, transformedEnd;
    magVec1 = euclideanDist(initial.coord, middle.coord); //std::sqrt(initial.coord.x*initial.coord.x + initial.coord.y*initial.coord.y);
    magVec2 = euclideanDist(end.coord, middle.coord);//std::sqrt(end.coord.x*end.coord.x + end.coord.y*end.coord.y);
    //printf("mV1 %f mV2 %f\n", magVec1, magVec2);
    magVec2 *= magVec1;
    // The vectors "start" from middle node, so we substract mid.x and mid.y from initial and end nodes This is just setting the center in middle node
    transformedInit = initial.coord - middle.coord;
    transformedEnd = end.coord - middle.coord;
    theta = ((transformedInit.x * transformedEnd.x) + (transformedInit.y * transformedEnd.y))/magVec2 ;
    if (debug == 3){printf("iCx %f eCx %f iCy %f eCy %f mV21 %f theta %f radians %f\n", transformedInit.x, transformedEnd.x, transformedInit.y, transformedEnd.y, magVec2, theta, acos(theta));}
    if (abs(theta) > 1.0 && abs(theta) < 1.1) theta = -1.0;
    return 180 - fabs(acos(theta) * 180.0 / M_PI);
}
// Same as above, but with direct points. angleBetweenVecs{2} returns a degree within [0,180]
double angleBetweenVecs2(cv::Point2f initial, cv::Point2f middle, cv::Point2f end){
    double magVec1, magVec2, theta;
    cv::Point2f transformedInit, transformedEnd;
    magVec1 = euclideanDist(initial, middle); //std::sqrt(initial.coord.x*initial.coord.x + initial.coord.y*initial.coord.y);
    magVec2 = euclideanDist(end, middle);//std::sqrt(end.coord.x*end.coord.x + end.coord.y*end.coord.y);
    //printf("mV1 %f mV2 %f\n", magVec1, magVec2);
    magVec2 *= magVec1;
    // The vectors "start" from middle node, so we substract mid.x and mid.y from initial and end nodes This is just setting the center in middle node
    transformedInit = initial - middle;
    transformedEnd = end - middle;
    theta = ((transformedInit.x * transformedEnd.x) + (transformedInit.y * transformedEnd.y))/magVec2 ;
    if (debug == 3){printf("iCx %f eCx %f iCy %f eCy %f mV21 %f theta %f radians %f\n", transformedInit.x, transformedEnd.x, transformedInit.y, transformedEnd.y, magVec2, theta, acos(theta));}
    if (abs(theta) > 1.0 && abs(theta) < 1.1) theta = -1.0;
    return 180 - fabs(acos(theta) * 180.0 / M_PI);
}

double euclideanDist(cv::Point2f& a, cv::Point2f& b){
    cv::Point2f diff = a - b;
    return std::sqrt(diff.x*diff.x + diff.y*diff.y);
}

/* Calculate angle between 3 points using atan2, setting start of the system in initial point */
double angleBetweenSegs(cv::Point initial, cv::Point middle, cv::Point end){
    double theta;
    /* The Segments coordinate system is based on the initial segment between initial and middle points. This is just setting the center in middle node*/
    if (initial == end || middle == initial){return 180;}
    theta = atan2(end.y - initial.y, end.x - initial.x) - atan2(middle.y - initial.y, middle.x - initial.x);
    if (debug == 4){
	printf("i (%d %d) m (%d %d) e (%d %d) theta %f angle %f\n", initial.x, initial.y, middle.x, middle.y, end.x, end.y, theta, theta * 180.0 / M_PI);
    }
    return theta * 180.0 / M_PI;
}

/* Paint short edges blue, normal edges red in graph and frameGraph Mats */
void paintEdges(std::pair<const int, Arista*> &element){
    //if (element.second == nullptr) {return;}
    int circleThickness = 2, lineThickness = 1;
    cv::line(graph, element.second->end1->coord, element.second->end2->coord, element.second->color);
    //cv::Scalar c1(element.second->end1->color[0], element.second->end1->color[1], element.second->end1->color[2]);
    //cv::Scalar c2(element.second->end2->color[0], element.second->end2->color[1], element.second->end2->color[2]);
    // nodes
    cv::Scalar colr(0,0,255);
    cv::circle(graph, element.second->end1->coord, circleThickness, colr);
    cv::circle(graph, element.second->end2->coord, circleThickness, colr);
    cv::circle(frameGraph, element.second->end1->coord, circleThickness, colr);
    cv::circle(frameGraph, element.second->end2->coord, circleThickness, colr);
    // blue
    cv::line(graph, element.second->end1->coord, element.second->end2->coord, cv::Scalar(255,0,0),lineThickness);
    cv::line(frameGraph, element.second->end1->coord, element.second->end2->coord, cv::Scalar(255,0,0),lineThickness);
    /* edge label/ids */
    //cv::putText(graph,std::to_string(element.first), element.second->end1->coord, cv::FONT_HERSHEY_SIMPLEX, 0.2, cv::Scalar(255,0,255), 1, cv::LINE_AA );
    //cv::putText(frameGraph,std::to_string(element.first), element.second->end1->coord, cv::FONT_HERSHEY_SIMPLEX, 0.2, cv::Scalar(255,0,255), 1, cv::LINE_AA);
}

double angle2Points(const cv::Point2f& v1, const cv::Point2f& v2){
    return atan2((v2.y - v1.y),(v2.x - v1.x)) * 180.0 / CV_PI;
}

/* Calculate nodes that are the center of angles in inter quality range. */
void nodeNeighborAngle(std::pair<const int, Cluster*> &element){
    Nodo *startNode, *endNode;
    double vecAngle;
    std::pair<int,int> angleOf2Vecs, angleOf2VecsReverse;
    // Dont waste time with nodes with less than 2 edges. Except if they are nodes from networkX
    if (element.second->edgesMap.size() < 2 && element.second->nxId == -1){return;}
    // Also skip nX nodes that are isolated
    if (element.second->edgesMap.size() == 0 && element.second->nxId > -1){return;}

    std::unordered_map<int,Arista*>::iterator neighborEdge = element.second->edgesMap.begin(), secondNeighborEdge;
    secondNeighborEdge = std::next(neighborEdge);
    // iterate over cluster->edgesMap
    while (secondNeighborEdge != element.second->edgesMap.end()){
	// element.first is the common node between this edges
	endNode = (neighborEdge->second->end1->id == element.first) ? neighborEdge->second->end2 : neighborEdge->second->end1;
	startNode = (secondNeighborEdge->second->end1->id == element.first) ? secondNeighborEdge->second->end2 : secondNeighborEdge->second->end1;
	//
	angleOf2Vecs = std::make_pair(startNode->id,endNode->id);
	angleOf2VecsReverse = std::make_pair(endNode->id, startNode->id);
	// calculate vecAngle and store in neighborNodesAngles
	vecAngle = angleBetweenVecs(*startNode, element.second->node, *endNode); // startNode is local/myopic start
	// if vecAngle is in ]theta, 90[, then its a starting node
	if (fabs(vecAngle) > cellRestrictions->angle && fabs(vecAngle) < 90){
	    // Dont asign type 4 to previously assigned nType 5 when cell is neuron
	    if (element.second->node.nType != 5 && element.second->node.nType != 6){element.second->node.nType = 4;}
	    // Add both edges to map
	    cellRestrictions->interQuaEdges[edgePointersByNode[std::make_pair(startNode->id, element.second->node.id)]->id] = 0;
	    cellRestrictions->interQuaEdges[edgePointersByNode[std::make_pair(element.second->node.id, endNode->id)]->id] = 0;
	}
	element.second->node.neighborNodesAngles[angleOf2Vecs] = vecAngle;
	element.second->node.neighborNodesAngles[angleOf2VecsReverse] = vecAngle;
	// Reset secondNeighborEdge and advance neighborEdge when secondNeighborEdge reaches --element.second->edgesMap.end()
	if (std::next(secondNeighborEdge) == element.second->edgesMap.end()){
	    neighborEdge++;
	    if (std::next(neighborEdge) == element.second->edgesMap.end()){break;}
	    secondNeighborEdge = std::next(neighborEdge);
	} else {
	    secondNeighborEdge++;
	}
    }
}
