
seed=6821 #0 10 1271 3389 6821
#destinyFolder="v0.5_skel_s$seed-field3_t0_2cellBcrop_filtered_1"
#destinyFolder="v0.5_noskel_s$seed-field3_t0_2cellBcrop_filtered_1"
#destinyFolder="v0.5_skel_s$seed-50_ROIs_Spinning_Marchantia"
#destinyFolder="v0.5_noskel_s$seed-50_ROIs_Spinning_Marchantia"
#destinyFolder="v0.5_skel_s$seed-field3_t0_2cellBcrop_filtered_2"
#destinyFolder="v0.5_noskel_s$seed-field3_t0_2cellBcrop_filtered_2"

#destinyFolder="v0.5_skel_s$seed-Synth-QuantitativeIFS-Fig7"
#destinyFolder="v0.56_skel_s$seed-Synth-QuantitativeIFS-Fig7-Sint20deg"

#destinyFolder="v0.5_skel_s$seed-Synth-DefineFig1"
destinyFolder="v0.56_skel_s$seed-Synth-DefineFig1_SintMAD2"

#destinyFolder="v0.55_skel_s$seed-Porta18-3a1-neuron"
#destinyFolder="v0.55_skel_s$seed-Porta10-5b-neuron"
#destinyFolder="v0.55_skel_s$seed-Porta6-4a1-neuron"
#destinyFolder="v0.55_skel_s$seed-Synth-red0"
#destinyFolder="v0.55_skel_s$seed-Synth-arbol0"
#destinyFolder="v0.55_skel_s$seed-Synth-arbol1"
echo $destinyFolder

mkdir -p $(pwd)/output/$destinyFolder/
mv /tmp/*png.log output/$destinyFolder/
mv /tmp/*Pruned* output/$destinyFolder/
mv /tmp/*Missing* output/$destinyFolder/
mv /tmp/*Ant* output/$destinyFolder/
