#!/usr/bin/env python3
#1st test if data follows normal distribution. Needs at least 20 elements between both tests for kurtosistest
# data in https://docs.google.com/spreadsheets/d/1iQdA7aS5sU3psBLtVDDKv-LjtbE5jeaX6wS1iE2bTOc/edit#gid=580255540 
import numpy as np
from scipy import stats
import sys

def normalTest(data1, data2):
    #t_test_data = np.concatenate((define, phil))
    t_test_data = np.concatenate((data1, data2))
    rejectH0 = False
    k2, p = stats.normaltest(t_test_data)
    print("Test 1: Test if sample comes from a normal distribution")
    print(k2,p)

    if p < 1e-3:
        print("The null hypothesis can be rejected -> Sample doesn't come from a normal distribution")
        rejectH0 = True
    else:
        print("The null hypothesis cannot be rejected -> Sample comes from a normal distribution")
    return rejectH0

def t_test_ind_2sample(data1, data2):
    #rejectH0 = False
    #tStatistic, pValue = stats.ttest_rel(define,phil)
    # Independent 2-sample/paired Student t-test
    #tStat, pVal = stats.ttest_ind(define,phil)
    tStat, pVal = stats.ttest_ind(data1, data2)

    print("Test 2, Student t-test: Test if there is no significal statistical difference")
    print(tStat, pVal)

    if pVal < 0.05:
        print("The null hypothesis can be rejected -> there is significal statistical difference")
    #    rejectH0 = True
    else:
        print("The null hypothesis cannot be rejected -> there is no significal statistical difference")
    #return pVal, rejectH0

def KS_Test(data1, data2):
    #rejectH0 = False
    print("Test 3, K-S Test: The 2 independent samples are drawn from the same continuous distribution")
    #ksStat, pValue = stats.ks_2samp(define, phil)
    ksStat, pValue = stats.ks_2samp(data1, data2)
    print(ksStat, pValue)

    if pValue < 0.05:
        print("The null hypothesis can be rejected -> The 2 independent samples are not drawn from the same continuous distribution")
        #rejectH0 = True
    else:
        print("The null hypothesis cannot be rejected -> The 2 independent samples are drawn from the same continuous distribution")
    #return pVal, rejectH0

define = [12,16,5,5,7]
phil = [12,15,5,6.2,9.2]
gTruth = [12,12,5,6,5]

print ("Define vs Phil")
rejectH0 = normalTest(define, phil)
if not rejectH0:
    t_test_ind_2sample(define, phil)
KS_Test(define, phil)

print()
print("gTruth vs Phil")
rejectH0 = normalTest(gTruth, phil)
if not rejectH0:
    t_test_ind_2sample(gTruth, phil)
KS_Test(gTruth, phil)
