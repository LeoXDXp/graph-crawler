#include "ants.hpp"
#include"graph.hpp"
#include "nodes.hpp" // angleBetweenVecs, angleBetweenSegs
#include <opencv2/highgui/highgui.hpp> // cv::imwrite
#include <algorithm>
//#include <random>
#include <string>
#include <cmath> // M_PI

/* Global vars of ant.cpp */
Colony *antColony;
PGconn  *conn;

/* Initialize ants */ 
void antColonyInit(){
    // Init db conn or exit
    conn = PQconnectdb("dbname='phils' user='phil' host='127.0.0.1' password='MyThesisIsGonnaKillM3!'");
    if (PQstatus(conn) == CONNECTION_BAD){
	fprintf(stderr, "Connection to database failed: %s\n", PQerrorMessage(conn));
	exit_nicely(conn);
	return;
    }// Clear any data with the same filename
    deletePreviousDB(conn);
    // Insert all valid nodes to the DB
    /*for (std::unordered_map<int, Cluster*>::iterator it = clusterPointers.begin(); it!= clusterPointers.end(); it++){
	if (it->second->clusterType >= 0){
	    insertNodeDB(conn, it->second->node);
	}
    }*/

    int i, startingEdge;
    bool deletedDup = false;
    //Ant *debuAnt = nullptr;
    //Arista *debuEdge = nullptr;
    antColony = new Colony(); // should initialize ants = 0
    antColony->edges = edgeID;
    antColony->alpha = 1.0; //Exploitation parameter, sets how the ants are attracted to pheromone concentration.
    antColony->beta = 1.0; // Exploration parameter, sets how the ants are more attracted to try out shorter paths.
    antColony->pheromoneBFactor = 0.5; // 50% increase or penalty for edges in bannedPathsPherom (pheromoneB aka gamma). 50% is based on SAP paper
    //antColony->notAdvancingFactor = 1; //0.5; // aka delta
    antColony->maxThetaScore = 2.0; // aka MAX_SCORE
    antColony->minGoodQualityFactor = 0.5;
    //antColony->coveredEdges = new int[antColony->edges](); // () Adds value-initialization to zero
    for (int j = 0; j < antColony->edges; j++){
	antColony->pendingEdges[j] = 1;
	antColony->coveredEdges.push_back(0);
    }

    fprintf(metadataFile,"endEdges: %ld interQuaEdges: %ld totalEdges %d startEdges/totalEdges %.4f\n", cellRestrictions->endEdges.size(), cellRestrictions->interQuaEdges.size(), antColony->edges, (double)(cellRestrictions->endEdges.size() + cellRestrictions->interQuaEdges.size())/antColony->edges);
    if (debug){
	fprintf(metadataFile,"endEdges\n");
	for (std::unordered_map<int,int>::iterator initEdge = cellRestrictions->endEdges.begin(); initEdge != cellRestrictions->endEdges.end(); ++initEdge){
	    fprintf(metadataFile,"%d(%d %d) ", initEdge->first, edgePointers[initEdge->first]->end1->id, edgePointers[initEdge->first]->end2->id);
	}
	fprintf(metadataFile,"\n interQuaEdges:\n");
	for (std::unordered_map<int,int>::iterator initEdge = cellRestrictions->interQuaEdges.begin(); initEdge != cellRestrictions->interQuaEdges.end(); ++initEdge){
            fprintf(metadataFile,"%d(%d %d)\n", initEdge->first, edgePointers[initEdge->first]->end1->id, edgePointers[initEdge->first]->end2->id);
        }
	fprintf(metadataFile,"\nantID | length | width | edgeCount | pathQ | startNode-endNode (edgeID) <prob> [segID] [pherom] | edge{Length|angle} | vecAngle |  massCenter |\t eigenVal<1,2> | avgAngle\n");
    }
    // Initialize ants
    i = 0;
    // Iterate until all edges have been covered or there are no more nodes to spawn ants from
    // or until the number of ants spawned is greater than twice the number of edges (non-convergence limit) 
    while (antColony->pendingEdges.size() > 0 && i < 4*antColony->edges){ // i < 2*antColony->edges
	//if no more startingEdges but covering not complete, delete the worst ant. In case of tie, delete random one?
	// Neuron and synth case
	if ((cellRestrictions->cellIdentifier <= 1) && (cellRestrictions->endEdges.size() <= 0)){
	    if (!deletedDup){ // Compact only once
		deletedDup = true;
		globalEval(); // Compare ants, delete duplicated ones, reduce goodAnts.size()
	    } else {
		break; //Exit ant path construction loop
	    }
	// MT and other cells case
	} else if ((cellRestrictions->endEdges.size() + cellRestrictions->interQuaEdges.size() <= 0)) {
	    fprintf(stdout, "Max iters reached. GoodAnts before globalEval: %ld. Remaining endEdges %ld, interQuaEdges %ld\n", antColony->goodAnts.size(), cellRestrictions->endEdges.size(), cellRestrictions->interQuaEdges.size());
	    if (!deletedDup){ // Compact only once
		deletedDup = true;
		globalEval(); // Compare ants, delete duplicated ones, reduce goodAnts.size()
		// Add non-interQuality edges with 0 assigned ants to interQuaEdges
		cellRestrictions->interQuaEdges.insert(antColony->pendingEdges.begin(), antColony->pendingEdges.end());
	    } else {
		break; //Exit ant path construction loop
	    }
	}
	// 1st Select a edge from endEdges or interQuaEdges, and then one of its nodes to start from
    	antColony->colonyMap[i] = new Ant();
    	antColony->colonyMap[i]->id = i;
	antColony->colonyMap[i]->colonyPtr = antColony;
	//if (debug == 4){debuAnt = antColony->colonyMap[i];}
	// Ant Color
	antColony->colonyMap[i]->color = cv::Scalar(rand() % 200, rand() % 250, rand() % 250);
	// Initialize prevSeg
	antColony->colonyMap[i]->prevSeg = 0;

	// Get startingEdge for each ant
	startingEdge = antStartingEdge(antColony->colonyMap[i]);
	if (startingEdge < 0){
	    // Delete this non used ant
	    delete antColony->colonyMap[i];
	    antColony->colonyMap.erase(i);
	    fprintf(stdout, "No more startingEdges available.\n");
	    break;
	}
	//if (debug == 4){debuEdge = edgePointers[startingEdge];}
	// Store the id of the startingEdge within the ant's structure
	antColony->colonyMap[i]->firstEdgeID = startingEdge;
	// Init 1st segment of each ant
	antColony->colonyMap[i]->segs.push_back(insertSegmentDB(conn, *antColony->colonyMap[i]->startNode, *antColony->colonyMap[i]->endNode));
	antColony->colonyMap[i]->internalSegs[antColony->colonyMap[i]->segs.back()] = new Segment();
	antColony->colonyMap[i]->internalSegs[antColony->colonyMap[i]->segs.back()]->dbId = antColony->colonyMap[i]->segs.back();
	antColony->colonyMap[i]->internalSegs[antColony->colonyMap[i]->segs.back()]->length = edgePointers[startingEdge]->length;
	antColony->colonyMap[i]->internalSegs[antColony->colonyMap[i]->segs.back()]->startNode = antColony->colonyMap[i]->startNode;
	antColony->colonyMap[i]->internalSegs[antColony->colonyMap[i]->segs.back()]->ogStartNode = antColony->colonyMap[i]->startNode;
	antColony->colonyMap[i]->internalSegs[antColony->colonyMap[i]->segs.back()]->endNode = antColony->colonyMap[i]->endNode;
	antColony->colonyMap[i]->internalSegs[antColony->colonyMap[i]->segs.back()]->nodes[antColony->colonyMap[i]->startNode->id] = antColony->colonyMap[i]->startNode->coord;
	antColony->colonyMap[i]->internalSegs[antColony->colonyMap[i]->segs.back()]->nodes[antColony->colonyMap[i]->endNode->id] = antColony->colonyMap[i]->endNode->coord;
	antColony->colonyMap[i]->internalSegs[antColony->colonyMap[i]->segs.back()]->edges.push_back(startingEdge);
	antColony->colonyMap[i]->internalSegs[antColony->colonyMap[i]->segs.back()]->extension = nullptr;
	antColony->colonyMap[i]->internalSegs[antColony->colonyMap[i]->segs.back()]->parentAnt = antColony->colonyMap[i];
	antColony->coveredEdges[startingEdge] = 1;

	antColony->colonyMap[i]->pathQuality = 0;
	antColony->colonyMap[i]->filament[startingEdge] = edgePointers[startingEdge];
	antColony->colonyMap[i]->edgeCount = 1; // ant edgeCount setup
	antColony->colonyMap[i]->latestAppendedEdgeID = startingEdge;
	antColony->colonyMap[i]->lastProb = 1;
    	// add startNode and other node of edge to ant's nodeIDs. Order doesnt matter in this point
	antColony->colonyMap[i]->nodeIDs[edgePointers[startingEdge]->end1->id] = edgePointers[startingEdge]->end1->coord;
	antColony->colonyMap[i]->nodeIDs[edgePointers[startingEdge]->end2->id] = edgePointers[startingEdge]->end2->coord;
	// if this edge has interNodes, add them to nodeIDs. For contour and Menger
	if (edgePointers[startingEdge]->interNodes.size() > 0){
	    antColony->colonyMap[i]->nodeIDs.insert(edgePointers[startingEdge]->interNodes.begin(), edgePointers[startingEdge]->interNodes.end());
	}
	// initial solution width, length, angle
	antColony->colonyMap[i]->length = edgePointers[startingEdge]->length;
	antColony->colonyMap[i]->width = edgePointers[startingEdge]->width;
	antColony->colonyMap[i]->avgAngle = edgePointers[startingEdge]->angle;
	antColony->colonyMap[i]->pixelCounter = edgePointers[startingEdge]->end1->parent->pixelMembers.size() + edgePointers[startingEdge]->end2->parent->pixelMembers.size();
	antColony->colonyMap[i]->m10 = edgePointers[startingEdge]->end1->parent->m10 + edgePointers[startingEdge]->end2->parent->m10;
	antColony->colonyMap[i]->m01 = edgePointers[startingEdge]->end1->parent->m01 + edgePointers[startingEdge]->end2->parent->m01;
	antColony->colonyMap[i]->m11 = edgePointers[startingEdge]->end1->parent->m11 + edgePointers[startingEdge]->end2->parent->m11;
	antColony->colonyMap[i]->m02 = edgePointers[startingEdge]->end1->parent->m02 + edgePointers[startingEdge]->end2->parent->m02;
	antColony->colonyMap[i]->m20 = edgePointers[startingEdge]->end1->parent->m20 + edgePointers[startingEdge]->end2->parent->m20;
	antColony->colonyMap[i]->massCenter = cv::Point2f(antColony->colonyMap[i]->m01/antColony->colonyMap[i]->pixelCounter,antColony->colonyMap[i]->m10/antColony->colonyMap[i]->pixelCounter);
	antDebugData(i);
	buildCandidateSolution(antColony->colonyMap[i]);
	i++;
    }
    printf("goodAnts %ld total ants %d\n", antColony->goodAnts.size(), i);
    globalEval(true);
}

/* Choose a neighbor at the first endNode and ...*/
void buildCandidateSolution(Ant* ant){
    Nodo *tmpNode, *startNode = ant->startNode; // tmpNode is intermediate node before updating
    double vecAngle, sumProbability, random, lowerProbabilityBound, upperProbabilityBound, pherom;
    double maxAngle = (cellRestrictions->angle * 2.5 >= 90) ? 90 : cellRestrictions->angle * 2.5;
    double tmp_covMatU11, tmp_covMatU20, tmp_covMatU02, tmp_sqrt;
    int goodQualityPaths, dbSegID;
    std::pair<int,int> angleOf2Vecs, angleOf2VecsReverse;
    std::unordered_map<int, edgeCandidateData*> QoE; // Quality of (neighbor) Edge key is edgeID
    std::unordered_map<int, edgeCandidateData*>::iterator QoEIterator;

    // iterate until a path that represents a solution is built
    while (ant->latestAppendedEdgeID >= 0){ // Advance until no choices are available (-1) or probabilites are not good (-2)
	tmpNode = ant->endNode;
	sumProbability = goodQualityPaths = 0;
    	// Retrieve edgesMap from cluster that is represented by endNode (cluster and endNode share same ID)
	// Calculate neighbor edges probabilities 
    	for(std::unordered_map<int,Arista*>::iterator neighborEdge = clusterPointers[tmpNode->id]->edgesMap.begin(); neighborEdge != clusterPointers[tmpNode->id]->edgesMap.end(); neighborEdge++){
	    //if (debug == 4){printf("aa%d-%d eID %d edgeAngle %.4f\n", ant->id, ant->edgeCount+1, neighborEdge->second->id, neighborEdge->second->angle);}
	    // Select the neighborEdge. Skip previously visted edge, as it will appear as a neighbor. Avoids going back.
	    if (ant->filament.count(neighborEdge->second->id)){
		if (debug == 4){printf("aa%d-%d eID %d skipped\n", ant->id, ant->edgeCount+1, neighborEdge->second->id);}
		continue;
	    }
	    if (debug == 4){printf("aa%d-%d eID %d eNode %d neighE2 %d neighE1 %d\n",ant->id, ant->edgeCount+1, neighborEdge->second->id,ant->endNode->id,neighborEdge->second->end2->id, neighborEdge->second->end1->id);}
	    // find which nodes are the extreme ones (If they are not the same, it's the end im looking for)
            ant->endNode = (neighborEdge->second->end1->id == tmpNode->id) ? neighborEdge->second->end2 : neighborEdge->second->end1;

	    // If cycles are not allowed, checked for already inserted nodeIDs
            if (!cellRestrictions->withCycles && ant->nodeIDs.count(ant->endNode->id) > 0){
		if (debug == 4){
		    printf("aa%d-%d eID %d cycle restriction. Node %d already in node map. skipped. neiEnd2 %d neiEnd1 %d\n", ant->id, ant->edgeCount+1, neighborEdge->second->id, ant->endNode->id, neighborEdge->second->end2->id, neighborEdge->second->end1->id);
		}
		ant->endNode = tmpNode; // reset ant->endNode to previous value
		continue;
	    }
	    // if allowsOverlap = false, skip this edge if already used by another ant, and update antColony->coveredEdge while building // TODO: Check
	    if (!cellRestrictions->allowsOverlap && ant->colonyPtr->coveredEdges[neighborEdge->second->id] > 0) {
		if (debug == 4){printf("aa%d-%d eID %d coveredEdge %d allowsOverlap False. skipped\n", ant->id, ant->edgeCount+1, neighborEdge->second->id, ant->colonyPtr->coveredEdges[neighborEdge->second->id]);}
		ant->endNode = tmpNode; // reset ant->endNode to previous value
		continue;
	    }

	    angleOf2Vecs = std::make_pair(startNode->id,ant->endNode->id);
	    angleOf2VecsReverse = std::make_pair(ant->endNode->id, startNode->id);
	    // if vecAngle data for this 3 nodes hasnt already been calculted, save it
	    if (clusterPointers[tmpNode->id]->node.neighborNodesAngles.count(angleOf2Vecs) > 0){
		vecAngle = clusterPointers[tmpNode->id]->node.neighborNodesAngles[angleOf2Vecs];
	    } else if (clusterPointers[tmpNode->id]->node.neighborNodesAngles.count(angleOf2VecsReverse) > 0){
		vecAngle = clusterPointers[tmpNode->id]->node.neighborNodesAngles[angleOf2VecsReverse];
	    } else if (clusterPointers[tmpNode->id]->node.neighborNodesAngles.count(angleOf2Vecs) == 0 && clusterPointers[tmpNode->id]->node.neighborNodesAngles.count(angleOf2VecsReverse) == 0) {
		vecAngle = angleBetweenVecs(*startNode, *tmpNode, *ant->endNode); // startNode is local/myopic start	
		clusterPointers[tmpNode->id]->node.neighborNodesAngles[angleOf2Vecs] = vecAngle;
		clusterPointers[tmpNode->id]->node.neighborNodesAngles[angleOf2VecsReverse] = vecAngle;
	    }

	    if (debug == 4){printf("aa%d-%d vecAngle %.4f eID %d sN %d tN %d eN %d esN %d tN %d eN %d\n", ant->id, ant->edgeCount+1, vecAngle, neighborEdge->second->id, startNode->id, tmpNode->id, ant->endNode->id, ant->startNode->id, tmpNode->id, ant->endNode->id);}

	    // Check if angle between edges (theta) is smaller than cellRestrictions->angle (theta_threshold)
	    if (abs((int)vecAngle) <= cellRestrictions->angle){ // 180 is ideal straightness continuity. So cellRestriction angle is the threshold of tolerance
		QoE[neighborEdge->second->id] = new edgeCandidateData();
		QoE[neighborEdge->second->id]->eta = pow(ant->colonyPtr->maxThetaScore, ant->colonyPtr->beta);
		QoE[neighborEdge->second->id]->probability = QoE[neighborEdge->second->id]->eta; //* pow(neighborEdge->second->pheromoneValueTau, ant->colonyPtr->alpha);
		QoE[neighborEdge->second->id]->newEndNodeID = ant->endNode->id;
		QoE[neighborEdge->second->id]->intermQuality = false;
		goodQualityPaths++;
		QoE[neighborEdge->second->id]->dbSegID = 0; // Not really here. Just for consistency when passing data to respective edgeCandidateData
		QoE[neighborEdge->second->id]->pherom = 0;

	    } else if (abs((int)vecAngle) < maxAngle){ // if angle between edges > cellRestrictions->angle, Penalize probability as diff between neighborEdge's angle and avgAngle increases
		// if there is more than 1 segment, then this segment potentially about to end is a bridge between its predecesor and the segment that comes next (if this new interQua Edge is choosen)
		if (ant->internalSegs[ant->segs.back()]->edges.size() == 1 && ant->segs.size() > 1){
		    dbSegID = extendedSegWithinDB(conn, ant->internalSegs[ant->segs.back()], ant->internalSegs[ant->prevSeg]->nodes);
		} else {
		    dbSegID = segWithinDB(conn, ant->segs.back());
		}
		pherom = 1.0;
		//ant->segs.back() = dbSeg; Cant do this. It causes segment evaluation to be voided
		// Checks if segment exists, then must check if combination edge-segment exists.
		if (dbSegID > 0){
		    try{
			pherom = ant->colonyPtr->bannedPathsPherom.at(neighborEdge->second->id).at(dbSegID);
		    } catch (...){ // const std::out_of_range &e, but e is never used
			pherom = 1.0;
		    }
		}
		// Cant use an edge so penalized
		if (pherom == 0){
		    if (debug == 4){printf("aa%d-%d eID %d banned-skipped\n", ant->id, ant->edgeCount+1, neighborEdge->second->id);}
		    continue;
		}
		QoE[neighborEdge->second->id] = new edgeCandidateData();
		// maxThetaScore * (1 - deltaAngle/180)
                QoE[neighborEdge->second->id]->eta = ant->colonyPtr->maxThetaScore * (1 - fabs(vecAngle - cellRestrictions->angle/2)/ 180);
		QoE[neighborEdge->second->id]->eta = pow(QoE[neighborEdge->second->id]->eta, ant->colonyPtr->beta);
		// The bannedPathsPherom add extra penalization to the edge pheromone if its part of a bad solution
                QoE[neighborEdge->second->id]->probability = QoE[neighborEdge->second->id]->eta * pow(pherom, ant->colonyPtr->alpha);
                QoE[neighborEdge->second->id]->newEndNodeID = ant->endNode->id;
		QoE[neighborEdge->second->id]->intermQuality = true;
		QoE[neighborEdge->second->id]->dbSegID = dbSegID;
		QoE[neighborEdge->second->id]->pherom = pherom;

	    } else { // Any angle over 90 is a bad choice and wont be considered
		continue; // skip altering sumProbability as QoE[neighborEdge->second->id] for this case wont exist
	    }
	    QoE[neighborEdge->second->id]->vecAngle = vecAngle;
	    sumProbability += QoE[neighborEdge->second->id]->probability;
	}
	
	// If QoE.size == 0, there are no new path options that can be taken. Ant path building ends
        if (QoE.size() == 0){
	    antDebugData(ant->id);
            ant->latestAppendedEdgeID = -1;
	    ant->endNode = tmpNode; // reset ant->endNode to previous value
            break;
        }

	// Select next edge based on path probability and random number
	lowerProbabilityBound = upperProbabilityBound = 0;
	// get random number [0,1]
        random = ((double) rand() / (RAND_MAX));
	// if there are no goodQuality Options, random belongs [0,0.5]
	//if (!goodQualityPaths){sumProbability *= pow(ant->colonyPtr->notAdvancingFactor,-1);} // 0.5 -> 2
	for (QoEIterator = QoE.begin(); QoEIterator != QoE.end(); QoEIterator++){
	    // Update probability value by dividing with sumProbability
	    QoEIterator->second->probability /= sumProbability;
	    upperProbabilityBound += QoEIterator->second->probability;
	    // Select edge where random number is within range
	    if ((random > lowerProbabilityBound) && (random <= upperProbabilityBound)){
		break; //winner has been selected
	    } else {
		lowerProbabilityBound = upperProbabilityBound;
	    }
	}

	// If this happens, the chance of not advancing has won & ant building end.
	if (lowerProbabilityBound == upperProbabilityBound){
	    ant->latestAppendedEdgeID = -5;
            antDebugData(ant->id);
	    break;
	}
	if (debug == 4){
	    printf("Ant %d-%d selected edge %d w/prob %.4f. endNode %d random %f sumProb %.4f\n", ant->id, ant->edgeCount+1, QoEIterator->first, QoEIterator->second->probability, QoEIterator->second->newEndNodeID, random, sumProbability);
	}

	ant->vecAngle = QoEIterator->second->vecAngle;
	ant->filament[QoEIterator->first] = edgePointers[QoEIterator->first];
	// if this edge has interNodes, add them to nodeIDs. For contour and Menger
        if (edgePointers[QoEIterator->first]->interNodes.size() > 0){
	    ant->nodeIDs.insert(edgePointers[QoEIterator->first]->interNodes.begin(), edgePointers[QoEIterator->first]->interNodes.end());
	}
        // update internal node pointers
        startNode = tmpNode;
	ant->endNode = &clusterPointers[QoEIterator->second->newEndNodeID]->node;
        ant->nodeIDs[QoEIterator->second->newEndNodeID] = ant->endNode->coord;
	// update geom properties
	ant->avgAngle = ((ant->avgAngle * ant->edgeCount) + edgePointers[QoEIterator->first]->angle) / (ant->edgeCount + 1);
	ant->length += edgePointers[QoEIterator->first]->length;
	ant->width += edgePointers[QoEIterator->first]->width;
	ant->latestAppendedEdgeID = edgePointers[QoEIterator->first]->id;
	ant->lastProb = QoEIterator->second->probability;
	// Testing: m10 and m01 should reflect some sort of direction
	//ant->curvature = sqrt( (24/pow(ant->length,3)) * (ant->length - euclideanDist(ant->startNode->coord, ant->endNode->coord)) );
	ant->pixelCounter += clusterPointers[QoEIterator->second->newEndNodeID]->pixelMembers.size();
	ant->m10 += clusterPointers[QoEIterator->second->newEndNodeID]->m10;
	ant->m01 += clusterPointers[QoEIterator->second->newEndNodeID]->m01;
	ant->massCenter = cv::Point2f(ant->m01/ant->pixelCounter, ant->m10/ant->pixelCounter);
	ant->m11 += clusterPointers[QoEIterator->second->newEndNodeID]->m11;
	ant->m20 += clusterPointers[QoEIterator->second->newEndNodeID]->m20;
	ant->m02 += clusterPointers[QoEIterator->second->newEndNodeID]->m02;
        tmp_covMatU11 = (ant->m11/ant->pixelCounter) - ant->massCenter.x * ant->massCenter.y;
        tmp_covMatU20 = (ant->m20/ant->pixelCounter) - pow(ant->massCenter.y,2);
        tmp_covMatU02 = (ant->m02/ant->pixelCounter) - pow(ant->massCenter.x,2);
        tmp_sqrt = sqrt(4*pow(tmp_covMatU11,2) + pow((tmp_covMatU20 - tmp_covMatU02),2) ) /2;
        ant->eigenVal1 = ((tmp_covMatU20 + tmp_covMatU02) / 2) + tmp_sqrt;
        ant->eigenVal2 = ((tmp_covMatU20 + tmp_covMatU02) / 2) - tmp_sqrt;
	// Update ant's Path Quality
	ant->pathQuality += QoEIterator->second->eta;
	ant->edgeCount++;

	// All Ants must track the edge were there was no good quality, for tour quality analysis
	if (QoEIterator->second->intermQuality){
	    ant->interQuaEdges.push_back(QoEIterator->first);
	    // Corner case of 2 single edge segments, where the only edge per segment is of interQuality. Extend the segment about to be closed with the nodes of its predecesor
	    // in order to avoid "bottlenecks" that damage the search of other solutions.
	    // if there is more than 1 segment, then this segment about to end is a bridge between its predecesor and the segment that comes next
	    if (ant->internalSegs[ant->segs.back()]->edges.size() == 1 && ant->segs.size() > 1){
		ant->internalSegs[ant->segs.back()]->extension = ant->internalSegs[ant->prevSeg]; // prevSeg still stores predecesor id
		// modify current segment's start node to prevSegment's original start node used to extend. This impacts segAngle calculations
		ant->internalSegs[ant->segs.back()]->startNode = ant->internalSegs[ant->prevSeg]->ogStartNode;
		//debug info: iter prevSeg nodes to print them
		if (debug == 4){
		    printf("Ant %d-%d extended segment %d w/segment %d ext_nodes: ", ant->id, ant->edgeCount+1, ant->segs.back(), ant->prevSeg);
		    for (std::unordered_map<int,cv::Point>::iterator nodIt = ant->internalSegs[ant->prevSeg]->nodes.begin(); nodIt != ant->internalSegs[ant->prevSeg]->nodes.end(); ++nodIt){
			extendSegDB(conn, ant->segs.back(), clusterPointers[nodIt->first]->node);
			printf("%d ", nodIt->first);
		    }
		    printf("\n");
		}
	    }

	    // if interQuaEdge was selected, check dbSegID for internalBanPathPherom assignment
	    if (QoEIterator->second->dbSegID > 0){ // add link between local segment and the one used for the pheromone value (bigger segment)
		ant->replacedSegs[ant->segs.back()] = QoEIterator->second->dbSegID;
		ant->pheromRecord[QoEIterator->first][QoEIterator->second->dbSegID] = QoEIterator->second->pherom;
	    } else { // If this is the end of a segment not in db, assign value of 1.0 to pherom (QoEIterator->second->pherom should be 1 if dbSegID == 0 for an interQuaEdge)
		ant->colonyPtr->bannedPathsPherom[QoEIterator->first][ant->segs.back()] = QoEIterator->second->pherom;
		ant->pheromRecord[QoEIterator->first][ant->segs.back()] = QoEIterator->second->pherom;
	    }
	    // As the selected edge was of interimQuality, end previous segment, append to segs vector
	    ant->prevSeg = ant->segs.back();
	    ant->segs.push_back(insertSegmentDB(conn, *tmpNode, *ant->endNode));
	    // Add segment data to ant's segData map
	    ant->internalSegs[ant->segs.back()] = new Segment();
	    ant->internalSegs[ant->segs.back()]->dbId = ant->segs.back();
	    ant->internalSegs[ant->segs.back()]->length = edgePointers[QoEIterator->first]->length;
	    ant->internalSegs[ant->segs.back()]->startNode = &clusterPointers[tmpNode->id]->node;
	    ant->internalSegs[ant->segs.back()]->ogStartNode = &clusterPointers[tmpNode->id]->node;
	    ant->internalSegs[ant->segs.back()]->endNode = ant->endNode;
	    ant->internalSegs[ant->segs.back()]->nodes[tmpNode->id] = tmpNode->coord;
	    ant->internalSegs[ant->segs.back()]->nodes[ant->endNode->id] = ant->endNode->coord;
	    ant->internalSegs[ant->segs.back()]->edges.push_back(QoEIterator->first);
	    ant->internalSegs[ant->segs.back()]->extension = nullptr;
	    ant->internalSegs[ant->segs.back()]->parentAnt = ant;
	} else { // Add node to current segment
	    // add directly to db
	    extendSegDB(conn, ant->segs.back(), *ant->endNode);
	    // Add segment data to ant's segData map
	    ant->internalSegs[ant->segs.back()]->length += edgePointers[QoEIterator->first]->length;
	    ant->internalSegs[ant->segs.back()]->endNode = ant->endNode;
	    ant->internalSegs[ant->segs.back()]->nodes[ant->endNode->id] = ant->endNode->coord;
	    ant->internalSegs[ant->segs.back()]->edges.push_back(QoEIterator->first);
	}
	antDebugData(ant->id);

	// Delete edgeCandidateData in QoE map values. Then clear map
	for (QoEIterator = QoE.begin(); QoEIterator != QoE.end(); QoEIterator++){
	    delete QoEIterator->second;
	}
	QoE.clear();
	// In case of neuron: If the selected node is of type 5 or 6, break loop
	if (ant->endNode->nType >= 5 && cellRestrictions->cellIdentifier == 1){
	    ant->latestAppendedEdgeID = -6;
            antDebugData(ant->id);
            break;
	}
    }

    // Tour Evaluation. Substract score if curvature is too high or length is greater than diamenter * 0.8
    if (ant->filament.size() == 1) { // Cant calculate vecAngle or menger for a single edge ant
	vecAngle = 0;
	if (ant->massCenter.x == 0 && ant->massCenter.y == 0){ // If no pixels where matched to this 1 filament ant, calculate massCenter this way
	    ant->massCenter = cv::Point2f(0.5*(ant->startNode->coord.x + ant->endNode->coord.x), 0.5*(ant->startNode->coord.y + ant->endNode->coord.y));
	}
    } else {
	Nodo mCnode = {};
	mCnode.coord = ant->massCenter;
	vecAngle = angleBetweenVecs(*ant->startNode, mCnode, *ant->endNode);
    }
    if (debug == 4){printf("Ant %d vecAngle %.4f\n", ant->id, vecAngle);}

    tourQuality(ant, vecAngle);
}

/* text and visual aid during ant tours */
void antDebugData(int antID){
    Ant *ant = antColony->colonyMap[antID];
    double angle, pheromAtMoment, edgeLength;
    std::string infoStr = "";
    cv::Point2f end1, end2;
    cv::Scalar edgeClr, end1NodeClr(255,0,255), end2NodeClr(255,0,255);
    if (debug){
        int circleThickness = 3;
        antsMat = graph.clone();
        for (std::map<int, Arista*>::reverse_iterator it = ant->filament.rbegin(); it != ant->filament.rend(); it++){
	    // patch if coord is 0,0
            if (it->second->end1->coord.x == 0 && it->second->end1->coord.y == 0){ end1 = it->second->end1->parent->massCenter;
            } else { end1 = it->second->end1->coord; }

            if (it->second->end2->coord.x == 0 && it->second->end2->coord.y == 0){ end2 = it->second->end2->parent->massCenter; 
            } else { end2 = it->second->end2->coord; }

	    // set edge color: If edgeID is in pheromRecord, then it must be a interQualityEdge
            if (ant->pheromRecord.count(it->first) > 0){
                edgeClr = ant->color;
		// If edge is of interQuality, the nodes of nType 4 must have a different color
		if (it->second->end1->nType == 4){
		    end1NodeClr = cv::Scalar(0,0,255);
		}
		if (it->second->end2->nType == 4){
                    end2NodeClr = cv::Scalar(0,0,255);
                }
            } else {
                edgeClr = cv::Scalar(0,0,255);
            }

	    // If a jsonFile is provided, paint ant->this_filament->edgePxls
	    if (cellRestrictions->nxJsonFile != ""){
		for (std::vector<cv::Point>::iterator pxlIt = it->second->edgePxls.begin(); pxlIt != it->second->edgePxls.end(); ++pxlIt){
		    antsMat.at<cv::Vec3b>(*pxlIt) = cv::Vec3b(edgeClr[0], edgeClr[1], edgeClr[2]);
		}
	    } // "old" method, short lines
	    else {
		cv::line(antsMat, end1, end2, edgeClr, 2);
	    }
            cv::circle(antsMat, ant->massCenter, circleThickness-1, cv::Scalar(0,255,0), cv::FILLED);
	    cv::circle(antsMat, end1, circleThickness, end1NodeClr, cv::FILLED);
	    cv::circle(antsMat, end2, circleThickness, end2NodeClr, cv::FILLED); // Also paint end2 edge
        }

	// Avoids error when last edge is -1 due to end of tour
        if (ant->latestAppendedEdgeID >= 0){
	    angle = edgePointers[ant->latestAppendedEdgeID]->angle;
	    edgeLength = edgePointers[ant->latestAppendedEdgeID]->length;
	    infoStr = "mC:" + std::to_string((int)ant->massCenter.x) + "," + std::to_string((int)ant->massCenter.y) + " eID:" + std::to_string(ant->latestAppendedEdgeID)
		    + "(" + std::to_string(edgePointers[ant->latestAppendedEdgeID]->end1->id) + "," + std::to_string(edgePointers[ant->latestAppendedEdgeID]->end2->id) + ")";
	} else {
	    angle = edgeLength = 0;
	}

	// Write data and execute imwrite outside forloop
        try {
            //printf("a%d infoStr %s\n", antID, infoStr.c_str());
            cv::putText(antsMat, infoStr, cv::Point(0,8), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.6, cv::Scalar(255,0,255), 1, 8);
            cv::imwrite("/tmp/" + cellRestrictions->filename + "-Ant" + std::to_string(antID) + "-" + std::to_string(ant->edgeCount) + ".jpg", antsMat);
        } catch (cv::Exception& e) {fprintf(stderr, "%s\n", e.what());
        } catch (...) {fprintf(stderr, "antDebugData imwrite unknown error\n");}

        // If the latest added edge is part of pheromRecord, retrieve value of at pheromRecord[latestAppEdge][prevSeg]
        if (ant->pheromRecord.count(ant->latestAppendedEdgeID) > 0){
            if (ant->replacedSegs.count(ant->prevSeg) > 0){ // if the original segment was replaced
                try {
                    pheromAtMoment = ant->pheromRecord.at(ant->latestAppendedEdgeID).at(ant->replacedSegs.at(ant->prevSeg));
		} catch (...) {
		    pheromAtMoment = -3;
		}
            } else {
                try {
                    pheromAtMoment = ant->pheromRecord.at(ant->latestAppendedEdgeID).at(ant->prevSeg);
                } catch (...) {
                    pheromAtMoment = -2;
                }
            }
        } else {
                pheromAtMoment = 0;
        }

        printf("a%d l %4.4f w %7.4f eCount %3d avgAngle %4.4f edgeid %3d(%3d,%3d) vecAngle %4.4f lastAppEdgeAng %3.4f massCntr %4.4f %4.4f eigV1 %5.4f eigV2 %5.4f pQual %3.4f interQuaEdges %ld\n", antID, ant->length, ant->width, ant->edgeCount, ant->avgAngle, ant->latestAppendedEdgeID, ant->startNode->id, ant->endNode->id, ant->vecAngle, angle, ant->massCenter.x, ant->massCenter.y, ant->eigenVal1, ant->eigenVal2, ant->pathQuality, ant->interQuaEdges.size());

        fprintf(metadataFile,"a%d    %4.4f %7.4f     %3d     %2.2f    %3d - %3d{%d° %d} (%4d) <%1.4f> [%6d] [%.4f]    %4.4f|%3.4f     %4.4f          %5.4f-%5.4f     <%5.2f, %5.2f> %3.4f\n", antID, ant->length, ant->width, ant->edgeCount, ant->pathQuality, ant->startNode->id, ant->endNode->id, ant->endNode->degree, ant->endNode->nType, ant->latestAppendedEdgeID, ant->lastProb, ant->segs.back(), pheromAtMoment, edgeLength, angle, ant->vecAngle, ant->massCenter.x, ant->massCenter.y, ant->eigenVal1, ant->eigenVal2, ant->avgAngle);
    }
}

/* Choose starting edge for each ant. Common part is to select from endEdges. Then logic doesnt apply to neurons */
int antStartingEdge(Ant* ant){
    int startingEdge = -1;
    std::unordered_map<int,int>::iterator startingEdge_it;
    // Start selection from endEdges first
    if (cellRestrictions->endEdges.size() > 0){
	startingEdge_it = std::next(std::begin(cellRestrictions->endEdges), rand() % cellRestrictions->endEdges.size());
	startingEdge = startingEdge_it->first;
	// cant assign startNode and endNode randomly as it causes mayhem
	if (edgePointers[startingEdge]->end1->degree == 1){
	    ant->startNode = edgePointers[startingEdge]->end1;
	    ant->endNode = edgePointers[startingEdge]->end2;
	} else {
	    ant->startNode = edgePointers[startingEdge]->end2;
	    ant->endNode = edgePointers[startingEdge]->end1;
	}
	// Remove edge from endEdges map
	cellRestrictions->endEdges.erase(startingEdge);
    }

    // End here for neurons, synth and others if manually disabled by user
    if (!cellRestrictions->initAsignHeuristic){ return startingEdge;}
    // Logic for MTs (and others cells) start edges: After using all endEdges, use interQuaEdges
    else {
	if (cellRestrictions->interQuaEdges.size() > 0){
	    startingEdge_it = std::next(std::begin(cellRestrictions->interQuaEdges), rand() % cellRestrictions->interQuaEdges.size());
	    startingEdge = startingEdge_it->first;
	    // Assign start to node that is of interm Quality
	    if (edgePointers[startingEdge]->end1->nType == 4){
		ant->startNode = edgePointers[startingEdge]->end1;
		ant->endNode = edgePointers[startingEdge]->end2;
	    } else {
		ant->startNode = edgePointers[startingEdge]->end2;
		ant->endNode = edgePointers[startingEdge]->end1;
	    }
	    // Remove edge from interQuaEdges map
	    cellRestrictions->interQuaEdges.erase(startingEdge);
	// Finally, use edges not covered
	} else {
	    startingEdge_it = std::next(std::begin(antColony->pendingEdges), rand() % antColony->pendingEdges.size());
	    startingEdge = startingEdge_it->first;
	    // Start-EndNode assignation static for now
	    ant->startNode = edgePointers[startingEdge]->end1;
	    ant->endNode = edgePointers[startingEdge]->end2;
	}
    }
    return startingEdge;
}

/* Delete every ant and the Colony struct*/
void endColony(){
    for (std::unordered_map<int, Ant*>::iterator ants = antColony->colonyMap.begin(); ants != antColony->colonyMap.end(); ants++){
	// for all ants, clear internalSegments
	for (std::unordered_map<int, Segment*>::iterator sIt = ants->second->internalSegs.begin(); sIt != ants->second->internalSegs.end(); ++sIt){
	    delete sIt->second;
	}
	delete ants->second;
    }
    // close db connection
    try {
    	exit_nicely(conn);
    } catch(...) {
	fprintf(stderr, "Error closing db connection at endColony\n");
    }
    // print coveredEdges content
    if (debug == 4){
        for (int i = 0; i < antColony->edges; i++){
            printf("%d ", antColony->coveredEdges[i]);
        }
        printf(" .covEdges size: %d\n", antColony->edges);
    }
    //if (antColony->coveredEdges){delete [] antColony->coveredEdges;}
    delete antColony;
}

// https://en.wikipedia.org/wiki/Menger_curvature
double mengerCurvature(cv::Point2f a, cv::Point2f b, cv::Point2f c) {
    double triangleArea = fabs((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) / 2;
    return 4 * triangleArea / (euclideanDist(a,b) * euclideanDist(b,c) * euclideanDist(a,c) );
}

/**/
void statistics(){
    int i = 0; //clustID = 0, clusterPixels = 0;
    cv::Vec3b clusterClr;
    cv::Point clusterMcntr;
    /*cv::Mat biggCluster = ogFrame.clone();
    std::vector<std::pair<int,int>> clusterPairs; // 1st element is cluster id,  second is pixelMemberSize
    for (std::unordered_map<int, Cluster*>::iterator cit = clusterPointers.begin(); cit != clusterPointers.end(); ++cit){
	clusterPairs.push_back(std::make_pair(cit->first, (int)cit->second->pixelMembers.size()));
    }
    // sort cluster by pixelMembers, get color and mass center position
    std::sort(clusterPairs.begin(), clusterPairs.end(), sortPairDesc);
    for(std::vector<std::pair<int,int>>::iterator clust = clusterPairs.begin(); clust != clusterPairs.begin() + 3; clust++){
	for (std::vector<std::pair<int,int>>::iterator p = clusterPointers[(*clust).first]->pixelMembers.begin(); p != clusterPointers[(*clust).first]->pixelMembers.end(); ++p){
	    biggCluster.at<cv::Vec3b>((*p).first, (*p).second) = clusterClr;
	}
	clustID = (*clust).first;
	clusterPixels = (*clust).second;
	clusterClr = clusterPointers[(*clust).first]->color;
	clusterMcntr = clusterPointers[(*clust).first]->massCenter;
	biggCluster.at<cv::Vec3b>(clusterMcntr.y, clusterMcntr.x) = cv::Vec3b(0,0,255);
	cv::circle(biggCluster, clusterMcntr, 2, clusterClr);
	fprintf(stdout, "cID %d, pixels %d color %d %d %d massCenter %d %d numNei %ld nxID %d absorbedClusters %ld:", clustID, clusterPixels, clusterClr[0], clusterClr[1], clusterClr[2], clusterMcntr.x, clusterMcntr.y, clusterPointers[clustID]->neighboursMap.size(), clusterPointers[clustID]->nxId, clusterPointers[clustID]->absorbedClusters.size());
	for (std::unordered_map<int,int>::iterator absClus = clusterPointers[clustID]->absorbedClusters.begin(); absClus != clusterPointers[clustID]->absorbedClusters.end(); ++absClus){
	    fprintf(stdout, "%d ", absClus->first);
	}
	fprintf(stdout,"\n");
    }
    showImg("biggest cluster", biggCluster);
    */// 1st, edge angle histogram data
    fprintf(stdout, "EdgeAngles=[");
    fprintf(metadataFile, "EdgeAngles=[");
    for(std::unordered_map<int, Arista*>::iterator edge = edgePointers.begin(); edge != edgePointers.end(); edge++){
	fprintf(stdout, "%.4f, ", edge->second->angle);
	fprintf(metadataFile, "%.4f, ", edge->second->angle);
    }
    fprintf(stdout, "]\nproposedFilaments = {");
    fprintf(metadataFile, "]\nproposedFilaments = {");
    for(std::map<int, Ant*>::iterator antIt = antColony->goodAnts.begin(); antIt != antColony->goodAnts.end(); ++antIt){
	fprintf(stdout, "%d: [", i);
	fprintf(metadataFile,  "%d: [", i);
	for(std::map<int,Arista*>::iterator edgeIt = antIt->second->filament.begin(); edgeIt != antIt->second->filament.end(); ++edgeIt){
	    fprintf(stdout, "%d, ", edgeIt->first);
	    fprintf(metadataFile,"%d, ", edgeIt->first);
	}
	fprintf(stdout, "], ");
	fprintf(metadataFile,"], ");
	i++;
    }
    fprintf(stdout, "}\n");
    fprintf(metadataFile, "}\n");

}

/* fitness function of each ant's solution/path */
/* update antColony->coveredEdges information for coverage info. This is the ant "returning" to the start node/edge.
   Any solution with an angle over 45 degrees (for MTs) in vecAngle, is just plain bad */
void tourQuality(Ant* ant, double vecAngle){
    int seg2ID, farthestNodeID, randClr;
    double segAngle, seg1Len, seg2Len, segmRatio, accumLen, mengerMassCtr, sinSegAngle, maxDisplacement; //, hullArea;
    double deltaY, deltaX, yIntercept, tmpDistance, nodeLineDistance = 0;
    std::string debugStringInfo;
    cv::Mat antPxls, debugContour;
    cv::Point2f tmpPnt, tmpEndPnt;
    std::vector<int>::reverse_iterator interQuaEdgeIt;
    std::vector<std::vector<cv::Point>> contours;
    std::vector<int> rejectedContour;
    bool breakFlag; // seg2WithinSeg1;
    Nodo init, mid, end;

    // Best case is the one that iterates only thru good options. Else, it must have at least 2 segments
    // Neurons also must end in a centerNode or connectionNode
    if (((ant->edgeCount - 1) * ant->colonyPtr->maxThetaScore == ant->pathQuality) && (cellRestrictions->cellIdentifier != 1 ||
			    (cellRestrictions->cellIdentifier == 1 && (ant->endNode->nType >= 5 || ant->endNode->nType == 3))) ) {
	// If edgeCount * maxThetaScore == pathQuality, it has passed thru perfect options only, thus been a great solution
	// if last node is of degree 1, it ended in an end node. This is better than a tour that ended because there were only bad choices 
	ant->pathQuality = ant->colonyPtr->maxThetaScore;
    // For those cases, check angle between segments, increase penalty in bad combination in bannedPathsPherom[][]
    } else {
	// Normalize pathQuality for all tours with quality greater than 0 (Dont normalize single edge tours)
	ant->pathQuality = (ant->pathQuality > 0) ? ant->pathQuality/(ant->edgeCount -1): 0;
	if (ant->nodeIDs.size() > 2){ // Menger makes no sense with only 1 edge / 2 nodes
	    // 1st, get node within ant's nodes that is the furthest from the line between startNode and endNode
	    deltaY = ant->startNode->coord.y - ant->endNode->coord.y; // y1 - y2
	    deltaX = ant->endNode->coord.x - ant->startNode->coord.x; // x2 - x1
	    yIntercept = (ant->startNode->coord.x * ant->endNode->coord.y) - (ant->endNode->coord.x * ant->startNode->coord.y); // x1 * y2 - x2 * y1
	    // Calculate node to line distance for each ant's node
	    for (std::unordered_map<int,cv::Point2f>::iterator nodeIt = ant->nodeIDs.begin(); nodeIt != ant->nodeIDs.end(); ++nodeIt){
		tmpDistance = fabs((deltaY * nodeIt->second.x) + (deltaX * nodeIt->second.y) + yIntercept);
		if (tmpDistance > nodeLineDistance){
		    nodeLineDistance = tmpDistance;
		    farthestNodeID = nodeIt->first;
		}
	    }
	    // Calculate Menger's curvature. Shouldnt be more than X times mengerCurv with massCenter (received in vecAngle) as farthest point
	    ant->mengerCurvature = mengerCurvature(ant->startNode->coord, ant->nodeIDs[farthestNodeID], ant->endNode->coord);
	} else {
	    ant->mengerCurvature = 0;
	}
	mengerMassCtr = mengerCurvature(ant->startNode->coord, ant->massCenter, ant->endNode->coord);
	// Check interQuaEdges, curvature and other criteria
	// reverse interQuaEdge iterator and last segment id
	interQuaEdgeIt = ant->interQuaEdges.rbegin();
	seg2ID = ant->segs.back();
	seg2Len = ant->internalSegs[seg2ID]->length;
	accumLen = ant->length - seg2Len;

        for (std::vector<int>::reverse_iterator segIt = ant->segs.rbegin() + 1; segIt != ant->segs.rend(); segIt++){
	    // Get angle between segments and length of penultimate seg. Bad case detector
	    seg1Len = ant->internalSegs[*segIt]->length;
	    // middle node as seg2 startNode allows to use modified startNode due to posible extension
	    segAngle = angleBetweenVecs2(ant->internalSegs[*segIt]->startNode->coord, ant->internalSegs[seg2ID]->startNode->coord, ant->internalSegs[seg2ID]->endNode->coord);
	    // segAngle must be within [0,90] to keep it positive
	    if (segAngle > 90){segAngle = 90;} // Anything greater than 90 degrees, we dont care as sin(90) = 1
	    sinSegAngle = (accumLen >= seg2Len) ? sin(segAngle*M_PI/180) * seg2Len : sin(segAngle*M_PI/180) * accumLen;
	    maxDisplacement = (accumLen >= seg2Len) ? accumLen * cellRestrictions->maxAxialDispl * 0.1 : seg2Len * cellRestrictions->maxAxialDispl * 0.1;
	    // Compare magnitude ratios between seg1Len and seg2Len
	    segmRatio = (seg1Len >= seg2Len) ? seg1Len/seg2Len : seg2Len/seg1Len;
	    // Evaluate if seg1 is extension of seg2
            //seg2WithinSeg1 = ((ant->internalSegs[seg2ID]->extension != nullptr) && (ant->internalSegs[seg2ID]->extension->dbId != *segIt)) ? false : true ;

	    if (debug == 4){
		printf("a%d segAng %.4f seg1 (%d Nodes %d %d l %.4f) seg2 (%d Nodes %d %d l %.4f) lenRatio %.4f accumLen %.4f vecAng %.4f mengerCurv %f massCntMengerCurv %f ratio %.4f sinSegAngle %.4f maxDisplacement %.4f\n", ant->id, segAngle, *segIt, ant->internalSegs[*segIt]->startNode->id, ant->internalSegs[*segIt]->endNode->id, seg1Len, seg2ID, ant->internalSegs[seg2ID]->startNode->id, ant->internalSegs[seg2ID]->endNode->id, seg2Len, segmRatio, accumLen, vecAngle, ant->mengerCurvature, mengerMassCtr, ant->mengerCurvature/mengerMassCtr, sinSegAngle, maxDisplacement);
	    }
	    breakFlag = false;
	    // Penalize first interQuaEdge when mengerCurv / massCntMengerCurv >= cellRestrictions->maxAxialDispl
	    /*if ((ant->mengerCurvature/mengerMassCtr) >= cellRestrictions->maxAxialDispl){
		ant->pathQuality = (ant->pathQuality >= 0) ? -1 : ant->pathQuality - 1;
		if (debug == 4){printf("a%d Quality: %2.4f mengerCurv %f massCntMengerCurv %f ratio %.4f\n", ant->id, ant->pathQuality, ant->mengerCurvature, mengerMassCtr, ant->mengerCurvature/mengerMassCtr);}
		breakFlag = true;
	    } */
	    // Magnitude of growth of a segment in a single direction cant be bigger than (10% of cellRestrictions->maxAxialDispl) times the accumulated length of previous segments
	    // Exceptions are if seg1 is the extension of seg2 and that if seg1+extension has a good quality angle with seg2
	    if ((int)segAngle > cellRestrictions->angle && (sinSegAngle > maxDisplacement)){
		//ant->pathQuality = -4;
		ant->pathQuality = (ant->pathQuality >= 0) ? -4 : ant->pathQuality - 4;
                fprintf(metadataFile,"a%d segAng %.4f seg1 (%d Nodes %d %d l %.4f) seg2 (%d Nodes %d %d l %.4f) sinSegAngle %.4f maxDisplacement %.4f\n", ant->id, segAngle, *segIt, ant->internalSegs[*segIt]->startNode->id, ant->internalSegs[*segIt]->endNode->id, seg1Len, seg2ID, ant->internalSegs[seg2ID]->startNode->id, ant->internalSegs[seg2ID]->endNode->id, seg2Len, sinSegAngle, maxDisplacement);
		breakFlag = true;
	    } 
	    // General Bad case. vecAngle is between startNode, massCenter and endNode. vecAngle was calculated with the massCenter as the pivote node, before calling tourQuality function 
	    if (fabs(vecAngle) >= cellRestrictions->maxAxialDispl * cellRestrictions->angle){
		//ant->pathQuality = -2;
		ant->pathQuality = (ant->pathQuality >= 0) ? -2 : ant->pathQuality - 2;
		fprintf(metadataFile,"a%d vecAngle  %.4f\n", ant->id, vecAngle);
		breakFlag = true;
	    }
	    // Neuron extra general bad case: if ant's path ends in other endNode/startNode
	    if (cellRestrictions->cellIdentifier == 1 && ant->endNode->degree == 1){
		ant->pathQuality = (ant->pathQuality >= 0) ? -8 : ant->pathQuality - 8;
                fprintf(metadataFile,"a%d endNode %d endNodeType %d\n", ant->id, ant->endNode->id, ant->endNode->nType);
                breakFlag = true;
	    } 
	    // testing all criterias separately
	    if (!breakFlag) {
		// Advance interQuaEdgeIt to next conflicting edgePair
		interQuaEdgeIt++;
		// Update accumLen width latest segment length
		accumLen -= seg1Len;
		// Update ID and length of seg2ID to segIt as we advance in reverse
		seg2ID = *segIt;
		seg2Len = seg1Len;
	    }

	    //Ban/Penalize edge - path relation between interQuaEdge and 1st seg in reverse (last segment). Cant do it viceversa as it might delete viable
	    // solutions from 2nd seg thru that edge with a different destination that seg1
	    if (breakFlag){
		// Check if local segment is contained by global segment in ant->replacedSegs map/dict, then reduce probability by pheromoneBFactor %
		if (ant->replacedSegs.count(*segIt) > 0){
		    try {
			ant->colonyPtr->bannedPathsPherom.at(*interQuaEdgeIt).at(ant->replacedSegs[*segIt]) *= ant->colonyPtr->pheromoneBFactor;
			if (ant->colonyPtr->bannedPathsPherom.at(*interQuaEdgeIt).at(ant->replacedSegs[*segIt]) <= pow(ant->colonyPtr->pheromoneBFactor,2)){
			    ant->colonyPtr->bannedPathsPherom[*interQuaEdgeIt][ant->replacedSegs[*segIt]] = 0;
			}
		    } catch (...) {
			ant->colonyPtr->bannedPathsPherom[*interQuaEdgeIt][ant->replacedSegs[*segIt]] = ant->colonyPtr->pheromoneBFactor;
		    }
		} else {
		    try {
			ant->colonyPtr->bannedPathsPherom.at(*interQuaEdgeIt).at(*segIt) *= ant->colonyPtr->pheromoneBFactor;
			if (ant->colonyPtr->bannedPathsPherom.at(*interQuaEdgeIt).at(*segIt) <= pow(ant->colonyPtr->pheromoneBFactor,2)){
			    ant->colonyPtr->bannedPathsPherom[*interQuaEdgeIt][*segIt] = 0;
			}
		    } catch (...){
			ant->colonyPtr->bannedPathsPherom[*interQuaEdgeIt][*segIt] = ant->colonyPtr->pheromoneBFactor; 
		    }
		    // Update banned edge in respective segment in db
		    updateSegmentsDB(conn, *interQuaEdgeIt, *segIt);
		}
		// Debug info of edges in normal or extended segments in bannedPathsPherom
		if (debug == 4){
		    printf("a%d bPP[%d][%d]=%.4f Edges: ", ant->id, *interQuaEdgeIt, *segIt, ant->colonyPtr->bannedPathsPherom[*interQuaEdgeIt][*segIt]);
		    for (std::vector<int>::iterator edgeIt = ant->internalSegs[*segIt]->edges.begin(); edgeIt != ant->internalSegs[*segIt]->edges.end(); ++edgeIt){
			printf("%d ", *edgeIt);
		    }
		    if (ant->internalSegs[*segIt]->extension != nullptr){ // print edges of extension segment if it has one
			for (std::vector<int>::iterator extEdgeIt = ant->internalSegs[*segIt]->extension->edges.begin(); extEdgeIt != ant->internalSegs[*segIt]->extension->edges.end(); ++extEdgeIt){
			    printf("%d ", *extEdgeIt);
			}
			printf("extId %d", ant->internalSegs[*segIt]->extension->dbId);
		    }
		    printf("\n");
		}
		// Once this if is entered and processed, break forloop
		break;
	    }
	}
	// Special case of "perfect path" (only one segment) with bad ending. This is not caught by the for loop
	if (ant->segs.size() == 1){
	    // Neuron extra general bad case: if ant's path ends in other endNode/startNode
            if (cellRestrictions->cellIdentifier == 1 && ant->endNode->degree == 1){
                ant->pathQuality = (ant->pathQuality >= 0) ? -8 : ant->pathQuality - 8;
                fprintf(metadataFile,"a%d endNode %d endNodeType %d\n", ant->id, ant->endNode->id, ant->endNode->nType);
            }
	}

    }

    // Draw contours only for ants with pathQuality > 0
    if (ant->pathQuality > 0){
	// assign memory to antContour and populate it with node/cluster pixels. All white
        antPxls = cv::Mat(frameGray.size(), frameGray.type(), cv::Scalar(255));
        for(std::unordered_map<int,cv::Point2f>::iterator it = ant->nodeIDs.begin(); it != ant->nodeIDs.end(); it++){
            for(std::vector<std::pair<int,int>>::iterator p = clusterPointers[it->first]->pixelMembers.begin(); p != clusterPointers[it->first]->pixelMembers.end(); ++p) {
                antPxls.at<uchar>((*p).first, (*p).second) = 0;
            }
        } // Get contour
        cv::findContours(antPxls, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
	if (debug == 4){ // Draw contour if debugging 4 enabled
            debugContour = cv::Mat(frameGray.size(), frame.type(), cv::Scalar(255,255,255));
            //printf("A%d NumContours %ld \n", ant->id, contours.size());
        }
	// Last contour is image frame, ignore it
        for(size_t i = 0; i< contours.size() - 1; i++){
	    // Register index of contours with less than 3 elements (2 elems == point. So MPoly cant add them)
            if (contours[i].size() < 3){
		rejectedContour.push_back(i);
		continue; // Dont add to debugCountourMat
	    }
            if (debug == 4){
		randClr = rand() % 200;
                cv::drawContours(debugContour, contours, (int)i, cv::Scalar( randClr, 128, randClr/2));
                // fill contour with start and end node/cluster pixelMembers
                for(std::vector<std::pair<int,int>>::iterator pairIt = ant->startNode->parent->pixelMembers.begin(); pairIt != ant->startNode->parent->pixelMembers.end(); pairIt++){
		    try {
			debugContour.at<cv::Vec3b>((*pairIt).first, (*pairIt).second) = ant->startNode->color;
		    } catch (...){
			debugContour.at<cv::Vec3b>((*pairIt).first, (*pairIt).second) = cv::Vec3b(ant->color[0], ant->color[1], ant->color[2]);
		    }
                }
                for(std::vector<std::pair<int,int>>::iterator pairIt = ant->endNode->parent->pixelMembers.begin(); pairIt != ant->endNode->parent->pixelMembers.end(); pairIt++){
		    try {
			debugContour.at<cv::Vec3b>((*pairIt).first, (*pairIt).second) = ant->endNode->color;
		    } catch (...) {
			debugContour.at<cv::Vec3b>((*pairIt).first, (*pairIt).second) = cv::Vec3b(ant->color[0], ant->color[1], ant->color[2]);
		    }
                }

                // draw StartEndNodesLine
                cv::line(debugContour, ant->startNode->coord, ant->massCenter, ant->color, 1);
                cv::line(debugContour, ant->massCenter, ant->endNode->coord, ant->color, 1);
                // Enhance start and end Nodes 
                cv::circle(debugContour, ant->startNode->coord, 1, cv::Vec3b(0,0,255));
                cv::circle(debugContour, ant->endNode->coord, 1, cv::Vec3b(0,0,255));
                // massCenter
                cv::circle(debugContour, ant->massCenter, 1, cv::Vec3b(255,0,255));
		debugStringInfo = "mC:" + std::to_string((int)ant->massCenter.x) + "," + std::to_string((int)ant->massCenter.y) + " sN:" + std::to_string((int)ant->startNode->coord.x) + "," + std::to_string((int)ant->startNode->coord.y) + " eN:" + std::to_string((int)ant->endNode->coord.x) + "," + std::to_string((int)ant->endNode->coord.y);
		cv::putText(debugContour, debugStringInfo, cv::Point(0,8), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.6, cv::Scalar(255,0,255), 1, 8);
                cv::imwrite("/tmp/" + cellRestrictions->filename + "-Ant" + std::to_string(ant->id) + "-Contour" + std::to_string(i) + ".jpg", debugContour);
		// reset to white space
		cv::putText(debugContour, debugStringInfo, cv::Point(0,8), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.6, cv::Scalar(255,255,255), 1, 8);
            }
        }// Store contours of this ants
	ant->contours = contours;
	// Clean contours of rejectedContours
	for (int j : rejectedContour){contours.erase(contours.begin() + j);}
	// Insert contours as MPolygon into DB
	insertMPolyDB(conn, contours, ant->id);
    }
	
    // Update coveredEdges, pheromone and others
    if (ant->pathQuality >= ant->colonyPtr->maxThetaScore * ant->colonyPtr->minGoodQualityFactor){ // Mark this ant as a useful/good Quality Solution
	ant->colonyPtr->goodAnts[ant->id] = ant;
        for (std::map<int,Arista*>::iterator antTrace = ant->filament.begin(); antTrace != ant->filament.end(); antTrace++){
            // Increase counter of coveredEdges, except of the startingEdge, that already had one added at the birth of this ant and had removed the edge from pendingEdges
	    if (antTrace->first == ant->firstEdgeID){continue;}
            ant->colonyPtr->coveredEdges[antTrace->first] += 1;
            //if (debug == 4){printf("A%d-%d curv %.4f (%.4f/%.4f)\n", ant->id,antTrace->first, (antTrace->second->angle/antTrace->second->length),antTrace->second->angle,antTrace->second->length);}
	    // Notify edges that they are part of this good ant
	    antTrace->second->inAnt[ant->id] = 1;
	    // Delete from pendingEdges map
	    try {
		ant->colonyPtr->pendingEdges.at(antTrace->first);
		ant->colonyPtr->pendingEdges.erase(antTrace->first);
	    } catch (...){ // was already deleted
		continue;
	    }
        }
    } else {
	// Restock that starting edge to cellRestrictions->endEdges map;
        if (ant->filament[ant->firstEdgeID]->endEdge){ // Restock in endEdges Map
	    cellRestrictions->endEdges[ant->firstEdgeID] = 0; // 0 Means unnasigned for a ant to start from
	} else if (cellRestrictions->cellIdentifier != 1) { // Restocking in interQuaEdges for all but neuron cells
	    cellRestrictions->interQuaEdges[ant->firstEdgeID] = 0;
	}
	// In any case, substract 1 from coveredEdges and restock startingEdge of failed ant to pendingEdges
        ant->colonyPtr->coveredEdges[ant->firstEdgeID] -= 1;
	ant->colonyPtr->pendingEdges[ant->firstEdgeID] = 1;
    }
    printf("a%d Quality: %.4f\n", ant->id, ant->pathQuality);
    fprintf(metadataFile,"a%d Quality: %.4f vecAngle %.4f menger %.4f\n", ant->id, ant->pathQuality, vecAngle, ant->mengerCurvature);
    // insert node ids to db
    antNodes(conn, ant);
}

/* Delete solutions that are already representend by others/better one's
 * aka daemon Actions */
void globalEval(bool finalCall){
    cv::Mat tmp, tmp2;
    size_t i;
    std::vector<int> dupAnts, deactivatedAnts;
    std::map<int, Ant*>::iterator antIt;
    // 1st mark duplicate of each ant as inactive
    for(antIt = antColony->goodAnts.begin(); antIt != antColony->goodAnts.end(); ++antIt){
	deactivatedAnts = dropDupDBAnts(conn, antIt->first);
	// deactivate ants returned from dropDupDBAnts 
	for (std::vector<int>::iterator deactIt = deactivatedAnts.begin(); deactIt != deactivatedAnts.end(); ++deactIt){
	    try{antColony->goodAnts.at(*deactIt)->pathQuality = 0;
	    } catch (...){continue;}
	}
    }

    // Search for all ants with one segment, which should have the biggest segment or be within the supposedly biggest segment
    // If their segment is smaller than the biggest one available, downgrade ant to pathQ = 0 
    for(antIt = antColony->goodAnts.begin(); antIt != antColony->goodAnts.end(); ++antIt){
	if (antWithinDB(conn, antIt->first) || antIt->second->pathQuality == 0){ // Dup Ants are within goodAnts but were just marked as dups above. Skip this ones
	    antIt->second->pathQuality = 0;
	    dupAnts.push_back(antIt->first);
	    antIt->second->contours.clear(); // free contours vector
	    // clear this ant from all of its edges inAnt map
	    for (std::map<int, Arista*>::iterator it = antIt->second->filament.begin(); it != antIt->second->filament.end(); ++it){
		it->second->inAnt.erase(antIt->first);
		// update coveredEdges. Cant restock pendingEdges as this ant is a dup, meaning that a bigger ant already contains those edges
		antIt->second->colonyPtr->coveredEdges[it->first] -= 1;
	    }

	} else if (finalCall){
	    // draw contours on candidate ants mat
	    for(i = 0; i< antIt->second->contours.size() - 1; i++){
		cv::drawContours(joinedAnts, antIt->second->contours, (int)i, antIt->second->color);
	    }
	    printf("Ant %d, Quality %.4f avgAngle %.4f Nodes: [", antIt->first, antIt->second->pathQuality, antIt->second->avgAngle);
	    // add nodeIDs to metadataFile
	    fprintf(metadataFile, "Ant %d, Quality %.4f Nodes: [", antIt->first, antIt->second->pathQuality);
	    for (std::unordered_map<int,cv::Point2f>::iterator nodeIt = antIt->second->nodeIDs.begin(); nodeIt != antIt->second->nodeIDs.end(); ++nodeIt){
		printf("%d,", nodeIt->first);
		fprintf(metadataFile, "%d,", nodeIt->first);
	    }
	    printf("] Edges: [");
	    fprintf(metadataFile, "] Edges: [");
	    for (std::map<int,Arista*>::iterator edgeIt = antIt->second->filament.begin(); edgeIt != antIt->second->filament.end(); ++edgeIt){
                printf("%d,", edgeIt->first);
                fprintf(metadataFile, "%d,", edgeIt->first);
		// Avoid falsely marking pendingEdges if they're part of goodAnts
		// Cant +1 coveredEdges as all good ants already did it in antTourQuality function
		if (antIt->second->colonyPtr->coveredEdges[edgeIt->first] <= 0){
		    antIt->second->colonyPtr->coveredEdges[edgeIt->first] = 1;
		}
		try { // Just to mark that this good Ant uses this edge and therefore is no longer pending
                    antIt->second->colonyPtr->pendingEdges.erase(edgeIt->first);
                } catch (...){ //
		    continue;
                }
            }
	    printf("]\n");
            fprintf(metadataFile, "]\n");
	}
    }
    for (std::vector<int>::iterator it = dupAnts.begin(); it != dupAnts.end(); ++it){
	try {antColony->goodAnts.erase(*it);
	} catch (...){continue;}
    }
    printf("dupAnts %ld goodAnts %ld total ants %ld\n", dupAnts.size(), antColony->goodAnts.size(), antColony->colonyMap.size());
    printf("Edges: Covered %ld, Total %d, Ratio %.4f percent\n", (antColony->edges - antColony->pendingEdges.size()), antColony->edges, 1 - (antColony->pendingEdges.size()/(double)antColony->edges));
    if (finalCall){
        fprintf(metadataFile, "dupAnts %ld goodAnts %ld total ants %ld\n", dupAnts.size(), antColony->goodAnts.size(), antColony->colonyMap.size());
        fprintf(metadataFile, "Edges: Covered %ld, Total %d, Ratio %.4f percent\n", (antColony->edges - antColony->pendingEdges.size()), antColony->edges, 1 - (antColony->pendingEdges.size()/(double)antColony->edges));
	if (debug == 4){
	    tmp2 = frameGraph.clone();
	    for (std::unordered_map<int, int>::iterator it = antColony->pendingEdges.begin(); it != antColony->pendingEdges.end(); ++it){
		tmp = frameGraph.clone();
		printf("%d (%d %d) covEdgeValue %d\n", it->first, edgePointers[it->first]->end1->id, edgePointers[it->first]->end2->id, antColony->coveredEdges[it->first]);
		cv::line(tmp, edgePointers[it->first]->end1->coord, edgePointers[it->first]->end2->coord, cv::Scalar(0,255,0), 2);
		cv::line(tmp2, edgePointers[it->first]->end1->coord, edgePointers[it->first]->end2->coord, cv::Scalar(0,255,0), 2);
		cv::imwrite("/tmp/" + cellRestrictions->filename + "-MissingEdge" + std::to_string(it->first) + ".jpg", tmp);
	    }
	    cv::imwrite("/tmp/" + cellRestrictions->filename + "-Missing-" + std::to_string(antColony->pendingEdges.size()) + "-Edges.jpg", tmp2);
	}
    }
}

/* DB FUNCTIONS */
void exit_nicely(PGconn *con){
    PQfinish(con);
    //exit(1);
}

/* Insert new node row to nodes table */
void insertNodeDB(PGconn *con, Nodo node){
    char sql[400], xcoord_str[50], ycoord_str[50];
    sprintf(xcoord_str, "%d", (int)node.coord.x);
    sprintf(ycoord_str, "%d", (int)node.coord.y);
    strcpy(sql, "INSERT INTO nodes(id, filename, coord) VALUES (");
    strcat(sql, std::to_string(node.id).c_str());
    strcat(sql,",'");
    strcat(sql, cellRestrictions->filename.c_str());
    strcat(sql, "', ST_GeomFromText('POINT(");
    strcat(sql, xcoord_str);
    strcat(sql, " ");
    strcat(sql, ycoord_str);
    strcat(sql, ")', 4326) )");
    PGresult *res = PQexec(con, sql);	
    if (PQresultStatus(res) != PGRES_COMMAND_OK){fprintf(stderr, "Node %d (%f %f) INSERT [%s] failed: %s\n", node.id, node.coord.x, node.coord.y, sql, PQerrorMessage(con));}
    PQclear(res);
}

/* Returns id of segment added*/
int insertSegmentDB(PGconn *con, Nodo node1, Nodo node2){
    int ret_id; //, rows;
    char sql[400], id_str[10];
    strcpy(sql, "INSERT INTO segments(filename, segment, node_ids) VALUES ('");
    strcat(sql, cellRestrictions->filename.c_str());
    strcat(sql, "', ST_GeomFromText('LINESTRING(");
    strcat(sql, std::to_string(node1.coord.x).c_str());
    strcat(sql," ");
    strcat(sql, std::to_string(node1.coord.y).c_str());
    strcat(sql,",");
    strcat(sql, std::to_string(node2.coord.x).c_str());
    strcat(sql," ");
    strcat(sql, std::to_string(node2.coord.y).c_str());
    strcat(sql, ")', 4326), '{");
    strcat(sql, std::to_string(node1.id).c_str());
    strcat(sql, ",");
    strcat(sql, std::to_string(node2.id).c_str());
    strcat(sql, "}') RETURNING ID");
    PGresult *res = PQexec(con, sql);
    if (PQresultStatus(res) != PGRES_TUPLES_OK){fprintf(stderr, "Segment with nodes %d %d INSERT failed: %s - %s\n", node1.id, node2.id, PQerrorMessage(con), sql);}
    strcpy(id_str, PQgetvalue(res, 0, 0));
    ret_id = std::stoi(id_str);
    /*rows = PQntuples(res);
    if (rows){
    	ret_id = PQgetvalue(res, 0, 0);
    } else {
	ret_id = 0;
    }*/
    PQclear(res);
    return ret_id;
}

/* Clean previous data if it existed for "filename" image */
void deletePreviousDB(PGconn *con){
    char sql[300], sql2[300], sql3[300];
    strcpy(sql, "DELETE FROM segments WHERE filename = '");
    strcat(sql, cellRestrictions->filename.c_str());
    strcat(sql, "'");
    strcpy(sql2, "DELETE FROM nodes WHERE filename = '");
    strcat(sql2, cellRestrictions->filename.c_str());
    strcat(sql2, "'");
    strcpy(sql3, "DELETE FROM ant WHERE filename = '");
    strcat(sql3, cellRestrictions->filename.c_str());
    strcat(sql3, "'");
    PGresult *res = PQexec(con, sql);
    if (PQresultStatus(res) != PGRES_COMMAND_OK){fprintf(stderr, "Delete from Segments failed: %s\n", PQerrorMessage(con));}
    PQclear(res);
    res = PQexec(con, sql2);
    if (PQresultStatus(res) != PGRES_COMMAND_OK){fprintf(stderr, "Delete from nodes failed: %s\n", PQerrorMessage(con));}
    PQclear(res);
    res = PQexec(con, sql3);
    if (PQresultStatus(res) != PGRES_COMMAND_OK){fprintf(stderr, "Delete from Ant failed: %s\n", PQerrorMessage(con));}
    PQclear(res);
}

/* 
 *
 * select id, node_ids, array_length(node_ids,1) AS alen from segments where id != 19359 AND (select node_ids from segments where id = 19359) <@ node_ids order by alen DESC limit 1; 
 * <@ reflects "is contained by" */
int segWithinDB(PGconn *con, int local_seg){
    int segID = 0, rows;
    char sql[200], global_seg_str[10], id_str[10];
    sprintf(id_str, "%d", local_seg);
    strcpy(sql, "SELECT id from segments where id != ");
    strcat(sql, id_str);
    strcat(sql, " AND (select node_ids from segments where id = ");
    strcat(sql, id_str);
    PGresult *res = PQexec(con, strcat(sql,") <@ node_ids order by array_length(node_ids,1) DESC limit 1 ") );
    rows = PQntuples(res);
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        fprintf(stderr, "SELECT is_contained_by failed: %s\n", PQerrorMessage(con));
    } else if (rows > 0){ // If rows == 0, then this segment is not contained by any other segment
        strcpy(global_seg_str,PQgetvalue(res, 0, 0));
	segID = std::stoi(global_seg_str);
    }
    PQclear(res);
    return segID;
}

/* Check if ... */
int extendedSegWithinDB(PGconn *con, Segment *currSeg, std::unordered_map<int,cv::Point>& prevSegNodes){
    int segID = 0, rows;
    char global_seg_str[10];
    std::string sqlStr = "SELECT id from segments where '{" + std::to_string(currSeg->startNode->id) + "," + std::to_string(currSeg->endNode->id);
    for (std::unordered_map<int,cv::Point>::iterator it = prevSegNodes.begin(); it != prevSegNodes.end(); ++it){
	sqlStr += "," + std::to_string(it->first);
    }
    sqlStr += "}' <@ node_ids order by array_length(node_ids,1) DESC limit 1 ";
    PGresult *res = PQexec(con, sqlStr.c_str());
    rows = PQntuples(res);
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        fprintf(stderr, "SELECT Extended_is_contained_by failed: %s\n", PQerrorMessage(con));
    } else if (rows > 0){ // If rows == 0, then this segment is not contained by any other segment
        strcpy(global_seg_str,PQgetvalue(res, 0, 0));
        segID = std::stoi(global_seg_str);
    }
    PQclear(res);
    return segID;
}

/* Add new point to Linestring geometry and to node array */
void extendSegDB(PGconn *con, int seg_id, Nodo new_node){
    char sql[200];
    strcpy(sql, "UPDATE segments SET segment = ST_AddPoint(segment, ST_Point(");
    strcat(sql, std::to_string(new_node.coord.x).c_str());
    strcat(sql, ",");
    strcat(sql, std::to_string(new_node.coord.y).c_str());
    strcat(sql, ")), node_ids = array_append(node_ids, ");
    strcat(sql, std::to_string(new_node.id).c_str());
    strcat(sql, ") WHERE id = ");
    PGresult *res = PQexec(con, strcat(sql, std::to_string(seg_id).c_str()) ) ;
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {fprintf(stderr, "UPDATE extendSegDB failed: %s\n", PQerrorMessage(con));}
    PQclear(res);
}

/* add data to the banned_edge column in segments table */
void updateSegmentsDB(PGconn *con, int edgeid, int segid){
    char sql[100], edgeid_str[10], segid_str[10];
    sprintf(edgeid_str, "%d", edgeid);
    sprintf(segid_str, "%d", segid);
    strcpy(sql, "UPDATE segments SET banned_edge = ");
    strcat(sql, edgeid_str);
    strcat(sql, " WHERE id = ");
    PGresult *res = PQexec(con, strcat(sql, segid_str));
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {fprintf(stderr, "UPDATE banned_edge failed: %s\n", PQerrorMessage(con));}
    PQclear(res);
}

/* Once an ant/filament is proposed, insert it into the ant table */
void insertMPolyDB(PGconn *con, std::vector<std::vector<cv::Point>> contours, int ant_id){
    size_t i;
    std::string sql = "INSERT INTO ant (id, filename, num_contours, contour) VALUES (" + std::to_string(ant_id) + ",'" + cellRestrictions->filename + "'," + std::to_string(contours.size() - 1) + ", ST_GeomFromText('MULTIPOLYGON(";

    for(i = 0; i< contours.size() - 1; i++){
	std::vector<cv::Point>::iterator cvPnt = contours[i].begin();
	// comma after first polygon is closed IF there are more polygons
	if (i > 0){sql += ",";}
	// Save 1st point to close polygon
	sql += "((";
	// Get points of each polygon per contour
	for(; cvPnt != contours[i].end(); cvPnt++){
	    sql += std::to_string((*cvPnt).x);
	    sql += " ";
	    sql += std::to_string((*cvPnt).y);
	    sql += ",";
	}
	cvPnt = contours[i].begin();
	sql += std::to_string((*cvPnt).x);
	sql += " ";
	sql += std::to_string((*cvPnt).y);
	sql += "))";
    }
    sql += ")', 4326))";
    PGresult *res = PQexec(con, sql.c_str());
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {fprintf(stderr, "INSERT MPoly [%s] failed: %s\n", sql.c_str(), PQerrorMessage(con));}
    PQclear(res);

    sql = "UPDATE ant SET area = (SELECT ST_Area(contour) from ant where id = " + std::to_string(ant_id) + " AND filename = '" + cellRestrictions->filename + "' LIMIT 1)," ;
    sql += " perimeter = (SELECT ST_Perimeter(contour) from ant where id = " + std::to_string(ant_id) + " AND filename = '" + cellRestrictions->filename + "' LIMIT 1)";
    sql += " WHERE id = " + std::to_string(ant_id) + " AND filename = '" + cellRestrictions->filename + "'";
    res = PQexec(con, sql.c_str());
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {fprintf(stderr, "UPDATE Area & Perimeter [%s] MPoly failed: %s\n", sql.c_str(), PQerrorMessage(con));}
    PQclear(res);
}

/*double avgWidthMPolyDB(PGconn *con, int ant_id){
    char sql[50], antid_str[10], avgWidth_str[40];
    double avgWidth;
    sprintf(antid_str, "%d", ant_id);

    strcpy(sql, "SELECT 2*area/perimeter from ant where id = ");
    PGresult *res = PQexec(con, strcat(sql, antid_str));
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        fprintf(stderr, "SELECT avgWidth [%s] failed: %s\n", sql, PQerrorMessage(con));
        avgWidth = 0;
    } else {
        strcpy(avgWidth_str,PQgetvalue(res, 0, 0));
        avgWidth = std::stod(avgWidth_str);
    }
    PQclear(res);
    return avgWidth;
}*/

// At tour end, insert into db the nodes that the ant passed through
void antNodes(PGconn *con, Ant* ant){
    std::string sql = "UPDATE ant SET nodes = '{";
    std::unordered_map<int,cv::Point2f>::iterator it;

    it = ant->nodeIDs.begin();
    sql += std::to_string(it->first);
    ++it;
    for (; it != ant->nodeIDs.end(); ++it){
	sql += ",";
	sql += std::to_string(it->first);
    }
    sql += "}' where id = " + std::to_string(ant->id) + " AND filename = '" + cellRestrictions->filename + "'";
    PGresult *res = PQexec(con, sql.c_str());
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {fprintf(stderr, "UPDATE ant Nodes [%s] failed: %s\n", sql.c_str(), PQerrorMessage(con));}
    PQclear(res);
}

/* Each ant must test if its contained already in other;
 * <@ reflects "is contained by" */
bool antWithinDB(PGconn *con, int ant_id){
    int rows;
    bool containedBy = false;
    std::string sql = "SELECT id from ant where id != " + std::to_string(ant_id) + " AND filename = '" + cellRestrictions->filename;
    sql += "' AND active = 't' AND (select nodes from ant where id = " + std::to_string(ant_id) + " AND filename = '" + cellRestrictions->filename + "' AND active = 't') <@ nodes";

    PGresult *res = PQexec(con, sql.c_str());
    rows = PQntuples(res);
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        fprintf(stderr, "ANT SELECT is_contained_by [%s] failed: %s\n", sql.c_str(), PQerrorMessage(con));
    } else if (rows > 0){ // If rows == 0, then this ant is not contained by any other ant
        containedBy = true;
	// In case of debugging print the ants that contain this ant
	if (debug == 4){
	    printf("Ant %d contained by ant ", ant_id);
	    for(int i = 0; i < rows; i++){printf("%s ", PQgetvalue(res, i, 0));}
	    printf("\n");
	}
    }
    PQclear(res);
    return containedBy;
}

/* Mark all ants that are exactly equal in db as ant_id as inactive/active = 'f' */
std::vector<int> dropDupDBAnts(PGconn *con, int ant_id){
    int i, rows;
    char ret_id_str[10];
    std::vector<int> deactivatedAnts;
    // Skip update on already deactivated ants
    std::string sql = "UPDATE ant SET active = 'f' WHERE active = 't' AND id !=" + std::to_string(ant_id) + " AND filename = '" + cellRestrictions->filename;
    sql += "' AND (SELECT nodes FROM ant WHERE id = " + std::to_string(ant_id) + " AND active = 't' AND filename = '" + cellRestrictions->filename + "') <@ nodes";
    sql += " AND nodes <@ (SELECT nodes FROM ant WHERE id = " + std::to_string(ant_id) + " AND active = 't' AND filename = '" + cellRestrictions->filename + "') RETURNING id";

    PGresult *res = PQexec(con, sql.c_str() );
    rows = PQntuples(res);
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {fprintf(stderr, "Inactivate Equal Ants [%s] failed: %s\n", sql.c_str(), PQerrorMessage(con)); }
    if (debug == 4 && rows > 0){printf("Ant %d deactivates ant ", ant_id);}
    for(i = 0; i < rows; i++){
	strcpy(ret_id_str,PQgetvalue(res, i, 0));
	deactivatedAnts.push_back(std::stoi(ret_id_str));
	// In case of debugging print the ants that are deactivated by this ante
	if (debug == 4){printf("%s ", PQgetvalue(res, i, 0));}
    }
    if (debug == 4 && rows > 0){printf("\n");}
    PQclear(res);
    return deactivatedAnts;
}

/*bool cluster_sorter(const Cluster* lhs, const Cluster* rhs) {
    return lhs->pixelMembers.size() < rhs->pixelMembers.size();
}*/

bool sortPairDesc(const std::pair<int,int> &lft, const std::pair<int,int> &rght){ 
       return (lft.second > rght.second);
}
