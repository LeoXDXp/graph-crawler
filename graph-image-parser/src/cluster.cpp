#include "cluster.hpp"
#include "nodes.hpp"
#include "graph.hpp"
#include <opencv2/highgui/highgui.hpp> // cv::imread for debugMat
#include <exception>
#include <fstream> // std::fstream::binary
#include <json/json.h> // jsoncpp

/* Set initial values */
Cluster* createCluster(int clusterid){
    Cluster *newCluster = new Cluster();
    newCluster->id = clusterid;
    newCluster->clusterType = -1;
    newCluster->pxlCounterX = newCluster->pxlCounterY = 1;
    newCluster->color[0] = rand() % 250;
    newCluster->color[1] = rand() % 200;
    newCluster->color[2] = rand() % 250;
    newCluster->angle = newCluster->magnitude = newCluster->lambdaRatio = 0.0;
    newCluster->replaceCluster = nullptr;
    newCluster->m10 = newCluster->m01 = newCluster->m11 = newCluster->m20 = newCluster->m02 = 0;
    newCluster->nxId = -1;
    return newCluster;
}

/* Check the 8 neighbors, if pxlMetadata goes back to Vec1w (ushort, 2 bytes, max 65536. Need to re-add 
 * if clause for max clusterID. Asign the cluster ID to the pixel neighbors, only if they dont have a cluster already assigned */
void checkNeighbors(int i, int j, double neighborThreshold, cv::Mat& borderFrame, bool alreadyAssigned){
    int clusterid = pxlMetadata.at<int>(i, j), neighborCluster;
    int x_ctr, y_ctr, x, y;
    double tmp_sqrt, eigenVal1, eigenVal2;
    double tmp_m00, tmp_m10, tmp_m01, tmp_m11, tmp_m20, tmp_m02, tmp_covMatU11, tmp_covMatU20, tmp_covMatU02, lambdaRatio, orientation;
    cv::Point2f tmp_massCenter;
    std::unordered_map<int, Cluster*>::iterator it;
    /* Top Left: x-1,j-1. Top i-1, j; Top Right i-1, j+1; Left i, j-1; 
     * Right: i, j+1; Bottom Left: i+1, j-1; Bottom: i+1, j; Bottom Right: i+1, j+1; 
     * -2 to 3 for 5x5*/
    for (x_ctr = cellRestrictions->kernelSizeLowerLimit; x_ctr < cellRestrictions->kernelSize - 1; x_ctr++){
        x = i+x_ctr;
        for (y_ctr = cellRestrictions->kernelSizeLowerLimit; y_ctr < cellRestrictions->kernelSize - 1; y_ctr++){
            y = j+y_ctr;
            /* Skip i,j*/
            if (x_ctr == 0 && y_ctr == 0) {continue;}
	    /* if it's marked as unassignable, pass */
	    if (pxlMetadata.at<int>(x,y) == -1) {continue;}
	    if (clusterPointers[clusterid]->pxlCounterX >= cellRestrictions->maxThickness) {continue;}
            /* check threshold and also if no cluster has been asigned. Avoid counting it twice */
            if (borderFrame.at<uchar>(x,y) < neighborThreshold) { // image is inverted here, so 255 is bg
                /* if no cluster has been assigned */
                neighborCluster = pxlMetadata.at<int>(x,y);
                if (neighborCluster == 0) {
		    /* temporal calculations */
		    tmp_m00 = clusterPointers[clusterid]->pixelMembers.size() + 1;
		    tmp_m10 = clusterPointers[clusterid]->m10 + x;
		    tmp_m01 = clusterPointers[clusterid]->m01 + y;
		    // Inverted
		    tmp_massCenter = cv::Point2f(tmp_m01/tmp_m00, tmp_m10/tmp_m00);
		    tmp_m11 = clusterPointers[clusterid]->m11 + x*y ;
		    tmp_m20 = clusterPointers[clusterid]->m20 + pow(x,2);
		    tmp_m02 = clusterPointers[clusterid]->m02 + pow(y,2);
		    tmp_covMatU11 = (tmp_m11/tmp_m00) - tmp_massCenter.x * tmp_massCenter.y;
		    tmp_covMatU20 = (tmp_m20/tmp_m00) - pow(tmp_massCenter.y,2);
		    tmp_covMatU02 = (tmp_m02/tmp_m00) - pow(tmp_massCenter.x,2);;
		    tmp_sqrt = sqrt(4*pow(tmp_covMatU11,2) + pow((tmp_covMatU20 - tmp_covMatU02),2) ) /2;
		    eigenVal1 = ((tmp_covMatU20 + tmp_covMatU02) / 2) + tmp_sqrt;
                    eigenVal2 = ((tmp_covMatU20 + tmp_covMatU02) / 2) - tmp_sqrt;
		    lambdaRatio = (eigenVal1 > eigenVal2) ? eigenVal1/eigenVal2 : eigenVal2/eigenVal1;
		    /* if eigenVal1 or 2 is zero */
		    if (std::isinf(lambdaRatio)) {lambdaRatio = eigenVal1;}
		    /* Get orientation only if both U20 and U02 are not zero*/
		    if (tmp_covMatU20 != 0 && tmp_covMatU02 != 0){orientation = 0.5 * atan(2*tmp_covMatU11/(tmp_covMatU20 - tmp_covMatU02)) * 180 / M_PI;}
		    
		    if (debug == 2){
                        fprintf(debugL2File, "cID: %d neiClust %d coords %d %d pxCY %d pxCX %d Mass %f %f color %d %d %d lam1 %f lam2 %f lRatio %f m00 %f m01 %f m10 %f m11 %f m02 %f m20 %f U11 %f U02 %f U20 %f sqrt %f orient %f\n", clusterid, neighborCluster, y-1, x-1, clusterPointers[clusterid]->pxlCounterX, clusterPointers[clusterid]->pxlCounterY, tmp_massCenter.y, tmp_massCenter.x, clusterPointers[clusterid]->color[0], clusterPointers[clusterid]->color[1], clusterPointers[clusterid]->color[2], eigenVal1, eigenVal2, lambdaRatio, tmp_m00, tmp_m01, tmp_m10, tmp_m11, tmp_m02, tmp_m20, tmp_covMatU11, tmp_covMatU20, tmp_covMatU02, tmp_sqrt, orientation);
                    }

		    /* Only add this pixel if the criteria is met */
		    /*TODO: 1st avoid clusters too big*/
		    if(clusterPointers[clusterid]->pxlCounterY >= cellRestrictions->maxThickness) {continue;}
		    //if (eigenVal2 !=0 && eigenVal1 !=0 && lambdaRatio < 1.5) {continue;}
		    /* Update spatial/raw Moments (m_ij) and covariace matrix elements (covMatU_ij) */
                    //clusterPointers[clusterid]->pixelMembers.size() = tmp_m00; //update area aka m00
		    clusterPointers[clusterid]->m10 = tmp_m10;
		    clusterPointers[clusterid]->m01 = tmp_m01;
		    clusterPointers[clusterid]->massCenter = tmp_massCenter;
		    clusterPointers[clusterid]->m11 = tmp_m11;
		    clusterPointers[clusterid]->m20 = tmp_m20;
		    clusterPointers[clusterid]->m02 = tmp_m02;
		    /* covariance matrix of image/blob been constructed */
		    clusterPointers[clusterid]->covMatU11 = tmp_covMatU11; 
                    clusterPointers[clusterid]->covMatU02 = tmp_covMatU02;
		    clusterPointers[clusterid]->covMatU20 = tmp_covMatU20;
		    /* from the covMatrix get the eccentricity. Could also provide orientation */
		    clusterPointers[clusterid]->eigenVal1 = eigenVal1;
		    clusterPointers[clusterid]->eigenVal2 = eigenVal2;
		    clusterPointers[clusterid]->lambdaRatio = lambdaRatio;
		    clusterPointers[clusterid]->angle = orientation;

		    /* x|y + -1 for 3x3 and x|y + -2 for 5x5 */
                    updateMinMaxXY(x + cellRestrictions->kernelSizeLowerLimit, y + cellRestrictions->kernelSizeLowerLimit,clusterPointers[clusterid]);
                    pxlMetadata.at<int>(x,y) = clusterid;
		    if (debug == 2){
		    	debugMat.at<cv::Vec3b>(x + cellRestrictions->kernelSizeLowerLimit, y + cellRestrictions->kernelSizeLowerLimit) = clusterPointers[clusterid]->color;
			debugMat.at<cv::Vec3b>(tmp_massCenter.y, tmp_massCenter.x) = cv::Vec3b(0,0,255);
			//debugMat.at<cv::Vec3b>(clusterPointers[clusterid]->minX, clusterPointers[clusterid]->minY) = cv::Vec3b(0,0,255);
			//debugMat.at<cv::Vec3b>(clusterPointers[clusterid]->maxX, clusterPointers[clusterid]->maxY) = cv::Vec3b(0,255,0);
			//debugMat.at<cv::Vec3b>(clusterPointers[clusterid]->minX, clusterPointers[clusterid]->maxY) = cv::Vec3b(0,255,128);
			//debugMat.at<cv::Vec3b>(clusterPointers[clusterid]->maxX, clusterPointers[clusterid]->minY) = cv::Vec3b(128,255,0);
		    }
                    /* pixelMembers must be related to frame dims, not to borderFrame dims */
                    clusterPointers[clusterid]->pixelMembers.push_back(std::make_pair(x + cellRestrictions->kernelSizeLowerLimit, y + cellRestrictions->kernelSizeLowerLimit));
		    //clusterPointers[clusterid]->magnitude += gradient.at<uchar>(y + cellRestrictions->kernelSizeLowerLimit, x + cellRestrictions->kernelSizeLowerLimit);
		    // If this pixel is part of Skel, add to skelPxls vector. O (black) means it is part of skel
		    if (thinning.at<uchar>(x + cellRestrictions->kernelSizeLowerLimit, y + cellRestrictions->kernelSizeLowerLimit) == 0){
			clusterPointers[clusterid]->skelPxls.push_back(cv::Point(x + cellRestrictions->kernelSizeLowerLimit, y + cellRestrictions->kernelSizeLowerLimit));
		    }
                } else { /* declare neighbor cluster as neighbor cluster. Both clusters already exists, so increment newCluster counter */
                    if (neighborCluster == clusterid) { /* skip to avoid adding the cluster to it's own neighboursMap */
                        continue;
                    }
                    /* if the cluster's neighboursMap is empty in the id to be added, increment newCluster counter */
                    if (clusterPointers[neighborCluster]->neighboursMap.count(clusterPointers[clusterid]->id) == 0) {
                        /* add this cluster (clusterPointers[clusterid]) to neighborCluster neighbours map */
                        clusterPointers[neighborCluster]->neighboursMap[clusterPointers[clusterid]->id] = clusterPointers[clusterid];
			// add 1 pixel to the neighborCluster's neighborBorderPxlCounter map
			clusterPointers[neighborCluster]->neighborBorderPxlCounter[clusterPointers[clusterid]->id] = 1;
                    } else { // the neighborCluster already had this cluster added, increase the neighborBorderPxlCounter in 1
			clusterPointers[neighborCluster]->neighborBorderPxlCounter[clusterPointers[clusterid]->id] += 1;
		    }

                    if (clusterPointers[clusterid]->neighboursMap.count(clusterPointers[neighborCluster]->id) == 0) {
                        /* add neighborCluster cluster to this cluster (clusterPointers[clusterid]) neighbours map */
                        clusterPointers[clusterid]->neighboursMap[clusterPointers[neighborCluster]->id] = clusterPointers[neighborCluster];
			// add 1 pixel to this cluster's (clusterPointers[clusterid]) neighborBorderPxlCounter for the neighborCluster just added to clusterPointers[clusterid]->neighboursMap
			clusterPointers[clusterid]->neighborBorderPxlCounter[clusterPointers[neighborCluster]->id] = 1;
                    } else { // this cluster (clusterid) already had the neighborCluster initial pixel added to it's neighborBorderPxlCounter. So, add another
			clusterPointers[clusterid]->neighborBorderPxlCounter[clusterPointers[neighborCluster]->id] += 1;
		    }
		    /* Initialize key of edgePointersByNode, with value null, to make it easier to check for loops later */
		    edgePointersByNode[std::make_pair(clusterid, neighborCluster)] = nullptr;
		    edgePointersByNode[std::make_pair(neighborCluster, clusterid)] = nullptr;

		    // If this pixel is in skel, it probably marks the "bridge" point joining 2 clusters
		    /*if (thinning.at<uchar>(x + cellRestrictions->kernelSizeLowerLimit, y + cellRestrictions->kernelSizeLowerLimit) == 0){
			// If colisions occur, modify value of skelNeighConn to be vec<Points>
			skelNeighConn[std::make_pair(clusterid, neighborCluster)] = cv::Point(x + cellRestrictions->kernelSizeLowerLimit, y + cellRestrictions->kernelSizeLowerLimit);
			skelNeighConn[std::make_pair(neighborCluster, clusterid)] = cv::Point(x + cellRestrictions->kernelSizeLowerLimit, y + cellRestrictions->kernelSizeLowerLimit);
		    }*/
                }
		if (debug == 2){
		    /* debugging Mat */
		    cv::namedWindow("debug", cv::WINDOW_NORMAL);
        	    cv::imshow("debug", debugMat);
		    cv::waitKey(50);
		}
            }
        }
    }
    /* Update pxlCounterX|Y, for corner case */
    if (!alreadyAssigned) {clusterPointers[clusterid]->pixelMembers.push_back(std::make_pair(i + cellRestrictions->kernelSizeLowerLimit, j + cellRestrictions->kernelSizeLowerLimit));}
}

/* Paint clusters. Assign clusterType */
void paintClusters(std::pair<const int, Cluster*> &element){
    int x, y;
    for(std::vector<std::pair<int,int>>::iterator p = element.second->pixelMembers.begin(); p != element.second->pixelMembers.end(); ++p) {
        x = (*p).first; y = (*p).second;
        cluster.at<cv::Vec3b>(x,y) = element.second->color;
    }
    //frame.at<cv::Vec3b>(element.second->massCenter.y, element.second->massCenter.x) = element.second->color;
    updateClusterType(element);
}

/* Assign clusterType */
void updateClusterType(std::pair<const int, Cluster*> &element){
    if (element.second->neighboursMap.size() == 0){ // 0 neigbors, island
        element.second->clusterType = 0;
    } else if (element.second->neighboursMap.size() == 1){ // Some sort of end Cluster
	element.second->clusterType = 1;
    } else if (element.second->neighboursMap.size() == 2) { // Bridge Cluster
        element.second->clusterType = 2;
    } else { // Connection Cluster
        element.second->clusterType = 3;
    }
}

/* Delete clusters in map*/
void freeClusters(std::pair<const int, Cluster*> &element){
    delete element.second;
}

/* merge clusters/nodes that exceed the minimum (cellRestrictions->connectivityThresh) i
 * or maximum (cellRestrictions->maxThickness) distance constrains */
void mergeClusters(std::pair<const int, Cluster*> &element){
    double tmpLength;
    cv::Mat tmp;
    cv::Scalar c1, c2;
    std::unordered_map<int, Cluster*>::iterator it, next_it, next_node, neighNeigh_it;
    std::vector<std::pair<int,int>>::iterator p;
    /* skip logically merged or deleted clusters/nodes */
    if (element.second->clusterType == -2 || element.second->clusterType == -3){return;}

    for (it = element.second->neighboursMap.begin(), next_node = it, next_it = it; it != element.second->neighboursMap.end(); it = next_it, next_node = next_it){
	/* In case of triangular deletions, next_it can be pointing to nullptr */
	if (next_it->second == nullptr){
	    ++next_it;
	    continue;
	}
	++next_node;
        ++next_it;
	if (debug == 3){
	    if (next_node != element.second->neighboursMap.end()){fprintf(stderr, "e %d it %d nxit %d nxnd %d\n", element.first, it->first, next_it->first, next_node->first);}
	    else {fprintf(stderr, "e %d it %d nxit -1 nxnd -1\n", element.first, it->first);}
	    for (std::unordered_map<int, Cluster*>::iterator tmpit = element.second->neighboursMap.begin(); tmpit != element.second->neighboursMap.end(); tmpit++){
		fprintf(stderr, "%d ", tmpit->first);
	    }
	    fprintf(stderr, "\n");
	}
    	tmpLength = euclideanDist(element.second->massCenter, it->second->massCenter);
	/* if lenght of posible new edge is smaller than connectivityThresh, nodes must be merged. Also if the pixelMembers.size() is too low */
	if ((tmpLength <= cellRestrictions->connectivityThresh && element.second->neighboursMap.size() > 2) || it->second->pixelMembers.size() <= cellRestrictions->maxThickness * 1.5){
	    // If neighbor's pixelMembers.size() is too low, but this cluster is not its biggest neighborBorderPxlCounter, then skip.
	    //if (it->second->pixelMembers.size() <= cellRestrictions->maxThickness * 1.5 && element.first != it->second->largestPxlNeighbor){continue;}
	    /* debug: Paint in nodesMat the before, and in frame, the merged node */
	    if (debug >= 1){
	    	c1 = cv::Scalar(element.second->color[0], element.second->color[1], element.second->color[2]);
	    	c2 = cv::Scalar(it->second->color[0], it->second->color[1], it->second->color[2]);
	    	cv::circle(nodesMat, element.second->massCenter, 1, c1);
	    	cv::circle(nodesMat, it->second->massCenter, 1, c2);
	    }
	    /* 1st: Delete this node from neighbor's neighboursMap and iterate over the rest to add non common neighbours to this node's neighboursMap*/
	    it->second->neighboursMap.erase(element.first); //avoids self loops
	    /* 1.1: Update the reference of the neighbors inside it->second->neighboursMap.begin(), to point to this cluster (element.second) instead of the original cluster (it->second) */
	    for (neighNeigh_it = it->second->neighboursMap.begin(); neighNeigh_it != it->second->neighboursMap.end(); neighNeigh_it++){
		neighNeigh_it->second->neighboursMap.erase(it->first);
		// Delete entry in edgePointersByNode map
		edgePointersByNode.erase(std::make_pair(it->first, neighNeigh_it->first));
		edgePointersByNode.erase(std::make_pair(neighNeigh_it->first, it->first));
		/* insert the new reference to this cluster (element.second) if it doesnt exist */
		if (neighNeigh_it->second->neighboursMap.count(element.first) == 0){
		    neighNeigh_it->second->neighboursMap[element.first] = element.second;
		}
	    }
	    /* Insert doesnt overwrite existing */
	    element.second->pixelMembers.insert(element.second->pixelMembers.end(), it->second->pixelMembers.begin(), it->second->pixelMembers.end());
	    element.second->neighboursMap.insert(it->second->neighboursMap.begin(), it->second->neighboursMap.end());
	    element.second->m10 += it->second->m10;
	    element.second->m01 += it->second->m01;
	    element.second->m11 += it->second->m11;
	    element.second->m20 += it->second->m20;
	    element.second->m02 += it->second->m02;
	    // Inverted
	    element.second->massCenter = cv::Point2f(element.second->m01/element.second->pixelMembers.size(), element.second->m10/element.second->pixelMembers.size());
	    // Annotate as absorbed Cluster
            element.second->absorbedClusters[it->first] = 1;
	    /* covariance matrix of merged cluster */
	    updateCovMat(element.second);
	    updateMinMaxXY(it->second->maxX, it->second->maxY, element.second);
	    updateMinMaxXY(it->second->minX, it->second->minY, element.second);
	    if (debug >= 1) {cv::circle(frame, element.second->massCenter, 1, c1);}
	    /* 2nd Update cluster Mat */
	    for(std::vector<std::pair<int,int>>::iterator p = it->second->pixelMembers.begin(); p != it->second->pixelMembers.end(); ++p) {
                cluster.at<cv::Vec3b>((*p).first, (*p).second) = element.second->color;
            }
	    cluster.at<cv::Vec3b>(element.second->massCenter.y, element.second->massCenter.x) = cv::Vec3b(0,0,255);
	    /* 3rd: Mark neighbor node as merged. then delete it (the neighbor node) from this node's neighboursMap */
	    it->second->clusterType = -2;
	    it->second->replaceCluster = element.second;
	    it->second->pixelMembers.clear();
	    element.second->neighboursMap.erase(it->first);
	    // Delete entry in edgePointersByNode map
	    edgePointersByNode.erase(std::make_pair(it->first, element.first));
	    edgePointersByNode.erase(std::make_pair(element.first, it->first));
	    clusterID--;
	    continue;
	}
	/* if tmpLength >= cellRestrictions->maxThickness remove node from neighbourhood 
	 * needs restriction of numNeighbors, to protect destroying clear filament segments */
	/*if (tmpLength > cellRestrictions->maxThickness && element.second->numNeighbors > 3){
	    element.second->neighboursMap.erase(it->first);
	}*/
	/* Other cause of merger is if this cluster (representend by its node in its massCenter) forms a with 2 of its neighbors (it, next_node) */
	/* If next_node->first (id) is in it->second->neighboursMap, that implies a triangule between this 3 exist */
	while (next_node != element.second->neighboursMap.end()){
	    if (edgePointersByNode.count(std::make_pair(it->first, next_node->first)) > 0){
		// Check some criteria: area, circumcircle within
		if (!cellRestrictions->withCycles){break;} // TODO: Improve this criteria with a real restriction
		// triple merge 
		// 1st, mark it and next_node as merge
		it->second->clusterType = -2;
		next_node->second->clusterType = -2;
		it->second->replaceCluster = element.second;
		next_node->second->replaceCluster = element.second;
		// Break the relations/neighborhood data
		it->second->neighboursMap.erase(element.first);
		it->second->neighboursMap.erase(next_node->first);
		next_node->second->neighboursMap.erase(element.first);
		next_node->second->neighboursMap.erase(it->first);
		// Delete it and next_node from their other neighbors
		for (neighNeigh_it = it->second->neighboursMap.begin(); neighNeigh_it != it->second->neighboursMap.end(); neighNeigh_it++){
		    neighNeigh_it->second->neighboursMap.erase(it->first);
		    // Delete entry in edgePointersByNode map
		    edgePointersByNode.erase(std::make_pair(it->first, neighNeigh_it->first));
		    edgePointersByNode.erase(std::make_pair(neighNeigh_it->first, it->first));
                    // insert the new reference to this cluster (element.second) if it doesnt exist
                    if (neighNeigh_it->second->neighboursMap.count(element.first) == 0){
                    	neighNeigh_it->second->neighboursMap[element.first] = element.second;
                    }
            	}
		for (neighNeigh_it = next_node->second->neighboursMap.begin(); neighNeigh_it != next_node->second->neighboursMap.end(); neighNeigh_it++){
                    neighNeigh_it->second->neighboursMap.erase(next_node->first);
		    // Delete entry in edgePointersByNode map
		    edgePointersByNode.erase(std::make_pair(next_node->first, neighNeigh_it->first));
		    edgePointersByNode.erase(std::make_pair(neighNeigh_it->first, next_node->first));
                    /* insert the new reference to this cluster (element.second) if it doesnt exist 
		     * aka: it they dont know me (element.first), add me */
                    if (neighNeigh_it->second->neighboursMap.count(element.first) == 0){
                        neighNeigh_it->second->neighboursMap[element.first] = element.second;
                    }
                }

		/* Insert doesnt overwrite existing */
		element.second->pixelMembers.insert(element.second->pixelMembers.end(), it->second->pixelMembers.begin(), it->second->pixelMembers.end());
		element.second->pixelMembers.insert(element.second->pixelMembers.end(), next_node->second->pixelMembers.begin(), next_node->second->pixelMembers.end());
            	element.second->m10 += it->second->m10 + next_node->second->m10;
            	element.second->m01 += it->second->m01 + next_node->second->m01;
            	element.second->m11 += it->second->m11 + next_node->second->m11;
            	element.second->m20 += it->second->m20 + next_node->second->m20;
            	element.second->m02 += it->second->m02 + next_node->second->m02;
            	// Inverted
            	element.second->massCenter = cv::Point2f(element.second->m01/element.second->pixelMembers.size(), element.second->m10/element.second->pixelMembers.size());
		// Annotate as absorbed Cluster
		element.second->absorbedClusters[it->first] = 1;
		element.second->absorbedClusters[next_node->first] = 1;
		/* covariance matrix of merged cluster */
		updateCovMat(element.second);
		updateMinMaxXY(it->second->maxX, it->second->maxY, element.second);
            	updateMinMaxXY(it->second->minX, it->second->minY, element.second);

            	if (debug >= 1) {cv::circle(frame, element.second->massCenter, 1, c1);}
            	/* 2nd Update cluster Mat */
            	for(p = it->second->pixelMembers.begin(); p != it->second->pixelMembers.end(); ++p) {
                    cluster.at<cv::Vec3b>((*p).first, (*p).second) = element.second->color;
            	}
		for(p = next_node->second->pixelMembers.begin(); p != next_node->second->pixelMembers.end(); ++p) {
                    cluster.at<cv::Vec3b>((*p).first, (*p).second) = element.second->color;
                }
            	cluster.at<cv::Vec3b>(element.second->massCenter.y, element.second->massCenter.x) = cv::Vec3b(0,255,0);
            	/* 3rd: Mark neighbor node as merged. then delete it (the neighbor node) from this node's neighboursMap */
            	it->second->pixelMembers.clear();
		next_node->second->pixelMembers.clear();
		/* If next_node is pointing to the same spot as next_it and it gets cleared, advace next_it before 
		 * Except when next_node and next_it are in the last position */
		if (*next_node == *next_it && next_node != element.second->neighboursMap.end()){++next_it;}
		
		/* Insert doesnt overwrite existing */
		element.second->neighboursMap.insert(it->second->neighboursMap.begin(), it->second->neighboursMap.end());
                element.second->neighboursMap.insert(next_node->second->neighboursMap.begin(), next_node->second->neighboursMap.end());
            	element.second->neighboursMap.erase(it->first);
		element.second->neighboursMap.erase(next_node->first);
		// Delete entry in edgePointersByNode map
		edgePointersByNode.erase(std::make_pair(it->first, next_node->first));
		edgePointersByNode.erase(std::make_pair(next_node->first, it->first));
		edgePointersByNode.erase(std::make_pair(it->first, element.first));
		edgePointersByNode.erase(std::make_pair(element.first, it->first));
		edgePointersByNode.erase(std::make_pair(next_node->first, element.first));
		edgePointersByNode.erase(std::make_pair(element.first, next_node->first));
            	clusterID -= 2;
		break;
	    }
	    ++next_node;
	}
    }
}

/* update x and y coordinates to calculate mask/ROI of the cluster */
void updateMinMaxXY(int x, int y, Cluster* cluster){
    cluster->minX = (x < cluster->minX) ? x : cluster->minX;
    cluster->maxX = (x > cluster->maxX) ? x : cluster->maxX;
    cluster->minY = (y < cluster->minY) ? y : cluster->minY;
    cluster->maxY = (y > cluster->maxY) ? y : cluster->maxY;
    /* Update pxlCounterXY and aspectRatio  */
    int diffY = cluster->maxY - cluster->minY;
    int diffX = cluster->maxX - cluster->minX;
    cluster->pxlCounterY = (diffY == 0) ? 1 : diffY + 1;
    cluster->pxlCounterX = (diffX == 0) ? 1 : diffX + 1;
    cluster->aspectRatio = static_cast<double>(cluster->pxlCounterX)/cluster->pxlCounterY; // width / height
}

/* Averaging cluster data extracted from sobel */ 
void clusterDataAverage(std::pair<const int, Cluster*> &element){
    double dist = 0, tmpDist;
    cv::Point tmpMassCntr;
    cv::Point2f tmpCast;
    std::vector<cv::Point>::iterator it;
    element.second->angle /= element.second->pixelMembers.size();
    element.second->magnitude /= element.second->pixelMembers.size();
    /*element.second->largestPxlNeighbor = 0;
    for (std::unordered_map<int, int>::iterator it = element.second->neighborBorderPxlCounter.begin(); it != element.second->neighborBorderPxlCounter.end(); it++){
	if (it->second > element.second->largestPxlNeighbor)
	element.second->largestPxlNeighbor = it->first;
    }*/
    // Skeleton use is only for MTs at this point
    if (element.second->skelPxls.size() == 0 || cellRestrictions->cellIdentifier != 2){return;}
    // Move mass center to nearest skel pixel
    tmpCast.x = element.second->skelPxls.front().x;
    tmpCast.y = element.second->skelPxls.front().y;
    dist = euclideanDist(element.second->massCenter, tmpCast);
    for (std::vector<cv::Point>::iterator it = element.second->skelPxls.begin() + 1; it != element.second->skelPxls.end(); ++it){
	tmpCast.x = (*it).x;
	tmpCast.y = (*it).y;
	tmpDist = euclideanDist(element.second->massCenter, tmpCast);
	if (tmpDist < dist){
	    dist = tmpDist;
	    tmpMassCntr = *it;
	}
    }
    if (tmpMassCntr.x > 0 && tmpMassCntr.y > 0){
	element.second->massCenter.y = tmpMassCntr.x;
	element.second->massCenter.x = tmpMassCntr.y;
    }
}

void updateCovMat(Cluster* cluster){
    double tmp_sqrt;
    /* covariance matrix of merged cluster */
    cluster->covMatU11 = (cluster->m11/cluster->pixelMembers.size()) - cluster->massCenter.x * cluster->massCenter.y;
    cluster->covMatU02 = (cluster->m02/cluster->pixelMembers.size()) - pow(cluster->massCenter.y, 2);
    cluster->covMatU20 = (cluster->m20/cluster->pixelMembers.size()) - pow(cluster->massCenter.x, 2);
    /* from the covMatrix get the eccentricity. Could also provide orientation */
    tmp_sqrt = sqrt(4*pow(cluster->covMatU11,2) + pow((cluster->covMatU20 - cluster->covMatU02),2) ) /2;
    cluster->eigenVal1 = ((cluster->covMatU20 + cluster->covMatU02) / 2) + tmp_sqrt;
    cluster->eigenVal2 = ((cluster->covMatU20 + cluster->covMatU02) / 2) - tmp_sqrt;
    cluster->lambdaRatio =
            (cluster->eigenVal1 > cluster->eigenVal2) ? cluster->eigenVal1/cluster->eigenVal2 : cluster->eigenVal2/cluster->eigenVal1;
    if (cluster->covMatU20 != 0 && cluster->covMatU02 != 0){
        cluster->angle = 0.5 * atan(2*cluster->covMatU11/(cluster->covMatU20 - cluster->covMatU02)) * 180 / M_PI;
    } else {
        cluster->angle = 0.0;
    }
}

/* Load nodes from json graph file, merge nodes from this  program clustering with those nodes. 
 * Then populate edges with the ones from the json graph file from networkX */
void mergeSkelClusters(){
    int x_ctr, y_ctr, x, y, neighborCluster = -1, nxStartNodeID, nxEndNodeID, jsonEdgeID, startNodeID, endNodeID, clustId;
    unsigned int k;
    cv::Point2f massCntr;
    cv::Point tmpEdgePnt;
    Cluster *tmp = nullptr;
    std::unordered_map<int,int> clusters2merge;
    std::unordered_map<int,int> nxNodeIDequiv; // Map nxID from json graph file to respective clusterID
    Json::CharReaderBuilder builder;
    Json::Value jsonData;
    std::string errors;
    std::fstream jsonFile(cellRestrictions->nxJsonFile, std::fstream::in); // Open json graph file read only
    if (!jsonFile.is_open()){
	fprintf(stderr,"Failed opening %s (json graph file)\n", cellRestrictions->nxJsonFile.c_str());
	return;
    }

    bool parsingSuccessful = Json::parseFromStream(builder, jsonFile, &jsonData, &errors);  // load json data to obj
    // If the json file cant be parsed, exit
    if (!parsingSuccessful){
	fprintf(stderr,"Failed parsing %s (json graph file): %s\n", cellRestrictions->nxJsonFile.c_str(), errors.c_str());
	jsonFile.close();
	return;
    }
    jsonFile.close();

    const Json::Value& jsonNodes = jsonData["nodes"];
    const Json::Value& jsonEdges = jsonData["links"];
    // Iterate over the nodes
    for (k = 0; k < jsonNodes.size(); k++){
	massCntr = cv::Point2f(jsonNodes[k]["o"][0].asInt64(), jsonNodes[k]["o"][1].asInt64());
	// create new cluster
	clusterCounter++;
	clusterPointers[clusterCounter] = createCluster(clusterCounter);
	clusterPointers[clusterCounter]->massCenter = massCntr;
	clusterPointers[clusterCounter]->nxId = jsonNodes[k]["id"].asInt();
	clusterPointers[clusterCounter]->pixelMembers.push_back(std::make_pair(massCntr.y, massCntr.x));
	nxNodeIDequiv[clusterPointers[clusterCounter]->nxId] = clusterCounter;
	// paint nodes from networkX
	cv::circle(frame, massCntr, 2, cv::Scalar(0,255,255));
	// init node data
	clusterPointers[clusterCounter]->node.id = clusterCounter;
	clusterPointers[clusterCounter]->node.color = clusterPointers[clusterCounter]->color ;
	clusterPointers[clusterCounter]->node.coord = massCntr ;
	clusterPointers[clusterCounter]->node.degree = 0 ;
	clusterPointers[clusterCounter]->node.parent = clusterPointers[clusterCounter];

	/* Top Left: x-1,j-1. Top i-1, j; Top Right i-1, j+1; Left i, j-1;
	* Right: i, j+1; Bottom Left: i+1, j-1; Bottom: i+1, j; Bottom Right: i+1, j+1;
	* -2 to 3 for 5x5*/
	for (x_ctr = cellRestrictions->kernelSizeLowerLimit; x_ctr < cellRestrictions->kernelSize - 1; x_ctr++){
	    x = massCntr.x + x_ctr;
	    for (y_ctr = cellRestrictions->kernelSizeLowerLimit; y_ctr < cellRestrictions->kernelSize - 1; y_ctr++){
		y = massCntr.y + y_ctr;
		// Not checking if its part of foreground or under threshold
		// All neighbors should be assigned or background
		neighborCluster = pxlMetadata.at<int>(y, x);
		// Ignore forbiden pixels
		if (neighborCluster < 0){
		    continue;
		} // if its zero, then it wasnt assigned. Take it for this cluster
		else if (neighborCluster == 0){
		    pxlMetadata.at<int>(y,x) = clusterCounter;
		    clusterPointers[clusterCounter]->pixelMembers.push_back(std::make_pair(y,x));
		    clusterPointers[clusterCounter]->m10 += x;
		    clusterPointers[clusterCounter]->m01 += y;
		    clusterPointers[clusterCounter]->m11 += x*y ;
                    clusterPointers[clusterCounter]->m20 += pow(x,2);
		    clusterPointers[clusterCounter]->m02 += pow(y,2);
		    continue;
		}
		// If pixel belongs to a merged cluster, iterate to find who ones the pixel after all the mergers and add its id to clusters2merge
		if (clusterPointers[neighborCluster]->replaceCluster != nullptr){
		    // tmp is a cluster pointer that will advance until it reaches the "real" owner
		    tmp = clusterPointers[neighborCluster]->replaceCluster;
		    while(tmp->replaceCluster != nullptr){
			tmp = tmp->replaceCluster;
		    }
		    // Add to clusters2merge only if its not a networkX node
		    if (tmp->nxId == -1){
			clusters2merge[tmp->id] = 2;
			// update pxlMetadata.at x,y to avoid unnecesary future lookups
			pxlMetadata.at<int>(y, x) = tmp->id;
		    }
		    tmp = nullptr;
		} else {
		    // add to clusters2merge map for later merger
		    clusters2merge[neighborCluster] = 1;
		}
	    }
	}
	// Populate clusterPointers[clusterCounter]
	for (std::unordered_map<int,int>::iterator it = clusters2merge.begin(); it != clusters2merge.end(); ++it){
	    clusterPointers[it->first] = clusterPointers[it->first];
	    // Update clusterPointers[clusterCounter]/clusterPointers[clusterCounter] pixelMembers
	    clusterPointers[clusterCounter]->pixelMembers.insert(clusterPointers[clusterCounter]->pixelMembers.end(), clusterPointers[it->first]->pixelMembers.begin(), clusterPointers[it->first]->pixelMembers.end());
	    clusterPointers[it->first]->pixelMembers.clear(); // free this memory
	    // Replace it->first cluster's id from its neighbors with clusterPointers[clusterCounter]->id
	    for (std::unordered_map<int, Cluster*>::iterator neighIt = clusterPointers[it->first]->neighboursMap.begin(); neighIt != clusterPointers[it->first]->neighboursMap.end(); neighIt++){
		// Skip the cluster that is clusterPointers[clusterCounter]. Avoid circular loops 
		if (neighIt->second->id == clusterPointers[clusterCounter]->id){continue;}
		// Replace only once
		if (neighIt->second->neighboursMap.count(clusterPointers[clusterCounter]->id) == 0){
		    neighIt->second->neighboursMap[clusterPointers[clusterCounter]->id] = clusterPointers[clusterCounter];
		}
		if (neighIt->second->neighboursMap.count(clusterPointers[it->first]->id) > 0){
		    neighIt->second->neighboursMap.erase(clusterPointers[it->first]->id);
		}
                // Delete entry in edgePointersByNode map
                edgePointersByNode.erase(std::make_pair(clusterPointers[it->first]->id, neighIt->first));
                edgePointersByNode.erase(std::make_pair(neighIt->first, clusterPointers[it->first]->id));
		// Add replacement
		edgePointersByNode[std::make_pair(clusterPointers[clusterCounter]->id, neighIt->first)] = nullptr;
		edgePointersByNode[std::make_pair(neighIt->first, clusterPointers[clusterCounter]->id)] = nullptr;
            }
	    // Update cluster Data 
	    clusterPointers[clusterCounter]->neighboursMap.insert(clusterPointers[it->first]->neighboursMap.begin(), clusterPointers[it->first]->neighboursMap.end());
	    clusterPointers[clusterCounter]->m10 += clusterPointers[it->first]->m10;
	    clusterPointers[clusterCounter]->m01 += clusterPointers[it->first]->m01;
	    clusterPointers[clusterCounter]->m11 += clusterPointers[it->first]->m11;
	    clusterPointers[clusterCounter]->m20 += clusterPointers[it->first]->m20;
	    clusterPointers[clusterCounter]->m02 += clusterPointers[it->first]->m02;
	    // mark cluster as merged
	    clusterPointers[it->first]->clusterType = -2;
	    clusterPointers[it->first]->replaceCluster = clusterPointers[clusterCounter];
	    // Annotate as absorbed Cluster
	    clusterPointers[clusterCounter]->absorbedClusters[it->first] = 1;
	}
	// reset map
	clusters2merge.clear();
	// Define clusterType
	if (clusterPointers[clusterCounter]->neighboursMap.size() >= 3){
	    clusterPointers[clusterCounter]->clusterType = 3;
	} else if (clusterPointers[clusterCounter]->neighboursMap.size() >= 0){
	    clusterPointers[clusterCounter]->clusterType = (int)clusterPointers[clusterCounter]->neighboursMap.size();
	}
	// if node has "centerNode" key, override nType to 5 if its a neuron
	if (jsonNodes[k].isMember("centernode") && cellRestrictions->cellIdentifier == 1){
	    clusterPointers[clusterCounter]->node.nType = 5;
	}
	updateSkelNodeType(clusterPointers[clusterCounter]);
    }

    // Iterate over the edges/links.
    for (k = 0; k < jsonEdges.size(); k++){
	nxStartNodeID = jsonEdges[k]["source"].asInt();
	nxEndNodeID = jsonEdges[k]["target"].asInt();
	jsonEdgeID = jsonEdges[k]["eid"].asInt();
	startNodeID = nxNodeIDequiv[nxStartNodeID];
	endNodeID = nxNodeIDequiv[nxEndNodeID];
	// update edgeID with the highest jsonEdgeID found
	if (edgeID < jsonEdgeID){edgeID = jsonEdgeID;}
	// for each edge, increase this node and neighbors node degree
	// NOTE, if this is done after, then the copied data is not contained in edgePointers[jsonEdgeID]->endX
        clusterPointers[startNodeID]->node.degree += 1;
	if (clusterPointers[startNodeID]->node.degree != (int)clusterPointers[startNodeID]->neighboursMap.size()){
	    updateSkelNodeType(clusterPointers[startNodeID]);
	}
        clusterPointers[endNodeID]->node.degree += 1;
	if (clusterPointers[endNodeID]->node.degree != (int)clusterPointers[endNodeID]->neighboursMap.size()){
            updateSkelNodeType(clusterPointers[endNodeID]);
        }
	// Initialize new edge based on eid from json graph file
	edgePointers[jsonEdgeID] = createEdge(jsonEdgeID);
	// Add nodes to new edge // Cluster *sClus = clusterPointers[startNodeID], *eClus = clusterPointers[endNodeID]; 
	edgePointers[jsonEdgeID]->end1 = &clusterPointers[startNodeID]->node;
	edgePointers[jsonEdgeID]->end2 = &clusterPointers[endNodeID]->node;
	edgePointers[jsonEdgeID]->length = jsonEdges[k]["weight"].asInt64();
	if (edgePointers[jsonEdgeID]->length < 1){ // weight can be zero. In this case use L2 formula
	    edgePointers[jsonEdgeID]->length = euclideanDist(clusterPointers[startNodeID]->massCenter, clusterPointers[endNodeID]->massCenter);
	}
	edgePointers[jsonEdgeID]->angle = angle2Points(clusterPointers[startNodeID]->node.coord, clusterPointers[endNodeID]->node.coord);
	// Add edge pixels from json graph file. Also match the pixels with the cluster/nodes calculated by our method
	const Json::Value& jsonEdgePnts = jsonEdges[k]["pts"];
	for (int i = 0; i < (int)jsonEdgePnts.size(); i++){
	    tmpEdgePnt = cv::Point(jsonEdgePnts[i][0].asInt(), jsonEdgePnts[i][1].asInt());
	    // Add to edge vector of pxls
	    edgePointers[jsonEdgeID]->edgePxls.push_back(tmpEdgePnt);
	    // if edge goes thru black pixels (fg), capture clusterID
            if (frameGray.at<uchar>(tmpEdgePnt) == 0){
		clustId = pxlMetadata.at<int>(tmpEdgePnt);
		// Add only if clustId not the same id as start or end node. Also check for -1 and 0 marked pixels (clusterID/Counter start from 1)
                if (clustId > 0 && clustId != edgePointers[jsonEdgeID]->end1->id && clustId != edgePointers[jsonEdgeID]->end2->id){
                    edgePointers[jsonEdgeID]->interNodes[clustId] = clusterPointers[clustId]->node.coord;
                }
	    }
	}

	// notify advance
	if (debug == 0 && edgeID % 100 == 0){printf(" %d ", jsonEdgeID);}
	// Add to edgePointersByNode
	edgePointersByNode[std::make_pair(edgePointers[jsonEdgeID]->end1->id, edgePointers[jsonEdgeID]->end2->id)] = edgePointers[jsonEdgeID];
	edgePointersByNode[std::make_pair(edgePointers[jsonEdgeID]->end2->id, edgePointers[jsonEdgeID]->end1->id)] = edgePointers[jsonEdgeID];
	// Add this edge to each nx node/cluster's edgesMap
	clusterPointers[startNodeID]->edgesMap[endNodeID] = edgePointers[jsonEdgeID];
	clusterPointers[endNodeID]->edgesMap[startNodeID] = edgePointers[jsonEdgeID];
    }
    for (std::unordered_map<int, Arista*>::iterator it = edgePointers.begin(); it != edgePointers.end(); ++it){
	// Find those that have at least one node with degree 1
	if (it->second->end1->degree == 1 || it->second->end2->degree == 1){
	    it->second->endEdge = true;
	    cellRestrictions->endEdges[it->first] = 0;
	}
	// Tag 1st degree neighbors of center nodes as type 6. Specially useful for neurons/soma
	/*if (it->second->end1->nType == 5 ) {
	    it->second->end2->nType = 6;
	} else if (it->second->end2->nType == 5){
	    it->second->end1->nType = 6;
	}*/
	if (debug){ // Add networkX edge data to metadataFile
	    fprintf(metadataFile,"%d\t %d (%.4f)[%d°]{%d}  %d (%.4f)[%d°]{%d} %.4f %.4f %.4f %.4f\t %.4f %.4f \t%.4f\n", it->first, it->second->end1->id, 0.0, it->second->end1->degree, it->second->end1->nType, it->second->end2->id, 0.0, it->second->end2->degree, it->second->end2->nType, it->second->end1->coord.x, it->second->end1->coord.y, it->second->end2->coord.x, it->second->end2->coord.y, it->second->length, it->second->width, it->second->angle);
	}
    }
}

void updateSkelNodeType(Cluster* cluster){
    // Do nothing to center nodes
    if (cluster->node.nType == 5 || cluster->node.nType == 6){return;}
    // Update nType depending on latest degree
    if (cluster->node.degree == 1){
	cluster->node.nType = 1;
    } else if (cluster->node.degree == 2) {
	cluster->node.nType = 2;
    } else if (cluster->node.degree == 3) {
        cluster->node.nType = 3;
    }
}
