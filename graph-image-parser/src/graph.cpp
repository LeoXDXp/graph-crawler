#include "cluster.hpp"
#include "nodes.hpp"
#include "graph.hpp"
#include "ants.hpp"
#include "githash.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp> // cv::circle, LineSegmentDetector
#include <opencv2/ximgproc.hpp> // anisotropicDiffusion, thinning.cpp
#include <gtk/gtk.h>
#include <unordered_map>
#include <ctime>
#include <cmath> // M_PI
#include <future>
#include <chrono>
#include <cstdio>
#include <fstream> // std::fstream open close
#include <string> // to_string
#include <getopt.h>
#include <sys/resource.h>
#include <signal.h> // handle keyboard interrupt and free memory
#include "zhang-suen-thinning/zhangsuen.h"
// C++11 old: g++ -ggdb graph.cpp -o graph `pkg-config --cflags --libs opencv` -Wall -pedantic . Add -pg for gprof profiling. -frounding-math might help in case of rounding errors 

/* extern global vars*/
int clusterID, clusterCounter, edgeID, maxEdgesSupported, kernelSize, debug;
cv::Mat pxlMetadata, frame, frameGray, ogFrame, cluster, nodesMat, thinning, gradient, graph, frameGraph, antsMat, debugMat, debugPhils, joinedAnts;
FILE *metadataFile, *debugL2File;
struct Restrictions *cellRestrictions;
std::unordered_map<int, Arista*> edgePointers;
std::unordered_map<std::pair<int,int>, Arista*, hash_pair> edgePointersByNode;
//std::unordered_map<int, PhilSegment*> philPointers;
cv::Scalar imgStdDev, imgMean;
double neighborThreshold;

/* this file's global vars */
cv::Mat legend, nNeighMat;
gboolean display{true};
template class std::unordered_map<int, Cluster*>; // needed to access clusterPointers in gdb
template class std::unordered_map<int, Arista*>;
template class std::unordered_map<int, Ant*>;
std::unordered_map<int, Cluster*> clusterPointers; // pointers to clusters

int main(int argc, char** argv){
    int i,j;
    cv::Mat borderFrame, clusterPrePruned, frameBW;
    clock_t begin = clock(), process_start;
    double elapsed_secs;
    /* Interrupt handler to exit gracefully */
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = interruptHandler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    //std::unordered_map<std::pair<int,int>, cv::Point, hash_pair> skelNeighConn; // Key are IDs of cluster's sharing border

    debug = 0;
    /* 1ST PART: Input, Sobel angles, Clustering and Info Gathering */
    int retcode = ProcessArgs(argc, argv);
    if (retcode != 0){
	return retcode;
    }
    /* set rlimit to avoid memory depletion while testing */
    struct rlimit lim = {4096, 4096};
    if (setrlimit(RLIMIT_STACK, &lim) == -1) return 1;
    srand(cellRestrictions->rseed);

    /* Thresholding for thinning mat as we need a BW image */
    neighborThreshold = cv::threshold(frameGray, frameBW, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
    printf("neighborThreshold %f\n", neighborThreshold);
    if (neighborThreshold == 0) {neighborThreshold = (imgMean[0] >= 255 - imgStdDev[0]) ? imgMean[0] : 255 - imgStdDev[0] ;} // manual option
    /* to color for clustering coloring images */
    cvtColor(frameGray, frame, cv::COLOR_GRAY2BGR);
    debugMat = frame.clone();
    /* avoid corner cases */
    copyMakeBorder(frameGray, borderFrame, 1, 1, 1, 1, cv::BORDER_CONSTANT, 255);
    /* pxlMetadata store Mat, initialized with 0 */
    pxlMetadata = cv::Mat::zeros(borderFrame.size(), CV_32S); /* dim represents the cluster it belongs*/

    /**/
    thinning = frameBW.clone();
    thin(thinning,false,false,false);

    // Calculate stuff, ignore extra borders (1, rows/cols-2)
    clusterID = 0;
    edgeID = 0;
    /* Selecting Y axis advance decreases the number of clusters to 3742, from 10451 for RFP_1_1*/
    /* X axis first */ // start from 2 and end in i/j < borderFrame.rows/cols - 3 for 5x5 
    for (i = abs(cellRestrictions->kernelSizeLowerLimit); i < borderFrame.rows - 1 + cellRestrictions->kernelSizeLowerLimit; i++) {
    	for (j = abs(cellRestrictions->kernelSizeLowerLimit); j < borderFrame.cols - 1 + cellRestrictions->kernelSizeLowerLimit; j++){
    /* Y axis first */
    //for (j = 1; j < borderFrame.cols - 2; j++){
        //for (i = 1; i < borderFrame.rows - 2; i++) {
	    /* Evaluate only pixels under the neighborThreshold, closer to black(0) */
	    if (borderFrame.at<uchar>(i, j) > neighborThreshold) {
		continue;
	    } else {
		/* if this pixel is marked as border in sobel (grey(127) or white(255)), and is not background in borderFrame, set it as background */
		if (gradient.at<uchar>(i-1,j-1) > 128 && thinning.at<uchar>(i-1,j-1) != 0) {
		    pxlMetadata.at<int>(i, j) = -1; //not to be asigned by any cluster
		    continue;
		}
		/* if black pixel with no previous assignment of clusterID, assign and add*/
		if (pxlMetadata.at<int>(i, j) == 0){
		    clusterID++;
		    pxlMetadata.at<int>(i, j) = clusterID;
		    /* create new cluster. Checking if it previously existed should be necessary */
		    clusterPointers[clusterID] = createCluster(clusterID);
		    clusterPointers[clusterID]->m10 = i + cellRestrictions->kernelSizeLowerLimit; // i - 1 for KernelSize 3 or i - 2 for KernelSize 5
		    clusterPointers[clusterID]->m01 = j + cellRestrictions->kernelSizeLowerLimit;
		    clusterPointers[clusterID]->m11 = (i + cellRestrictions->kernelSizeLowerLimit)*(j + cellRestrictions->kernelSizeLowerLimit);
		    clusterPointers[clusterID]->m20 = pow(i + cellRestrictions->kernelSizeLowerLimit,2);
		    clusterPointers[clusterID]->m02 = pow(j + cellRestrictions->kernelSizeLowerLimit,2);
		    clusterPointers[clusterID]->aspectRatio = 1;
		    clusterPointers[clusterID]->maxX = clusterPointers[clusterID]->minX = i;
		    clusterPointers[clusterID]->maxY = clusterPointers[clusterID]->minY = j;
		    clusterPointers[clusterID]->massCenter = cv::Point2f(clusterPointers[clusterID]->m01, clusterPointers[clusterID]->m10);
		    /* debug: enable to check if clusterPointers[clusterID]->neighboursMap.size() is increasing*/
                    if (debug == 2){fprintf(debugL2File, "x %d y %d cID %d nNeigh %ld pxlCounter %ld\n", j-1, i-1, clusterPointers[clusterID]->id, clusterPointers[clusterID]->neighboursMap.size(), clusterPointers[clusterID]->pixelMembers.size());}
		    checkNeighbors(i, j, neighborThreshold, borderFrame);
		} else { /* In Existing cluster (pxlMetadata.at<int>(i, j) > 0), set flag to not add an assigned pixel again */
		    /* debug: enable to check if clusterPointers[clusterID]->neighboursMap.size() is increasing*/
		    if (debug == 2){fprintf(debugL2File, "x %d y %d cID %d nNeigh %ld pxlCounter %ld\n", j-1, i-1, clusterPointers[clusterID]->id, clusterPointers[clusterID]->neighboursMap.size(), clusterPointers[clusterID]->pixelMembers.size());}
		    checkNeighbors(i, j, neighborThreshold, borderFrame, true);
		}
	    }
        }
    }
    /* average all cluster coord */
    std::for_each(clusterPointers.begin(), clusterPointers.end(), clusterDataAverage);
    printf("Clusters/Nodes: %d\n", clusterID);
    /* 2ND PART: Cleaning and Painting */
    //legend = cv::Mat(frame.rows, frame.cols, frame.type(), cv::Scalar(255,255,255));
    nodesMat = frame.clone();//Mat(frame.rows, frame.cols, frame.type(), cv::Scalar(255,255,255)); // frame.clone()
    ogFrame = frame.clone();
    cluster = frame.clone();
    //nNeighMat = frame.clone();
    frameGraph = frame.clone();
    graph = cv::Mat(frame.rows, frame.cols, frame.type(), cv::Scalar(255,255,255));
    joinedAnts = cv::Mat(frame.rows, frame.cols, frame.type(), cv::Scalar(255,255,255));

    /* If commenting out this line, remember to call updateClusterType manually */
    printf("paintClusters/updateClusterType:");
    process_start = clock();
    std::for_each(clusterPointers.begin(), clusterPointers.end(), paintClusters);
    printf("%f\n", double(clock() - process_start) / CLOCKS_PER_SEC);
    clusterPrePruned = cluster.clone();

    // Cluster counter stores the max count of clusterID before mergers
    clusterCounter = clusterID;

    /* merge nodes/clusters that dont comply with min/max distance constrains */
    //for (int counter = 0; counter < 2; counter++){
    printf("mergeClusters:");
    process_start = clock();
    std::for_each(clusterPointers.begin(), clusterPointers.end(), mergeClusters);
    printf("%f\n", double(clock() - process_start) / CLOCKS_PER_SEC);
    //}

    // If a jsonFile is provided, call mergeSkelClusters
    if (cellRestrictions->nxJsonFile != ""){
	printf("mergeSkelClusters:");
	process_start = clock();
	mergeSkelClusters();
	printf("%f\n", double(clock() - process_start) / CLOCKS_PER_SEC);
    }

    /* create edges */
    antsMat = ogFrame.clone();
    debugPhils = ogFrame.clone();
    maxEdgesSupported = clusterID*(clusterID-1)/2;
    printf("%d nodes/clusters, worst case: %d(%d-1)/2 = %d\n", clusterID, clusterID, clusterID, maxEdgesSupported);
    if (cellRestrictions->nxJsonFile == ""){
	/* Create a node in each cluster mass Center */
	printf("initNodeinCluster:");
	process_start = clock();
	std::for_each(clusterPointers.begin(), clusterPointers.end(), initNodeinCluster);
	printf("%f\n", double(clock() - process_start) / CLOCKS_PER_SEC);
	printf("%d nodes/clusters, worst case: %d(%d-1)/2 = %d\n", clusterID, clusterID, clusterID, maxEdgesSupported);
	process_start = clock();
	printf("populateEdges: ");
	std::for_each(clusterPointers.begin(), clusterPointers.end(), populateEdges); // In populateEdges the graph construction ENDS. PATH/TOURs are in ants
	printf("time %f\n", double(clock() - process_start) / CLOCKS_PER_SEC);
    }
    printf("Node average degree %f\n", clusterID/(double)edgeID);

    if (debug == 4){printf("edges %d edgePointers %ld edgePointersByNode %ld\n", edgeID, edgePointers.size(), edgePointersByNode.size());}

    /* Get angular data of intermediate Quality nodes and the edges they belong */
    printf("nodeNeighborAngle:");
    process_start = clock();
    std::for_each(clusterPointers.begin(), clusterPointers.end(), nodeNeighborAngle);
    printf("%f\n", double(clock() - process_start) / CLOCKS_PER_SEC);

    if (debug){ // debug >= 1
    	printf("Clusters/Nodes: %d, Edges %d. paintEdges:", clusterID, edgeID);
    	process_start = clock();
    	std::for_each(edgePointers.begin(), edgePointers.end(), paintEdges);
	//cv::resize(graph, graph, cv::Size(), 2, 2);
    	printf("%f\n", double(clock() - process_start) / CLOCKS_PER_SEC);

	printf("ClusterNodeInfo:");
	process_start = clock();
	fprintf(metadataFile,"cId(nxId) | numNei | pxlCtr (x,y) | massCenter | 1st pxl (color) |  mag | nType(cType) | lambdaRatio | eigen<1,2>| aspectRatio | orientation | absorbedClusters \n");
	std::for_each(clusterPointers.begin(), clusterPointers.end(), printClusterNodeInfo);
	printf("%f\n", double(clock() - process_start) / CLOCKS_PER_SEC);
	//std::for_each(clusterPointers.begin(), clusterPointers.end(), paintLegend);   //showImg("Legend", legend);

	cv::imwrite("/tmp/" + cellRestrictions->filename + "-ClusterPruned.jpg", cluster);
	cv::imwrite("/tmp/" + cellRestrictions->filename + "-NodesPruned.jpg", frame);
	cv::imwrite("/tmp/" + cellRestrictions->filename + "-Gradient.jpg", gradient);

	showImg("SkelZH84",thinning);
	showImg("Gradient", gradient);
    	showImg("Cluster pre pruned", clusterPrePruned);
	showImg("Cluster pruned", cluster);
    	showImg("Nodes pre pruned", nodesMat);
    	showImg("Nodes pruned", frame);
    	showImg("Graph", graph);
    	showImg("frame Graph", frameGraph);
    }
    //showImg("Original Image", ogFrame);
    printf("ants:\n");
    process_start = clock();
    antColonyInit();
    printf("Ants time %f\n", double(clock() - process_start) / CLOCKS_PER_SEC);
    elapsed_secs = double(clock() - begin) / CLOCKS_PER_SEC;
    printf("total time: %f\n", elapsed_secs);
    fprintf(metadataFile, "total time: %f\n", elapsed_secs);
    showImg("Candidate ants", joinedAnts);

    // Print Statistics
    statistics();

    if (display) cv::waitKey(0); // only wait when there is a X server to show stuff
    /* free PhilSegments, edges and clusters */
    //std::for_each(philPointers.begin(), philPointers.end(), freePhilSegments);
    std::for_each(edgePointers.begin(), edgePointers.end(), freeEdges);
    std::for_each(clusterPointers.begin(), clusterPointers.end(), freeClusters);
    delete cellRestrictions;
    /* close metadataFile and debugL2File */
    if (metadataFile != nullptr){fclose(metadataFile);}
    if (debugL2File != nullptr){fclose(debugL2File);}
    endColony();
    return 0;
}

void paintLegend(std::pair<const int, Cluster*> &element){
    cv::Vec3b clr;
    bool clr_gray = false;
    if (element.second->neighboursMap.size() > 0) {
        clr[0] = element.second->color[0]; clr[1] = element.second->color[1]; clr[2] = element.second->color[2];
    } else {
        clr[0] = clr[1] = clr[2] = 255;
        clr_gray = true;
    }
    if (!clr_gray){
        putText(legend, std::to_string(element.first),cv::Point(element.second->massCenter.x, element.second->massCenter.y), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.6, cv::Scalar(clr[0],clr[1],clr[2]), 1);
    }
}

/* Aux function to avoid code repetition. Show or save */
void showImg(std::string title, cv::Mat& img, int h, int w){
    if (display) {
    	cv::namedWindow(title, cv::WINDOW_NORMAL);
    	cv::resizeWindow(title, h, w);
    	cv::imshow(title, img);
    } else { // Save if no X is found
	cv::imwrite(title + ".jpg", img);
    }
}
/* Returns angles*gradient. Can replace with Scharr by switching commented lines 
 * Use 32F to avoid aproximation errors */
void sobelAngles(){
    cv::Mat grad_x, grad_y, abs_grad_x, abs_grad_y;
    //Scharr( frameGray, grad_x, CV_16S, 1, 0);
    Sobel( frameGray, grad_x, CV_32FC1, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT );
    convertScaleAbs( grad_x, abs_grad_x );
    /* Y axis */
    //Scharr( frameGray, grad_y, CV_16S, 0, 1 );
    Sobel( frameGray, grad_y, CV_32FC1, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT );
    convertScaleAbs( grad_y, abs_grad_y );
    /* Total Gradient (approximate) */
    addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, gradient );
    /* angle for each pixel. Dont use abs values here. Use angles */
    //phase(grad_x, grad_y, pixAngles, true);
    /* gradient from CV_32F to CV_8U */
    gradient.convertTo(gradient, CV_8U);
    //pixAngles.convertTo(pixAngles, CV_8U);
    //printf("%d %d\n", pixAngles.type(), gradient.type());
}

/* print cluster and node data */
void printClusterNodeInfo(std::pair<const int, Cluster*> &element){
    try {
	std::pair<int,int> p = element.second->pixelMembers.front();
	fprintf(metadataFile,"%d(%d),  %ld,\t %ld (%d, %d), %.1f:%.1f, %d:%d - %d:%d:%d,   %.2f,  %d(%d),  %.3f,\t   <%.3f, %.3f>,    %.3f,\t  %.3f\t", element.first, element.second->nxId, element.second->neighboursMap.size(), element.second->pixelMembers.size(), element.second->pxlCounterX, element.second->pxlCounterY, element.second->massCenter.x, element.second->massCenter.y, p.first, p.second, element.second->color[2], element.second->color[1], element.second->color[0], element.second->magnitude, element.second->node.nType, element.second->clusterType, element.second->lambdaRatio, element.second->eigenVal1, element.second->eigenVal2, element.second->aspectRatio, element.second->angle);
	for (std::unordered_map<int,int>::iterator absIt = element.second->absorbedClusters.begin(); absIt != element.second->absorbedClusters.end(); ++absIt){
                fprintf(metadataFile,"%d ", absIt->first);
	}
	fprintf(metadataFile,"\n");
    } catch (...) {
	fprintf(stderr, " ");
    }
}
/*void printClusterInfo(Cluster* element){
    try{
	fprintf(metadataFile,"-%d, %ld, %ld,%f:%f,%d:%d:%d, %f, %f, %f, %d, %d\n", element->id, element->neighboursMap.size(), element->pixelMembers.size(), element->massCenter.x, element->massCenter.y, element->color[2], element->color[1], element->color[0], element->angle, element->angle * 180 / M_PI, element->magnitude, element->pxlCounterX, element->pxlCounterY);
    } catch (...){
	fprintf(stderr, " ");
    }
}*/

/* Get moments directly from mask */
/*void secuencialMoments(Cluster* cluster, cv::Mat& img){
	double tmp_sqrt;
	//int lineLength = 3;
	cv::Point end;
	cv::Scalar tmp;
	cluster->momts = cv::moments(img, true); // true for binary image
	// store de mass center
	cluster->massCenter = cv::Point2f( static_cast<float>(cluster->momts.m10 / (cluster->momts.m00 + 1e-5)),
                                    static_cast<float>(cluster->momts.m01 / (cluster->momts.m00 + 1e-5))) ; //add 1e-5 to avoid division by zero
	tmp_sqrt = sqrt(4*pow(cluster->momts.m11,2) + pow((cluster->momts.m20 - cluster->momts.m02),2) );
	//end.x = cluster->massCenter.x + lineLength * cos(cluster->angle);
	///end.y = cluster->massCenter.y + lineLength * sin(cluster->angle);
}*/

void print_usage(void) {
    printf("Usage: ./phil <params> \n"
	"\t --filename, -f: path to image. Required \n"
        "\t --cellType, -c: neuron (n) <default>, plant MT (p), animal MT (a), reticulum (r) or synthetic (s)\n"
        "\t --bg_color, -b: white (w) <default> or black (b); Image background color.\n"
        "\t --maxThickness, -t: Max known cell thickness, in pixels, in this image. Required\n"
        "\t --saveImage, -s (flag): Save output images to files instead of to stdout. Optional\n"
	"\t --debug, -d (1,2,3): All debug levels save cluster/superPixels data to file. Creates filename+fileExtension(.log) data file in /tmp/ Level 1 Shows images of node/cluster Prunning, gradient and superPixels/clusters. Level 2 shows how the superPixels/clusters are created. Level 3 shows how edges and ants are created.\n"
	"\t --overlaps, -o (flag): Disables overlap of filaments. Optional\n"
	"\t --nodesFromSkel, -z: JSON file with networkX graph data. Required\n"
	"\t --angleThreshold, -a: Override theta angle. Optional\n"
	"\t --maxAxialDispl, -m: Override Max Axial Displacement factor. Optional\n"
	"\t --initialAssignHeuristic, -i: Disable steps 2 & 3 of this heuristic. Optional Flag\n"
	"\t --randomseed, -r: set seed for srand. Optional, defaults to 0\n"
        "\t --help, -h: help\n " // Add example usage
	"\t Example: ./phil -d4 -o -cs -r0 -t11 -f 50_ROIs_Spinning_Marchantia.png -z SpinningMarchantia_Skel.json\n");
}

/* Get variables from user, and perform basic integrity checks */
int ProcessArgs(int argc, char** argv){
    int retcode = 0, opt, option_index;
    std::string delimiter, filename, nxJsonFile, bg_color, fileExtension = ".log";
    float angle = -1.0, maxAxial = -1.0;
    size_t pos = 0;
    std::fstream jsonFile;
    cellRestrictions = new Restrictions();
    cellRestrictions->maxThickness = -1;
    cellRestrictions->kernelSize = 3;
    cellRestrictions->kernelSizeLowerLimit = -1;
    cellRestrictions->withCycles = false;
    cellRestrictions->allowsOverlap = true;
    cellRestrictions->rseed = 1;
    cellRestrictions->initAsignHeuristic = true;
    const char* const short_opts = "c:b:sd:t:f:koz:r:a:m:i";
    static struct option long_opts[] = {
            {"filename", required_argument, nullptr, 'f'},
            {"cellType", required_argument, nullptr, 'c'},
            {"bg_color", required_argument, nullptr, 'b'},
            {"maxThickness", required_argument, nullptr, 't'},
	    {"saveImage", no_argument, nullptr, 's'},
	    {"debug",required_argument, nullptr, 'd'},
	    {"kernelSize",no_argument, nullptr, 'k'},
	    {"overlaps",no_argument, nullptr, 'o'},
	    {"nodesFromSkel", required_argument, nullptr, 'z'},
	    {"randomseed",optional_argument, nullptr, 'r'},
	    {"angleThreshold",optional_argument, nullptr, 'a'},
	    {"maxAxialDispl",optional_argument, nullptr, 'm'},
	    {"initialAssignHeuristic",no_argument, nullptr, 'i'},
	    {NULL,      0,                 NULL,    0}
    };
    printf("phil %s branch %s, compiled on %s\n", GIT_HASH, GIT_BRANCH, COMPILE_TIME); 
    while (3.14){
        opt = getopt_long(argc, argv, short_opts, long_opts, &option_index);
	if (opt == -1){break;}

        switch (opt) {
	    case 0:
            case 'f':
            	filename = std::string(optarg);
	    	frameGray = cv::imread(filename.c_str(), cv::IMREAD_GRAYSCALE);
	        if(frameGray.empty()){                      // Check for invalid input
		    printf("Could not open or find the image\n");
		    retcode = -1;
	    	}
                break;
	    case 1:
       	    case 'c':
            	cellRestrictions->cellType = std::string(optarg);
            	break;
            case 2:
	    case 'b':
            	bg_color = std::string(optarg);
		if (bg_color != "w" && bg_color != "b"){
		    printf("Please pass a the value w or b after -b option\n");
                    retcode = -1;
		}
            	break;
            case 3:
	    case 't':
            	cellRestrictions->maxThickness = std::stoi(optarg);
            	break;
	    case 4:
	    case 's':
	    	display = false;
	    	break;
	    case 5:
	    case 'd': // Intermediate images and Log file for cluster/node data
		try{
		    debug = std::stoi(optarg);
		} catch (...){
		    retcode = -1;
		}
                break;
	    case 6:
            case 'k':
                cellRestrictions->kernelSize = 5;
		cellRestrictions->kernelSizeLowerLimit = -2;
                break;
	    case 7:
	    case 'o':
		cellRestrictions->allowsOverlap = false;
		break;
	    case 8:
            case 'z':
		nxJsonFile = std::string(optarg);
                jsonFile.open(nxJsonFile, std::fstream::in); // Read only
                if(jsonFile.fail()){                      // Check for invalid input
                    printf("Could not open networkX Json graph file\n");
                    retcode = -1;
                } else {
		    jsonFile.close();
		}
                break;
	    case 9:
	    case 'r':
		cellRestrictions->rseed = std::stoi(optarg);
		break;
	    case 'a':
		angle = std::stod(optarg);
		break;
	    case 'm':
		maxAxial = std::stod(optarg);
		break;
	    case 'i':
		cellRestrictions->initAsignHeuristic = false;
		break;
        }
    }
    if (retcode < 0 || argc < 2 || cellRestrictions->maxThickness == -1 || cellRestrictions->kernelSize % 2 != 1 ){
	print_usage();
	delete cellRestrictions;
	return -1;
    }

    if (cellRestrictions->cellType.empty() || cellRestrictions->cellType == "neuron" || cellRestrictions->cellType == "n"){
	cellRestrictions->cellType = "neuron";
	cellRestrictions->cellIdentifier = 1;
	cellRestrictions->connectivityThresh = (int)(cellRestrictions->maxThickness * 0.5); // Using 17% (16.6 rounded) of cellRestrictions->maxThickness
	cellRestrictions->angle = 45;
	cellRestrictions->maxAxialDispl = 2.0;
	cellRestrictions->initAsignHeuristic = false;
    } else if (cellRestrictions->cellType == "plant" || cellRestrictions->cellType == "p") {
        cellRestrictions->cellIdentifier = 2;
	cellRestrictions->connectivityThresh = (int)(cellRestrictions->maxThickness * 0.4);
	cellRestrictions->angle = 30;
	cellRestrictions->maxAxialDispl = 1.5;
    } else if (cellRestrictions->cellType == "animal" || cellRestrictions->cellType == "a"){
        cellRestrictions->cellIdentifier = 3;
	cellRestrictions->connectivityThresh = 6;
	cellRestrictions->angle = 60;
	cellRestrictions->maxAxialDispl = 2.5;
    } else if (cellRestrictions->cellType == "reticulo" || cellRestrictions->cellType == "r"){
        cellRestrictions->cellIdentifier = 4;
        cellRestrictions->connectivityThresh = (int)(cellRestrictions->maxThickness * 0.4);
        cellRestrictions->angle = 60;
	cellRestrictions->nodeMaxDegree = 3;
	cellRestrictions->withCycles = true;
	cellRestrictions->maxAxialDispl = 2.5;
    } else {
	cellRestrictions->cellType = "sintentico";
        cellRestrictions->cellIdentifier = 0;
	cellRestrictions->connectivityThresh = (int)(cellRestrictions->maxThickness * 0.4);
	cellRestrictions->angle = 45;
	cellRestrictions->maxAxialDispl = 1.5;
    }
    // Apply user selected params
    if (angle > -1){cellRestrictions->angle = angle;}
    if (maxAxial > -1){cellRestrictions->maxAxialDispl = maxAxial;}
    if (bg_color == "black" || bg_color == "b"){
	cellRestrictions->bg_color = "black";
	printf("inverting bg_color from black to white through bitwise_not\n");
	cv::bitwise_not(frameGray, frameGray);
	sobelAngles();
    } else {
	sobelAngles(); /* Sobel works with white background */
	cellRestrictions->bg_color = "white";
    }
    /* Get stdDev and mean of image */
    cv::meanStdDev(frameGray, imgMean, imgStdDev);
    if (display != false){
	display = gtk_init_check(NULL,NULL);
    }

    delimiter = "/";
    while ((pos = filename.find(delimiter)) != std::string::npos) { // update for windows compatibility later
        filename.erase(0, pos + delimiter.length());
    }

    if (debug == 2){debugL2File = fopen(( "/tmp/debugL2_" + filename + fileExtension).c_str(), "w");}
    if (debug){
	metadataFile = fopen(( "/tmp/" + filename + fileExtension).c_str(), "w");
	fprintf(metadataFile,"eID  |  cID (°)| \t neighID (°) |\tend1MC |\tend2MC |\t\t length |   width |\tphilSegmentCounter | edgeAngle(°) \n");
    }

    cellRestrictions->filename = filename;
    cellRestrictions->nxJsonFile = nxJsonFile;
    printf("cellType: %s, maxThick %d, connectivityThresh %d, bg_Color %s debug: %d, filename: %s, kernelSize: %d, mean %f stdDev %f, allowsOverlap %s, withCycles %s skelJsonFile %s randSeed %d angle %f maxAxialDispl %f initialAsignHeuristicComplete %s\n", cellRestrictions->cellType.c_str(), cellRestrictions->maxThickness, cellRestrictions->connectivityThresh, cellRestrictions->bg_color.c_str(), debug, filename.c_str(), cellRestrictions->kernelSize, imgMean[0], imgStdDev[0], cellRestrictions->allowsOverlap ? "true" : "false", cellRestrictions->withCycles ? "true" : "false", cellRestrictions->nxJsonFile.c_str(), cellRestrictions->rseed, cellRestrictions->angle, cellRestrictions->maxAxialDispl, cellRestrictions->initAsignHeuristic ? "true" : "false");

    if (debug){
        metadataFile = fopen(( "/tmp/" + filename + fileExtension).c_str(), "w");
	fprintf(metadataFile, "cellType: %s, maxThick %d, connectivityThresh %d, bg_Color %s debug: %d, filename: %s, kernelSize: %d, mean %f stdDev %f, allowsOverlap %s, withCycles %s skelJsonFile %s randSeed %d angle %f maxAxialDispl %f initialAsignHeuristicComplete %s\n", cellRestrictions->cellType.c_str(), cellRestrictions->maxThickness, cellRestrictions->connectivityThresh, cellRestrictions->bg_color.c_str(), debug, filename.c_str(), cellRestrictions->kernelSize, imgMean[0], imgStdDev[0], cellRestrictions->allowsOverlap ? "true" : "false", cellRestrictions->withCycles ? "true" : "false", cellRestrictions->nxJsonFile.c_str(), cellRestrictions->rseed, cellRestrictions->angle, cellRestrictions->maxAxialDispl, cellRestrictions->initAsignHeuristic ? "true" : "false");
        fprintf(metadataFile,"eID  |  cID (°)| \t neighID (°) |\tend1MC |\tend2MC |\t\t length |   width |\tphilSegmentCounter | edgeAngle(°) \n");
    }

    return retcode;
}

void interruptHandler(int s){
    try {
	if (display) cv::waitKey(0); // only wait when there is a X server to show stuff
    	/* free PhilSegments, edges and clusters */
    	//std::for_each(philPointers.begin(), philPointers.end(), freePhilSegments);
    	std::for_each(edgePointers.begin(), edgePointers.end(), freeEdges);
    	std::for_each(clusterPointers.begin(), clusterPointers.end(), freeClusters);
    	delete cellRestrictions;
	endColony();
    	/* close metadataFile */
    	if (metadataFile != nullptr){fclose(metadataFile);}
	if (debugL2File != nullptr){fclose(debugL2File);}
	printf("Caught signal %d, interrupting\n",s);
    } catch (std::exception &e){
	printf("%s\n", e.what());
    } catch (...){
	printf("General Exception catching while cleaning memory\n");
    }
    printf("Exiting\n");
    exit(1);
}
