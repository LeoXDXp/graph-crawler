# Before compiling: Requirements:
* This algorithm (aka "Phil the ACO Algorithm") was mainly tested in a Fedora OS (29 to 33 aprox.), and the packages used where:
```
gcc-c++ gtk2-devel opencv-devel jsoncpp libpq-devel jsoncpp-devel 
```

* The name of this packages might change between other Linux OSes. There's also a Dockerfile that you can just build with
docker or podman locally.

# How to Run

## Database

A running postgres database with the postgis extension created is needed. The db name, user and password are stored in ants.cpp
To setup a postgres database from scratch after installing it:
1. postgresql-setup --initdb
2. As user postgres, run psql
3. create user phil with password 'hiddenpassword';
4. create database phils
5. \c phils
6. create extension postgis;create extension postgis_topology;
**Just for testing/development. A more serious setup requires fine grained permissions**
7. grant ALL ON ALL tables in schema public to phil; grant ALL ON ALL sequences in schema public to phil;
8. create table ant(id serial NOT NULL, filename TEXT NOT NULL, num_contours integer, contour geometry(Multipolygon, 4326), area double precision null, perimeter double precision null, active boolean default 't', nodes int[], PRIMARY KEY(id, filename));
9. create table segments(id serial PRIMARY KEY, filename TEXT, banned_edge integer, segment geometry(LINESTRING,4326), node_ids integer[3]);
10. create table nodes(id serial PRIMARY KEY, filename TEXT, coord geometry(POINT,4326));
11. pg_hba.conf

```
# "local" is for Unix domain socket connections only
local   all             all                                     peer
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     md5
host    replication     all             127.0.0.1/32            md5
host    replication     all             ::1/128                 md5
```

## Preprocess
* In order to get a graph from an image, the jupyter notebook **SkelFromImage.ipynb** (located at ../MTs/benchmarkImages/) has an example flow through steps 1.1 to 1.4. Each function used in the notebooks is at phil_transformations.py or phil_viz.py. 
* This steps (1.1 to 1.4) will give you the **JSON file** needed for the **-z** option of the algorithm.

## Phil/ACO Algorithm

* Example with the _50_ROIs_Spinning_Marchantia.png_ image located at _../MTs/benchmarkImages/SpinningMarchantiaGraph/_ . Includes debugging in level 4 (optional) 
```
phil -d4 -cp -t11 -f ../MTs/benchmarkImages/SpinningMarchantiaGraph/50_ROIs_Spinning_Marchantia.png -z ../MTs/benchmarkImages/SpinningMarchantiaGraph/SpinningMarchantia_Skel.json
```

* Other examples can be found in the _gdblines_ file. Instead of having _phil_ at the beggining of each line, they have an **r**, due to it being tested within **gdb** during development.

## Results
* To visualize the results, the output of "Phil the ACO Algorithm", can be process through steps 2 and forward as indicated in the jupyter notebook **SkelFromImage.ipynb** (located at ../MTs/benchmarkImages/).

## Benchmark
The detail of the results presented in the thesis is available in [this benchmark](https://docs.google.com/spreadsheets/d/1iQdA7aS5sU3psBLtVDDKv-LjtbE5jeaX6wS1iE2bTOc) spreadsheet

## Creation of Grount Truth
1. Once the skeleton is passed to a networkX graph, the function _draw_graph_ assigns a unique ID to each node and edge. 
2. Then, the function _graphPlusBg_ show the graph with the edge IDs ant the segmented filament image as background, giving a better visualization of the relationship between filaments and edges. 
3. The previous image, plus the original image and the ROIs in fiji allow the manual assignation of the edges belonging to each filament. The edges IDs are the ones used to create arrays of filaments, which then are grouped in the variable _g_truth_ within each g_truth function in experimentData.py
