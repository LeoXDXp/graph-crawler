#ifndef NODES_HPP // include guard
#define NODES_HPP

#include "cluster.hpp"

void initNodeinCluster(std::pair<const int, Cluster*> &element);

Arista* createEdge(int edgeid);
void freeEdges(std::pair<const int, Arista*> &element);
void paintEdges(std::pair<const int, Arista*> &element);
void populateEdges(std::pair<const int, Cluster*> &element);
double euclideanDist(cv::Point2f& a, cv::Point2f& b); // better performance than cv::norm for simple points
double angleBetweenVecs(Nodo& initial, Nodo& middle, Nodo& end);
double angleBetweenVecs2(cv::Point2f initial, cv::Point2f middle, cv::Point2f end);
double angle2Points(const cv::Point2f& v1, const cv::Point2f& v2);
void nodeNeighborAngle(std::pair<const int, Cluster*> &element);
double angleBetweenSegs(cv::Point initial, cv::Point middle, cv::Point end);
#endif
