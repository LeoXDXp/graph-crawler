#ifndef GRAPH_HPP // include guard
#define GRAPH_HPP

#include "cluster.hpp"

int ProcessArgs(int argc, char** argv);
void print_usage(void);
void paintLegend(std::pair<const int, Cluster*> &element);
void showImg(std::string title, cv::Mat& img, int h=500, int w=1000);
void sobelAngles();
void printClusterNodeInfo(std::pair<const int, Cluster*> &element);
void secuencialMoments(Cluster* cluster, cv::Mat& img);

void interruptHandler(int s);
void printClusterInfo(Cluster* element);
#endif
