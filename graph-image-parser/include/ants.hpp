#ifndef ANTS_HPP // include guard
#define ANTS_HPP

#include "cluster.hpp"
#include <map>
#include <libpq-fe.h>

struct Ant;
struct Colony;
struct tourQuality;
struct Segment;

/* Cycle Check:
 * Key/id in filament map reflects order of insertion. This incremental id is controlled by internalID
 * for non-cycle structures,
 * numInitialSegments counts the number of edges created */
struct Ant{
    int id, internalID, edgeCount, latestAppendedEdgeID, firstEdgeID, pixelCounter, prevSeg;//plusDirection, minusDirection;
    double pathQuality, width, length, avgAngle, vecAngle, curvature, mengerCurvature, lastProb; // elongation; // vecAngle and lastProb are just for debugging
    double m10, m01, m11, m20, m02, eigenVal1, eigenVal2; // Image moments data from cluster/nodes
    cv::Point2f massCenter;
    cv::Scalar color;
    std::vector<std::vector<cv::Point>> contours;
    std::map<int,Arista*> filament; //unlike Cluster->edgesMap, here, int reflects the Arista's* id
    std::unordered_map<int,cv::Point2f> nodeIDs; // key is node/cluster id, value is node coords
    Nodo *startNode, *endNode;
    Colony *colonyPtr; // Reference to colony where ant belongs
    std::vector<int> interQuaEdges; // EdgeIDs of a path of intermQuality (not good, not bad)
    std::vector<int> segs; // Ints are segment IDs from the DB
    //std::unordered_map<int, std::vector<int>> segsData;
    std::unordered_map<int, int> replacedSegs; // map of replaced segments that were contained by bigger segments
    std::unordered_map<int, Segment*> internalSegs; // key is dbId
    std::unordered_map<int, std::unordered_map<int, double>> pheromRecord; // Ant internal copy of pherom value at moment of edge selection
};

struct Colony{
    int nodes, edges; //ants;// store node and edge count.
    std::vector<int> coveredEdges; // coveredEdges: 0 for not assigned, 1 for assigned to a path/ant. goodQualSolutions just a counter
    double alpha, beta, pheromoneBFactor; // Alpha param for pheromone weight (tau). Beta for (eta) quality weight. pheromoneBFactor aka gamma
    double maxThetaScore, minGoodQualityFactor; //notAdvancingFactor aka delta
    std::unordered_map<int, Ant*> colonyMap;
    std::unordered_map<int, int> pendingEdges;
    std::map<int, Ant*> goodAnts;
    // Pairs of edges of intermediate quality that were chosen and are part of a bad quality tour. Initial value is 1.0. Value decreases, penalizing pheromone of bad edges
    std::unordered_map<int, std::unordered_map<int, double>> bannedPathsPherom; // key is edge in interQuaEdges that results in a bad solution. Value has a key.value of  segmentID(from the db), pheromValue
};

struct edgeCandidateData{
    int newEndNodeID, dbSegID; // edgeID is in the key of the map that uses this struct
    double eta, pherom, probability, vecAngle;
    bool intermQuality;
};

struct Segment{
    int dbId;
    double length;
    Ant* parentAnt;
    Nodo *startNode, *endNode, *ogStartNode;
    std::unordered_map<int,cv::Point> nodes;
    std::vector<int> edges;
    Segment *extension;
};

//bool cluster_sorter(const Cluster* lhs, const Cluster* rhs);
bool sortPairDesc(const std::pair<int,int> &lft, const std::pair<int,int> &rght);
/* Comparison among different paths/ants */
void globalEval(bool finalCall=false);
/* Per ant tour evaluation */
void tourQuality(Ant* ant, double vecAngle);
/* Initialize ants, and calls parallel ant function */
void antColonyInit();
/* Ant tour building, eta, tau, probability calculation. Also tour quality assesment */
void buildCandidateSolution(Ant* ant);
//extern struct Colony *antColony;
void antDebugData(int antID);
void endColony();
//bool antSorter(Ant* ant1, Ant* ant2);
double mengerCurvature(cv::Point2f a, cv::Point2f b, cv::Point2f c);
void statistics();
int antStartingEdge(Ant* ant);
// DB Functions
void exit_nicely(PGconn *con);
void insertNodeDB(PGconn *con, Nodo node);
int insertSegmentDB(PGconn *con, Nodo node1, Nodo node2);
int segWithinDB(PGconn *con, int local_seg);
int extendedSegWithinDB(PGconn *con, Segment *currSeg, std::unordered_map<int,cv::Point>& prevSegNodes);
void extendSegDB(PGconn *con, int seg_id, Nodo new_node);
void updateSegmentsDB(PGconn *con, int edgeid, int segid);
void deletePreviousDB(PGconn *con);
void insertMPolyDB(PGconn *contours, std::vector<std::vector<cv::Point>> cont, int ant_id);
//double avgWidthMPolyDB(PGconn *con, int ant_id);
void antNodes(PGconn *con, Ant* ant);
bool antWithinDB(PGconn *con, int ant_id);
std::vector<int> dropDupDBAnts(PGconn *con, int ant_id);
#endif
