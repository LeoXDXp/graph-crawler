#ifndef CLUSTER_HPP // include guard
#define CLUSTER_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <unordered_map>

struct Nodo;
struct Arista;
struct Cluster;
struct Restrictions;
struct hash_pair;

// A hash function used to hash a pair of any kind
struct hash_pair {
    template <class T1, class T2>
    size_t operator()(const std::pair<T1, T2>& p) const{
        auto hash1 = std::hash<T1>{}(p.first);
        auto hash2 = std::hash<T2>{}(p.second);
        return hash1 ^ hash2;
    }
};

struct Nodo { /* node degree: 0: Disconnected node. 1: End Node, 2: Bridge Node (Only 2 neighbors/edges), 3: Connection Node (3+ neighbors/edges) */
    int id, degree;
    int nType; /* Same as clusterType. Added type 4: interQuaNode (aka startNode) and type 5 for center nodes in neuron case*/
    cv::Vec3b color;
    cv::Point2f coord;
    std::unordered_map<std::pair<int,int>, double, hash_pair> neighborNodesAngles; // stores the info returned by angleBetweenVecs. int's are neighbor node IDs
    Cluster *parent;
};
struct Arista {
    int id;
    double angle, width, length; // Angle is thru tan definition.
    bool endEdge; // if it begins in a endNode
    cv::Vec3b color; // initially green for everything ...
    Nodo *end1, *end2; // this Edge is incident to this nodes
    std::unordered_map<int, int> inAnt;
    std::vector<cv::Point> edgePxls; // In case of -z, store the pixels that conform the edge created by sknw/networkX
    std::unordered_map<int, cv::Point2f> interNodes; // In case of -z, store the non networkX nodes to recover pixels
};

/* numNeighbors reflect the cluster's degree. Not yet nodes, but share similar properties */
struct Cluster {
    int id, nxId, pxlCounterX, pxlCounterY, largestPxlNeighbor; // contourPxlCounter; //pxlCounterX|Y thickness and length
    int clusterType; // -3: deleted. -2: Merged w/bigger cluster, -1: Not Assigned, 0: Island, 1: End (non-border) , 2: Bridge (Only 2 neighbors), 3: Connection, 4: End (border)
    int minX, minY, maxX, maxY;
    double angle, magnitude, aspectRatio, eigenVal1, eigenVal2, lambdaRatio, veselness;
    double m10, m01, m11, m20, m02, covMatU20, covMatU02, covMatU11; // m00 == pixelMembers.size(). u_ij are the members of the covariance matrix to get orientation and elongation of a blob
    Cluster *replaceCluster;
    cv::Vec3b color;
    cv::Point2f massCenter;
    cv::Mat mask;
    std::unordered_map<int, Cluster*> neighboursMap;
    std::unordered_map<int,int> neighborBorderPxlCounter, absorbedClusters; // TODO: Consider re-assigning or deprecating neighborBorderPxlCounter
    std::vector<std::pair<int,int>> pixelMembers;
    std::vector<cv::Point> skelPxls;
    Nodo node;
    std::unordered_map<int, Arista*> edgesMap; // int here is clusterid, not edgeID
};

struct Restrictions { // 0: other, 1: neuron, 2: plant, 3: animal
    std::string cellType, bg_color, filename, nxJsonFile;
    int cellIdentifier, maxThickness, connectivityThresh, kernelSize, kernelSizeLowerLimit, nodeMaxDegree, rseed; //, minLength;
    double angle, maxAxialDispl; // The bending rigidity of microtubules is about 100 times that of intermediate and actin filaments (Civalek 2011)
    bool withCycles, allowsOverlap, initAsignHeuristic;
    std::unordered_map<int,int> interQuaEdges, endEdges; // Key is edge id, val es 0 for non occupied, 1 for occupied
};

/*
struct KeyHash : public std::unary_function<MyTuple, std::size_t> {
    std::size_t operator()(const MyTuple& k) const {
        // the magic operation below makes collisions less likely than just the standard XOR
        std::size_t seed = std::hash<int>()(std::get<0>(k));
        seed ^= std::hash<char>()(std::get<1>(k)) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        return seed ^ (std::hash<char>()(std::get<2>(k)) + 0x9e3779b9 + (seed << 6) + (seed >> 2));
    }
};
// define the comparison operator for this tuple
struct KeyEqual : public std::binary_function<MyTuple, MyTuple, bool> {
    bool operator()(const MyTuple& v0, const MyTuple& v1) const {
        return (std::get<0>(v0) == std::get<0>(v1) && std::get<1>(v0) == std::get<1>(v1) &&
                std::get<2>(v0) == std::get<2>(v1));
    }
};

typedef unordered_map<MyTuple, int, KeyHash, KeyEqual> MyMap;

*/
void checkNeighbors(int i, int j, double neighborThreshold, cv::Mat& borderFrame, bool alreadyAssigned=false);
Cluster* createCluster(int clusterid);
void paintClusters(std::pair<const int, Cluster*> &element);
void freeClusters(std::pair<const int, Cluster*> &element);
void mergeClusters(std::pair<const int, Cluster*> &element);
void updateClusterType(std::pair<const int, Cluster*> &element);
void updateMinMaxXY(int x, int y, Cluster* cluster);
void clusterDataAverage(std::pair<const int, Cluster*> &element);
void updateCovMat(Cluster* cluster);
void mergeSkelClusters();
void updateSkelNodeType(Cluster* cluster);

extern int clusterID, clusterCounter, edgeID, maxEdgesSupported, debug;
extern cv::Mat pxlMetadata, frame, frameGray, ogFrame, cluster, nodesMat, thinning, gradient, graph, frameGraph, antsMat, debugMat, debugPhils, joinedAnts;
extern FILE *metadataFile, *debugL2File;
extern std::unordered_map<int, Arista*> edgePointers;
extern std::unordered_map<std::pair<int,int>, Arista*, hash_pair> edgePointersByNode;
extern std::unordered_map<int, Cluster*> clusterPointers;
extern struct Restrictions *cellRestrictions;
//extern std::unordered_map<int, PhilSegment*> philPointers;
extern cv::Scalar imgStdDev, imgMean;
extern double neighborThreshold;
//extern std::unordered_map<std::pair<int,int>, cv::Point, hash_pair> skelNeighConn; // Key are IDs of cluster's sharing border
#endif
