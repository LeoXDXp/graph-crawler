###################################################### license

#    Copyright 2015 David Breuer
#    breuer@mpimp-golm.mpg.de
#    http://mathbiol.mpimp-golm.mpg.de/DeFiNe/
#
#    This file is part of DeFiNe.
#    
#    DeFiNe is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    DeFiNe is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with DeFiNe. If not, see <http://www.gnu.org/licenses/>.

##################################################################### imports

import gtk

import DeFiNe_calc
reload(DeFiNe_calc)

##################################################################### run: gui

class DeFiNe:    

    def __init__(self):
        self.builder=gtk.Builder()
        self.builder.add_from_file("DeFiNe_gui.glade")
        self.window=self.builder.get_object('window1')
        self.builder.connect_signals(self)
        self.window.show()        

    def on_window1_destroy(self,object):
        print ("Closing DeFiNe.")
        gtk.main_quit()

    def on_button1_clicked(self,object):
        print "Running DeFiNe."        
        inp=str(self.builder.get_object("filechooserbutton1").get_filename()).replace('\\','/')  
        sampling=self.builder.get_object("combobox1").get_active()
        overlap=1-self.builder.get_object("combobox2").get_active()        
        quality=self.builder.get_object("combobox3").get_active()        
        objective=self.builder.get_object("combobox4").get_active()
        angle=float(self.builder.get_object("spinbutton1").get_value())
        DeFiNe_calc.DeFiNe_calc(self,gtk,inp,sampling,overlap,quality,objective,angle)
        return 0
        
##################################################################### run: main       

if __name__ == '__main__':
    app=DeFiNe()
    gtk.main()
