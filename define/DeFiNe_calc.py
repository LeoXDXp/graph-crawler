###################################################### license

#    Copyright 2015 David Breuer
#    breuer@mpimp-golm.mpg.de
#    http://mathbiol.mpimp-golm.mpg.de/DeFiNe/
#
#    This file is part of DeFiNe.
#    
#    DeFiNe is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    DeFiNe is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with DeFiNe. If not, see <http://www.gnu.org/licenses/>.

###################################################### imports

import collections
import copy, time
import gtk
import matplotlib as mpl
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import random
import scipy as sp
import scipy.misc
import scipy.ndimage
import scipy.optimize
import scipy.spatial
import scipy.stats
import sys
import cvxopt
import cvxopt.glpk

import DeFiNe_help
import importlib
#importlib.reload(DeFiNe_help)

###################################################### calc: program
# sampling: BF (0) or RMST (1)
# overlap = true or false
# quality = pairwise, all-to-all, length, angle
# objective = total or average minimization
# angle = threshold degree
def DeFiNe_calc(self,gtk,inp,sampling,overlap,quality,objective,angle):
    start_time = time.time()
    #if(not images[t][e][i].lower().endswith(('.png','.jpg','.jpeg','.tif','.tiff'))):
    #message = gtk.MessageDialog(parent=self.window,type=gtk.MESSAGE_INFO, buttons=gtk.BUTTONS_OK,message_format='Wrong image format.')
    #message.format_secondary_text('Path: '+dir_err+'\n\nRequired: jpg, tiff, or png.\nFound: '+images[t][e][i].split('.')[-1])
    #result=message.run()
    #message.destroy()
    #if result == gtk.RESPONSE_OK: #return 0    
     
#    inp='/home/breuer/graph.gml'
#    sampling=1
#    overlap=0
#    quality=0
#    objective=0
#    angle=60.0
    
    ############################## read input   

    self.builder.get_object('progressbar1').set_text('Reading .gml file.')
    self.builder.get_object('progressbar1').set_fraction(0.25)
    while gtk.events_pending(): gtk.main_iteration()
        
    gg,pos,auto,manu=DeFiNe_help.gml2graph(inp)
    random.shuffle(manu)
    pathm=DeFiNe_help.pathe2pathn(gg,manu)
    roughs=np.array([DeFiNe_help.path_roughs(p,gg,pos) for p in pathm])
    angles=np.array([DeFiNe_help.path_angles(p,gg,pos) for p in pathm])
    qualim=np.vstack([roughs.T,angles.T]).T        
    
    ############################## generate paths
    
    self.builder.get_object('progressbar1').set_text('Generating input paths.')
    self.builder.get_object('progressbar1').set_fraction(0.50)
    while gtk.events_pending(): gtk.main_iteration()
    
    angle=int(angle)
    pathn=DeFiNe_help.generate_paths(gg,pos,50,angle,mode=sampling)
    roughs=np.array([DeFiNe_help.path_roughs(p,gg,pos) for p in pathn])
    angles=np.array([DeFiNe_help.path_angles(p,gg,pos) for p in pathn])
    qualia=np.vstack([roughs.T,angles.T]).T    
    idx=(qualia[:,0]>0)*(qualia[:,10]<angle)    
    qualia=qualia[idx]
    patha=[p for i,p in enumerate(pathn) if idx[i]]    
    pathe=DeFiNe_help.pathn2pathe(gg,patha)
    print ("edge paths pre FCP")
    for p in pathe:
        print (p)
    print("-")
    ############################## run FCP
    
    self.builder.get_object('progressbar1').set_text('Solving filament cover problem.')
    self.builder.get_object('progressbar1').set_fraction(0.75)
    while gtk.events_pending(): gtk.main_iteration()
    
    quali=[4,5,1,10][quality] # quality = 2 for length
    if(objective==0):
        xx,obj=DeFiNe_help.setcover(pathe,qualia[:,quali],exact=1-overlap)
    else:
        xx,obj=DeFiNe_help.setcover_mean(pathe,qualia[:,quali],exact=1-overlap)
    print("xx")
    print(xx)
    print("obj")
    print(obj)
    auto=[e for i,e in enumerate(pathe) if xx[i]]
    print("auto")
    print(auto)
    qualia=qualia[xx]    
    
    sim=DeFiNe_help.partition_similarity(auto,manu,gg=gg)
    print("sim")
    print(sim)
    mpp=DeFiNe_help.partition_matching(auto,manu)
    print("mpp")
    print(mpp)
     
    ############################## extend graph
    
    self.builder.get_object('progressbar1').set_text('Plotting filament cover and analyzes.')
    self.builder.get_object('progressbar1').set_fraction(1.0)
    while gtk.events_pending(): gtk.main_iteration()
    
    me=[[] for e in gg.edges()]
    for gi,g in enumerate(manu):
        for ei,e in enumerate(g):
            me[e].append(str(gi)+'-'+str(ei))
    ae=[[] for e in gg.edges()]        
    for gi,g in enumerate(auto):
        for ei,e in enumerate(g):
            ae[e].append(str(gi)+'-'+str(ei))
    for ei,e in enumerate(gg.edges(data=1)):
        e[2]['manu']=';'.join(me[ei])
        e[2]['auto']=';'.join(ae[ei])   
    
    ############################## generate output   
    
    name=inp[:-4]+'_sampling='+str(sampling)+'_overlap='+str(overlap)+'_quality='+str(quality)+'_objective='+str(objective)+'_angle='+str(angle)
    qualin=['rough_len_lin','rough_len_sq','rough_len_euc','rough_len_conv','rough_diff_pair','rough_diff_all','rough_abs_avg','rough_abs_cv','angle_len_lin','angle_diff_mean','angle_diff_max','angle_diff_cv','angle_abs_median','edge_assignment']
    similarities=['VI','RI','JI']
    colors=['SpringGreen','MediumBlue','DarkOrange']
    titles=['manu','auto (FCP)']    
    mpl.rcParams['font.size']=5        
    mpl.rcParams['figure.figsize']=8.0,6.0 # default 8.0,6.0
   
    ml=[';'.join([str(i) for i in j]) for j in manu]    
    mx=np.vstack([qualim.T,ml]).T        
    mx=np.vstack([qualin,mx])    
    np.savetxt(name+'_manu.csv',mx,fmt="%s",delimiter=",")
    
    al=[';'.join([str(i) for i in j]) for j in auto]    
    ax=np.vstack([qualia.T,al]).T    
    ax=np.vstack([qualin,ax])
    np.savetxt(name+'_auto.csv',ax,fmt="%s",delimiter=",")
    elapsed_time = time.time() - start_time
    print( "Completed DeFiNe Calculations. %f seconds" %elapsed_time)
    plt.clf()    
    plt.subplot(1,4,1) # manu           
    plt.title(titles[0])                 
    lbs,eps=DeFiNe_help.filament_label(manu,gg)
    lbc,epc,epm=DeFiNe_help.filament_color(eps,lbs,np.sort(mpp))
    DeFiNe_help.graph2gml(gg,pos,name+'_manu.gml',epc=epc)
    nx.draw_networkx_edges(gg,pos[:,:2],edge_color=epc,width=2)
    nx.draw_networkx_edge_labels(gg,pos[:,:2],edge_labels=lbc,font_color='black',font_size=5,bbox={'edgecolor':'none','facecolor':'none'})
    plt.ylim(plt.ylim()[::-1])
    plt.axis('off')  
    
    plt.subplot(1,4,2) # auto   
    plt.title(titles[1])        
    lbs,eps=DeFiNe_help.filament_label(auto,gg)
    print("lbs")
    print(lbs)
    #print("eps")
    #print(eps)
    lbc,epc,epm=DeFiNe_help.filament_color(eps,lbs,mpp)
    print("lbc")
    print(lbc)
    #print("epc")
    #print(epc)
    #print("epm")
    #print(epm)
    filamentNodeDict = {}
    filamentEdgeDict = {}
    for k,v in lbc.iteritems():
        if v in filamentNodeDict:
            filamentNodeDict[v].append(k)
        else:
            filamentNodeDict[v] = [k]
        try:
            if v in filamentEdgeDict:
                # Some values can have more than one id, in that case, it must be splitted and distributed
                filamentEdgeDict[v].append(gg[k[0]][k[1]]['eid'])
            else:
                filamentEdgeDict[v] = [gg[k[0]][k[1]]['eid']]
        except:
            continue
    '''
    Some keys in filamentEdgeDict can have more than one id, in that case, it must be splitted and distributed
    towars their respective single filaments ids
    '''
    multi_id_filaments = []
    for key, value in filamentEdgeDict.items():
        multi_ids = key.split(",")
        if len(multi_ids) > 1:
            multi_id_filaments.append(key)
            # Add the respective value (edge_id) to the single-id filament
            for fil_id in multi_ids:
                print("Splitting %s - %s" %(key, fil_id))
                # Check if the single-id filament has an entry in the dict
                if fil_id not in filamentEdgeDict:
                    filamentEdgeDict[fil_id] = []
                # Iterate and append values
                for val in value:
                    filamentEdgeDict[fil_id].append(val)

    for complex_id in multi_id_filaments:
        del filamentEdgeDict[complex_id]

    print("filamentNodeDict")
    print(filamentNodeDict)
    print("filamentEdgeDict")
    print(filamentEdgeDict)

    DeFiNe_help.graph2gml(gg,pos,name+'_auto.gml',epc=epc)
    nx.draw_networkx_edges(gg,pos[:,:2],edge_color=epc,width=2)
    nx.draw_networkx_edge_labels(gg,pos[:,:2],edge_labels=lbc,font_color='black',font_size=5,bbox={'edgecolor':'none','facecolor':'none'})   
    plt.ylim(plt.ylim()[::-1])                     
    plt.axis('off')      
    
    plt.subplot(2,4,3) # dist len      
    s=0
    mm=max(qualim[:,s].max(),qualia[:,s].max())
    sm=int(mm+10)/10*10
    plt.hist(qualim[:,s],list(range(sm+10)),lw=2,color='black',histtype='step',align='mid',alpha=0.5)
    plt.hist(qualia[:,s],list(range(sm+10)),lw=2,color='black',histtype='step',align='mid',alpha=1.0)
    plt.title('p_KS = '+'%.1e'%sp.stats.ks_2samp(qualim[:,s],qualia[:,s])[1])
    plt.xlim(0,sm)
    plt.xlabel('filament length')
    plt.ylabel('frequency')
    
    plt.subplot(2,4,4) # dist ang 
    a=10
    try:
        plt.hist(qualim[:,a],list(range(0,61,5)),lw=2,color='black',histtype='step',align='mid',alpha=0.5)
    except:
        pass
    try:
        plt.hist(qualia[:,a],list(range(0,61,5)),lw=2,color='black',histtype='step',align='mid',alpha=1.0)
    except:
        pass
    plt.title('p_KS = '+'%.1e'%sp.stats.ks_2samp(qualim[:,a],qualia[:,a])[1])
    plt.xlim(-5,65)
    plt.xlabel('filament angle')
    plt.ylabel('frequency')
    
    plt.subplot(2,4,7) # sim    
    sims=np.reshape(sim[3:],(-1,2)).T
    S=len(sims[0])
    H=max(S,1) 
    for i in range(3):
        plt.plot(list(range(1,H+1)),sim[i]*np.ones(H),label=similarities[i],lw=2,color=colors[i],ls='--')
    for i in range(2*min(1,S)):
        plt.plot(list(range(1,H-1)),sims[i][:-2],lw=2,color=colors[1+i],label=similarities[1+i]+'$^d$')     
        diff=(sims[i][-1]-sims[i][-2])
        plt.plot([H-2+0.00,H-2+0.25],[sims[i][-2],sims[i][-2]+0.25*diff],lw=2,color=colors[1+i],ls=':')     
        plt.plot([H-2+0.75,H-2+1.00],[sims[i][-2]+0.75*diff,sims[i][-1]],lw=2,color=colors[1+i],ls=':')     
        plt.plot([H-1],sims[i][-1:],lw=2,color=colors[1+i],marker='o')     
    plt.xlabel('edge distance d')
    plt.ylabel('partition similarity')
    plt.ylim(-0.05,1.05)
    plt.xlim(1,H-1)
    plt.xticks(np.hstack([list(range(1,H-1,2)),H-1]),np.hstack([list(range(1,H-1,2)),'$\\infty$']))
    plt.legend(loc=0,frameon=0)
    
    plt.subplot(2,4,8) # corr
    r,a=6,10
    man=qualim[:,0]>1
    if(man.sum()==0):
        man=qualim[:,0]>-99
    poly=np.polyfit(qualim[:,r][man],qualim[:,a][man],1)
    plt.plot(qualim[:,r],np.poly1d(poly)(qualim[:,r]),lw=2,color='black',alpha=0.5)
    plt.plot(qualim[:,r][man],qualim[:,a][man],marker='s',ls='-',mew=2,mfc='none',mec=[0,0,0,0.5],alpha=0.5,ms=6,label='manu')
    plt.plot(qualim[:,r][-man],qualim[:,a][-man],marker='s',ls='-',mew=2,mfc='none',mec=[0,0,0,0.5],alpha=0.5,ms=2)
    aan=qualim[:,0]>1
    if(aan.sum()==0):
        aan=qualim[:,0]>-99
    poly=np.polyfit(qualim[:,r][aan],qualim[:,a][aan],1)
    plt.plot(qualim[:,r],np.poly1d(poly)(qualim[:,r]),lw=2,color='black',alpha=1.0)
    plt.plot(qualim[:,r][aan],qualim[:,a][aan],marker='o',ls='-',mew=2,mfc='none',color=[0,0,0,1.0],alpha=1.0,ms=6,label='auto')
    plt.plot(qualim[:,r][-aan],qualim[:,a][-aan],marker='o',ls='-',mew=2,mfc='none',color=[0,0,0,1.0],alpha=1.0,ms=2)
    plt.title('auto p_tau = '+'%.1e'%sp.stats.kendalltau(qualim[:,r],qualim[:,a])[1]+'\n manu p_tau = '+'%.1e'%sp.stats.kendalltau(qualim[:,r],qualim[:,a])[1],ha='right')
    plt.legend(loc=0,frameon=0)
    plt.xlabel('filament weight')
    plt.ylabel('filament angle')
    
    plt.tight_layout(pad=0.1)    
    plt.savefig(name+'.pdf')
    plt.savefig(name+'.svg')
    plt.show() 
    
    self.builder.get_object('progressbar1').set_text('Done.')
    self.builder.get_object('progressbar1').set_fraction(0.0)
    while gtk.events_pending(): gtk.main_iteration()
    
    return 0
